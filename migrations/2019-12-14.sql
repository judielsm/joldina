INSERT INTO `tipo_doc` (`nombre`) VALUES ('ID(permiso de conducir)');
ALTER TABLE tipo_doc ADD enabled  BOOLEAN DEFAULT true;
UPDATE tipo_doc SET enabled = false where id in ("1","4","6");
ALTER TABLE productos ADD src VARCHAR(200) DEFAULT NULL;
ALTER TABLE clientes ADD labels TEXT DEFAULT NULL;
ALTER TABLE clientes ADD comments TEXT DEFAULT NULL;
ALTER TABLE clientes ADD lista_negra BOOLEAN DEFAULT false;
CREATE TABLE baneados (id int NOT NULL AUTO_INCREMENT, dni VARCHAR(255), id_cliente int, CONSTRAINT baneados_pk PRIMARY KEY (id))

