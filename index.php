<?php
include ("inc/config.php");

if (isset($_GET['email']) && isset($_GET['token'])){
	$Query = "SELECT email,token FROM empleados WHERE email='" . $_GET['email'] . "' AND token='" . $_GET['token'] . "'";
	$QueryB = $mysqli->query($Query);
	if ($QueryB->num_rows > 0){
		$validate = "success";
		}

	$QueryB->close();
	}
?>
<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="theme-color" content="#424242">
      <title>Joldina Remesas SLU</title>
      <link rel="shortcut icon" href="img/favicon.ico">
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/login.css">
      <script src="js/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
	  <script type="text/javascript">
	  <?php if ($validate=="success"){ ?>	
	  
		$(window).on('load', function() {
			$('#recuperar_clave').modal('show');
		});

		function nuevaClave() {
			var nemail = $('#emailResett').val();
			var nclave = $('#passwordResett').val();
			$.ajax({
				url: 'loggin.php',
				data: { 
						"nclave" : nclave,
						"nemail" : nemail
						},
				type: "POST",
				dataType: 'json',
				success: function(data) {
					if (data.status == 'success') {
						window.location.href = "index.php?message=inicio";
					}
				}
			});
		}
		
		<?php } ?>
		
		window.setTimeout(function() {
			$(".alert-danger").fadeTo(500, 0).slideUp(500, function() {
				$(this).remove();
			});
		}, 5000);

		function recuperarClave() {
			var email = $('#emailReset').val();
			var dataString = 'email=' + email;
			$.ajax({
				url: 'reset.php',
				data: dataString,
				type: "POST",
				dataType: 'json',
				success: function(data) {
					console.log(data);
					if (data.status == null) {
						window.location.href = "index.php?message=recovery";
					} else {
						window.location.href = "index.php?message=enviado";
					}
				}
			});
		}	
	  </script>
</head>
<body>
   <nav class="navbar navbar-default navbar-defaulta">
      <div class="container">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="img/brand.png" title="Joldina Remesas SLU"></a>
         </div>
         <div class="collapse navbar-collapse" id="navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
               <li><a data-toggle="modal" href="#login" class="tope">Iniciar Sesión</a></li>
            </ul>
         </div>
      </div>
   </nav>
   <div class="site-wrapper-inner">
      <div class="cover-container">
         <div class="inner cover">
            <?php
			if (isset($_GET['message'])){
               $strAccion = $_GET['message'];
               switch ($strAccion) {
               case "error":
               ?>
            <div class="row">
               <div class="alert alert-danger  col-md-4 col-md-offset-4" align="center">
                  Usuario o clave invalida
               </div>
            </div>
            <?php
               break;
               case "recovery":
               ?>
            <div class="row">
               <div class="alert alert-danger  col-md-4 col-md-offset-4" align="center">
                  Usuario no existe
               </div>
            </div>
            <?php
               break;
               case "enviado":
               ?>
            <div class="row">
               <div class="alert alert-success  col-md-6 col-md-offset-3" align="center">
                  Se ha enviado el email de recuperación
               </div>
            </div>
            <?php
               break;
               case "inicio":
               ?>
            <div class="row">
               <div class="alert alert-success  col-md-6 col-md-offset-3" align="center">
                  Clave cambiada correctamente inicie sesión
               </div>
            </div>
            <?php
               break;
               }
			   }
               ?>
            <p class="lead"><a class="btn btn-lg btn-primary" data-toggle="modal" href="#envia">ENVIAR AHORA</a></p>
         </div>
         <div class="mastfoot">
            <div class="inner">
               <p>© 2018 Joldina Remesas SLU</p>
            </div>
         </div>
      </div>
   </div>
   <div class="modal fade" id="login" role="dialog">
      <div class="modal-dialog modal-sm">
         <div class="modal-content">
            <div class="modal-header" align="center">
               <img id="img_logo" src="img/logo-login.png">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               x
               </button>
            </div>
            <form name="login" action="login.php" method="post" enctype="multipart/form-data">
               <div class="modal-body">
                  <input id="user" name="user" class="form-control" type="email" placeholder="E-mail" required>
                  <input id="pass" name="pass" class="form-control" type="password" placeholder="Clave" required>
                  <div style="padding-top:10px;"><a data-toggle="modal" data-dismiss="modal" href="#reset">¿Olvido su contraseña?</a></div>
               </div>
               <div class="modal-footer">
                  <div>
                     <input id="enviar" name="enviar" type="submit" class="btn btn-primary btn-lg btn-block" value="Entrar">
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="modal fade" id="reset" role="dialog">
      <div class="modal-dialog modal-sm">
         <div class="modal-content">
            <div class="modal-header" align="center">
               <img id="img_logo" src="img/logo-login.png">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               x
               </button>
            </div>
            <form id="recovery" method="POST" onsubmit="recuperarClave();">
               <div class="modal-body">
                  <input id="emailReset" name="emailReset" class="form-control" type="email" placeholder="E-mail" required>
               </div>
               <div class="modal-footer">
                  <div>
                     <input id="recuperar" name="recuperar" type="submit" class="btn btn-primary btn-lg btn-block" value="Recuperar">
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="modal fade" id="recuperar_clave" role="dialog">
      <div class="modal-dialog modal-sm">
         <div class="modal-content">
            <div class="modal-header" align="center">
               <img id="img_logo" src="img/logo-login.png">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               x
               </button>
            </div>
            <form id="nueva_clave" method="POST" onsubmit="nuevaClave();">
               <div class="modal-body">
                  <input id="emailResett" name="emailResett" class="form-control" type="email" placeholder="E-mail" value="<?php echo $_GET["email"];?>" readonly>
                  <input id="passwordResett" name="passwordResett" class="form-control" type="text" placeholder="Clave" required>
               </div>
               <div class="modal-footer">
                  <div>
                     <input id="restablecer" name="restablecer" type="submit" class="btn btn-primary btn-lg btn-block" value="Restablecer">
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="modal fade" id="envia" role="dialog">
      <div class="modal-dialog modal-sm">
         <div class="modal-content">
            <div class="modal-header" align="center">
               <img id="img_logo" src="img/logo-login.png">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              x
               </button>
            </div>
            <div class="alert alert-danger" role="alert">
               ESTA FUNCIÓN NO ESTA ACTIVA
            </div>
         </div>
      </div>
   </div>
</body>
</html>