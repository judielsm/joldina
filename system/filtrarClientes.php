<?php
include ("../inc/config.php");
$nacionalidad = $_GET['n'];
$pago = $_GET['p'];
$envios = $_GET['s'];
$compra = $_GET['c'];


function pagos($pago)
{
    if (is_null($pago) || !$pago ) {
        return '';
    }

    return "
        JOIN
            pagos
        ON
            clientes.id = pagos.id_cliente
    ";
}

function envios($envio)
{
    if (is_null($envio) || !$envio ) {
        return '';
    }
    return "
        JOIN
            envios
        ON
            clientes.id = envios.id_cliente
    ";
}
function compras($compra)
{
    if (is_null($compra) || !$compra ) {
        return '';
    }

    return "
        JOIN
            productos_ventas
        ON
            clientes.id = productos_ventas.id_cliente
    ";
}

function filter($nacionalidad)
{
    
    if (is_null($nacionalidad)) {
        return '';
    }
    return " where clientes.nacionalidad = $nacionalidad ";
}
$strQuery = "SELECT
				C.*,
                paises.nombre as pais,
                (SELECT COUNT(P.Id) FROM pagos P WHERE P.id_cliente= C.id) AS pagosCount,
                (SELECT SUM(P.monto) FROM pagos P WHERE P.id_cliente= C.id) AS pagoMonto,
                (SELECT COUNT(E.Id) FROM envios E WHERE E.id_cliente= C.id) AS enviosCount,
                (SELECT SUM(E.monto) FROM envios E WHERE E.id_cliente= C.id) AS enviosMonto,
                 (SELECT COUNT(CM.Id) FROM productos_ventas CM WHERE CM.id_cliente= C.id) AS comprasCount,
                 (SELECT SUM(CM.total) FROM productos_ventas CM WHERE CM.id_cliente= C.id) AS comprasMonto
               
			FROM
                 clientes C
             LEFT OUTER JOIN 
                paises ON paises.id = C.nacionalidad
                 ";


$clientes= $mysqli->query($strQuery);
$json = array();
while($row = $clientes->fetch_array()){
     $json['data'][] = $row;
}	

$clientes->close();

echo json_encode($json);

?>
