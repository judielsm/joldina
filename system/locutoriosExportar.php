<?php
include ("../inc/config.php");			

$fechaIni = $_POST['fecha_ini']; 
$fechaIni = explode('-', $fechaIni);
$fechaIni = $fechaIni[2].'-'.$fechaIni[1].'-'.$fechaIni[0];
$fechaFin = $_POST['fecha_fin']; 
$fechaFin = explode('-', $fechaFin);
$fechaFin = $fechaFin[2].'-'.$fechaFin[1].'-'.$fechaFin[0];
$idSucursal  = $_POST['sucursal']; 
$formatoTipo = $_POST['formato'];

if($idSucursal=="100"){
	
	$Query   = "SELECT pagos_locutorio.id AS NUM, DATE_FORMAT(pagos_locutorio.fecha, '%d-%m-%Y') FECHA,"; 
	$Query  .= "sucursal.nombre AS SUCURSAL, UPPER(empleados.nombres) EMPLEADO, UPPER(tipo_pago.nombre) TPAGO, pagos_locutorio.monto AS MONTO "; 
	$Query  .= "FROM locutorio, pagos_locutorio, sucursal, empleados, tipo_pago WHERE pagos_locutorio.id_locutorio = locutorio.id AND ";
	$Query  .= "pagos_locutorio.id_sucursal = sucursal.id AND pagos_locutorio.id_empleado = empleados.id AND pagos_locutorio.id_pago = tipo_pago.id AND "; 
	$Query  .= "pagos_locutorio.fecha BETWEEN('".$fechaIni."') AND ('".$fechaFin."') ORDER BY  NUM ASC";
	
}else{
		
	$Query   = "SELECT pagos_locutorio.id AS NUM, DATE_FORMAT(pagos_locutorio.fecha, '%d-%m-%Y') FECHA,"; 
	$Query  .= "sucursal.nombre AS SUCURSAL, UPPER(empleados.nombres) EMPLEADO, UPPER(tipo_pago.nombre) TPAGO, pagos_locutorio.monto AS MONTO "; 
	$Query  .= "FROM locutorio, pagos_locutorio, sucursal, empleados, tipo_pago WHERE pagos_locutorio.id_locutorio = locutorio.id AND ";
	$Query  .= "pagos_locutorio.id_sucursal = sucursal.id AND pagos_locutorio.id_empleado = empleados.id AND pagos_locutorio.id_pago = tipo_pago.id AND "; 
	$Query  .= "pagos_locutorio.fecha BETWEEN('".$fechaIni."') AND ('".$fechaFin."') AND pagos_locutorio.id_sucursal = '".$idSucursal."' ORDER BY  NUM ASC";	
		
}
if($formatoTipo=="1"){
?>
<?php	
header('Content-type: application/vnd.ms-excel;charset=utf-8');
header('Content-Disposition: attachment; filename=locutorios-'.date('d-m-Y').'.xls');

	$Reporte = $mysqli->query($Query); ?>

	<table border="1" cellpadding="2" cellspacing="0" width="100%"> 
	  <tr>
		<td bgcolor="#C4D79B" align="center"><b>NUM</b></td>
		<td bgcolor="#C4D79B" align="center"><b>FECHA</b></td>
		<td bgcolor="#C4D79B" align="center"><b>SUCURSAL</b></td>
		<td bgcolor="#C4D79B" align="center"><b>EMPLEADO</b></td>
		<td bgcolor="#C4D79B" align="center"><b>TIPO PAGO</b></td>
		<td bgcolor="#C4D79B" align="center"><b>MONTO</b></td>
	  </tr>
	<?php while($row = $Reporte->fetch_array()){  
			if ($colordefila==0){
				$color= "#ffffff";
				$colordefila=1;
			 }else{
				$color="#DCE6F1";
				$colordefila=0;
			 }
	?>
	  <tr>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['NUM'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['FECHA'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['SUCURSAL'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo utf8_decode($row['EMPLEADO']);?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['TPAGO'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['MONTO'], 2, ',', '.');?></td>
	  </tr>
<?php	} ?>
	</table>
<?php	
}else{

header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename=locutorios-'.date('d-m-Y').'.csv');
?>
NUM;FECHA;SUCURSAL;EMPLEADO;TIPO PAGO;MONTO
<?php
	$Reporte = $mysqli->query($Query);
	while($row = $Reporte->fetch_array()){   
	
echo$row['NUM'].";".$row['FECHA'].";".$row['SUCURSAL'].";".utf8_decode($row['EMPLEADO']).";".$row['TPAGO'].";".number_format($row['MONTO'], 2, ',', '.')."\n"; }
		
}
?>
