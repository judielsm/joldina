<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Respaldo de datos</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<div class="row">
   <div class="card-box">
      <p class="text-muted m-b-25 font-13 text-center">Recuerde hacer respaldo de su base de datos diariamente esto ayuda a recuperar cualquier información</p>
      <div class="row mb-4">
         <div class="col-md-2 col-xs-1">&nbsp;</div>
         <div class="col-md-8 col-xs-10 text-center">
            <a href="../backup/backup.php" target="ventana" class="btn btn-info btn-lg"><i class="fa fa-cloud"></i> Descargar Respaldo</a>
         </div>
         <div class="col-md-2 col-xs-1">&nbsp;</div>
      </div>
      <iframe name="ventana" style="height: 100%; width: 100%; border:none;"></iframe>
   </div>
</div>