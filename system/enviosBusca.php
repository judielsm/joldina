<?php
include ("../inc/config.php");

$sql = "SELECT
			envios.id AS id_en,
			DATE_FORMAT(envios.fecha, '%d-%m-%Y') fecha_en,
			envios.n_envio AS numero_en,
			sucursal.nombre AS sucursal_en,
			remesadora.nombre AS remesadora_en,
			envios.monto AS monto_en,
			envios.id_cliente AS cliente_en
		FROM
			envios,
			sucursal,
			remesadora
		WHERE
			envios.devolucion = 0 AND 
			envios.id_sucursal = sucursal.id AND
			envios.id_remesadora = remesadora.id AND
			DATE_FORMAT(envios.fecha, '%d-%m-%Y') ='".date('d-m-Y')."'";

if($_GET['id'] !="100"){
	$sql .= " AND envios.id_sucursal='".$_GET['id']."'";
}

$result = $mysqli->query($sql);
$json = array();
while($row = $result->fetch_array()){
     $json['data'][] = $row;
}	

$result->close();		
echo json_encode($json);		

?>