<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
$strQuery = "SELECT * FROM sms";
 $row = $mysqli->query($strQuery);
 $sms = $row->fetch_array(); 
?>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Gateway SMS</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<div class="row">
<div class="col-md-4 col-xs-12">&nbsp;</div>
<div class="col-md-4 col-xs-12">
<div class="card-box">
   <div class="row">
      <div class="text-center">
         <div class="member-card">
		<form action="index.php?page=acciones&accion=smsEdita" method="post" enctype="multipart/form-data">
		 <input type="hidden" id="validator" name="validator" value="ok">
            <div class="text-center">
               <p class="text-muted font-13"> Api
			   <input class="form-control" type="text" id="api" name="api" placeholder="Api Key" value="<?php echo $sms['api']; ?>" required>
			   </p>
            </div>
			 <div class="text-center">
               <p class="text-muted font-13"> Teléfono
			   <input class="form-control" type="number" id="numero" name="numero" placeholder="Teléfono" value="<?php echo $sms['numero']; ?>" required>
			   </p>
            </div>
            <hr/>
            <button type="submit" class="btn btn-info btn-sm w-sm waves-effect m-t-10 waves-light"> Actualizar Datos</button>
			</form>
         </div>
      </div>
   </div>
   <div class="col-md-4 col-xs-12">&nbsp;</div>
</div>