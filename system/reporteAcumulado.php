<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Total Acumulado</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<form id="frm-exportar" action="acumuladoExportar.php" method="post" enctype="multipart/form-data" target="_blank">
<div class="row">
	<div class="col-md-4">
	<label class="control-label" for="fecha"> Fecha</label>
				<div class="input-group date" data-provide="datepicker">
				<input type="text" id="fecha" name="fecha" class="form-control" value="<?php echo date("d-m-Y"); ?>" required>
				<div class="input-group-addon">
			<span class="glyphicon glyphicon-th"></span>
		</div>
	</div>						
	</div>
	<div class="col-md-4">
		<div class="form-group">
                     <label class="control-label" for="sucursal"> Sucursal</label>
                     <select class="form-control" name="sucursal" id="sucursal">
                        <?php
								$strQuery = "SELECT * FROM sucursal WHERE id!=100 ORDER BY id ASC";
								$Sucursales= $mysqli->query($strQuery);
								while($Suc = $Sucursales->fetch_array()) {?>
									<option value="<?php echo $Suc['id']; ?>"><?php echo $Suc['nombre']; ?></option>
								<?php	 
								}
								$Sucursales->close();
							?>	
                     </select>
                  </div>		
	</div>
	<div class="col-md-4 text-center" style="padding-top: 27px;">
		<button type="submit" id="formato" name="formato" value="2" class="btn w-lg btn-info waves-effect w-md waves-light m-b-5"><i class="mdi mdi-eye"></i> Visualizar</button>	
		<button type="submit" id="formato" name="formato" value="1" class="btn w-lg btn-teal waves-effect w-md waves-light m-b-5"><i class="mdi mdi-file-excel-box"></i> Exportar Excel</button>
	</div>
</div>
</form>