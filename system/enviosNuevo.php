<?php
if ($_SESSION["joldin_log"] == TRUE) {
} else {
    header("Location: ../index.php");
}
?>
<script type="text/javascript">
$selecClientes = null;
$selecBene = null;
$selecPaises = null;
$selecPagador = null;
$(function() {
	

	
    var form = $("#frm-envio");


form.validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
});
    iniciarBusquedaPais();
    iniciarBusquedaCliente();
    iniciarBusquedaBene();
	iniciarBuscaPagadores();
	
    $('input:radio[name="optradio"]').on('click', function() {
        iniciarBusquedaCliente();
    });
    $('input:radio[name="optradioB"]').on('click', function() {
        iniciarBusquedaBene();
    });


    $("#buscarCliente").on("select2:select", function() {
        cargarCliente();
    });

    $("#buscarBeneficiario").on("select2:select", function() {
        cargarBene();
    });
});

function iniciarBusquedaPais() {
		$selecPaises = $('#pais').select2({
		tags:true,
        language: "es",
		allowClear: true,
        ajax: {
            url: 'enviosPais.php',
            dataType: 'json',
            processResults: function(data) {
                return {
                    results: data
                };
            },
			
        }
    });
}

function iniciarBuscaPagadores(){
		$("#pagador").val('').trigger('change');
		$selecPagador = $('#pagador').select2({
		tags:true,
        language: "es",
		allowClear: true,
		 placeholder: "Seleccione un pagador...",
        ajax: {
          url: "enviosPagador.php?id="+$('input:radio[name="remesadora"]:checked').val()+"&pais="+$('select[name=pais]').val(),
          dataType: 'json',
		  type:"GET",
          processResults: function (data) {
            return {
              results: data
            };
          },
        }
      });
}


function iniciarBusquedaCliente() {
    $selecClientes = $("#buscarCliente").select2({
        allowClear: true,
        tags: true,
        language: "es",
        ajax: {
            url: 'enviosClientes.php?tipo=' + $('input:radio[name="optradio"]:checked').val(),
            dataType: 'json'

        }
    });
}

function iniciarBusquedaBene() {
	$("#id-bene").val(0);
	$("#nombresB").val('');
	$("#ciudadB").val('');
	$("#pais").val('').trigger('change');
    $selecBene = $("#buscarBeneficiario").select2({
        allowClear: true,
        tags: true,
        language: "es",
        ajax: {
            url: 'enviosBeneficiarios.php?cliente='+$("#id-cliente").val() +'&tipo=' + $('input:radio[name="optradioB"]:checked').val(),
            dataType: 'json'
        }
    });
}

function cargarCliente() {
    var id = $("#buscarCliente").val();
    console.log(id);
    $.ajax({
        url: 'enviosCargaC.php',
        type: 'POST',
        data: "id=" + id,
        dataType: "JSON",
        success: function(per) {
            console.log(per);
            if (per.cant > 0) {
                var dat = per.datos;
                $("#id-cliente").val(dat.id);
                $("#nombres").val(dat.nombres);
                $("#tipo_id").val(dat.tipo_doc);
                $("#n_doc").val(dat.documento);
                $("#direccion").val(dat.direccion);
                $("#ciudad").val(dat.ciudad);
                $("#telefono").val(dat.telefono);
				iniciarBusquedaBene();
            } else {
                $("#id-cliente").val(0);
                $("#nombres").val('');
                $("#tipo_id").val('');
                $("#n_doc").val('');
                $("#direccion").val('');
                $("#ciudad").val('');
                $("#telefono").val('');
            }
            $selecClientes.val(null).trigger("change");
        }
    });
}

function cargarBene() {
    var id = $("#buscarBeneficiario").val();
    console.log(id);
    $.ajax({
        url: 'enviosCargaB.php',
        type: 'POST',
        data: "id=" + id,
        dataType: "JSON",
        success: function(per) {
            console.log(per);
            if (per.cant > 0) {
                var dat = per.datos;
                $("#id-bene").val(dat.id);
                $("#nombresB").val(dat.nombres_b);
                $("#ciudadB").val(dat.ciudad_b);
                var data = [{
                    id: dat.pais_b,
                    text: dat.paisn_b+" | "+dat.paisnn_b
                }];

                $('#pais').select2({
                    data: data
                });
                $('#pais').val(dat.pais_b);
                iniciarBusquedaPais();
            } else {
                $("#id-bene").val(0);
                $("#nombresB").val('');
                $("#ciudadB").val('');
                $("#pais").val('');

            }
            $selecBene.val(null).trigger("change");
        }
    });
}
</script>
<div class="row">
    <div class="col-xs-12">
        <div class="page-title-box">
            <h4 class="page-title">Datos Envío</h4>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row" style="padding:10px;">
    <a href="index.php?page=enviosNuevoExpress" class="btn waves-effect btn-xlg btn-info"><i class="mdi mdi-plus-box"></i> ENVÍO EXPRESS</a>
</div>
<div class="row">
    <div class="card-box">
		<form id="frm-envio" action="index.php?page=acciones&accion=enviosNuevo" method="post" enctype="multipart/form-data">
		<input type="hidden" name="validator" id="validator" value="ok" />
		   <input type="hidden" name="empleado" id="empleado" value="<?php echo $_SESSION["joldin_ide"];?>" />
		 <input type="hidden" name="sucursal" id="sucursal" value="<?php echo $_SESSION["joldin_suc"];?>" />
		 <div>
            <h3>Cliente</h3>
            <section>
                <div class="row" style="padding-bottom:10px;">
                    <div class="col-md-6">
                        Buscar Cliente Por:
                    </div>
                    <div class="col-md-3">
                        <label><input type="radio" name="optradio" value="d">Documento</label>
                    </div>
                    <div class="col-md-3">
                        <label><input type="radio" name="optradio" value="n" checked>Nombres</label>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <select id="buscarCliente" name="buscarCliente" class="buscarCliente form-control"></select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <input type="hidden" id="id-cliente" name="id-cliente">
                            <label class="control-label" for="nombres"> Nombres</label>
                            <input id="nombres" name="nombres" type="text" class="form-control required" placeholder="Pedro Jose" autocomplete="name"/>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="tipo_id"> Tipo Doc</label>
                            <select id="tipo_id" name="tipo_id" class="form-control">
                                <?php
                                $strQuery = "SELECT * FROM tipo_doc";
                                $strBuscaTD = $mysqli->query($strQuery);
                                while ($strTD = $strBuscaTD->fetch_array()) {

                                    ?>
                                    <option value="<?php echo $strTD['id']; ?>"><?php echo $strTD['nombre']; ?></option>
                                    <?php
                                }
                                $strBuscaTD->close();
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="n_doc"> N Doc</label>
                            <input id="n_doc" name="n_doc" type="text" class="form-control" placeholder="0000000000"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="direccion"> Dirección</label>
                            <textarea id="direccion" name="direccion" class="form-control required" rows="2" autocomplete="street-address"> </textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="ciudad"> Ciudad</label>
                            <input id="ciudad" name="ciudad" type="text" class="form-control required" autocomplete="address-level2" placeholder="Madrid"/>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="telefono"> Teléfono</label>
                            <input id="telefono" name="telefono" type="number" class="form-control required" autocomplete="tel-national" placeholder="000000000"/>
                        </div>
                    </div>
                </div>
            </section>
            <h3>Beneficiario</h3>
            <section>
                <div class="row" style="padding-bottom:10px;">
                    <div class="col-md-6">
                        Buscar Beneficiario Por:
                    </div>
                    <div class="col-md-3">
                        &nbsp;
                    </div>
                    <div class="col-md-3">
                        <label><input type="radio" name="optradioB" value="n" checked>Nombres</label>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <select class="buscarBeneficiario form-control" name="buscarBeneficiario" id="buscarBeneficiario"></select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
							<input type="hidden" id="id-bene" name="id-bene" value="0">
                            <label class="control-label" for="nombresB"> Nombres</label>
                            <input id="nombresB" name="nombresB" type="text" class="form-control required" autocomplete="name" placeholder="Pedro Jose"/>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="pais"> Pais</label>
                            <select id="pais" name="pais" class="pais form-control required" autocomplete="country-name"></select>
                        </div>
                    </div>
					 <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="ciudadB"> Ciudad</label>
                            <input id="ciudadB" name="ciudadB" type="text" class="form-control required" autocomplete="address-level2" placeholder="Madrid"/>
                        </div>
                    </div>
                </div>
            </section>
			<h3>Envio</h3>
            <section>
                <div class="row">
                    <div class="col-md-3 col-xs-12">
					<label class="control-label" for="fecha"> Fecha</label>
					<div class="input-group date" data-provide="datepicker">
						<input type="text" id="fecha" name="fecha" class="form-control required" value="<?php echo date("d-m-Y"); ?>">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-th"></span>
						</div>
					</div>
                    </div>
                    <div class="col-md-3 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="nenvio"> Nro Envio</label>
                            <input id="nenvio" name="nenvio" type="text" class="form-control" placeholder="000000"/>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="tpago"> Tipo Pago</label>
                            <select id="tpago" name="tpago" class="form-control required">
                                <?php
                                    $strQuery = "SELECT * FROM tipo_pago ORDER BY nombre ASC";
                                    $Pag = $mysqli->query($strQuery);
                                    while ($TPag = $Pag->fetch_array()) { ?>
                                        <option value="<?php echo $TPag['id']; ?>"><?php echo $TPag['nombre']; ?></option>
                                        <?php
                                    }
                                    $Pag->close();
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="monto"> Monto</label>
                            <input id="text" name="monto" type="number" class="form-control required" placeholder="00.00"  pattern="^\d+(?:\.\d{1,2})?$"/>
                        </div>
                    </div>
                </div>
            </section>
            <h3>Remesadora</h3>
            <section>
                <div class="row" style="padding-bottom:10px;">
				<?php
					$strQuery = "SELECT remesadora.id as id, remesadora.nombre as nombre, remesadora.imagen as imagen FROM remesadora,pagador WHERE pagador.id_remesadora=remesadora.id GROUP BY remesadora.nombre";
					$strBuscaC = $mysqli->query($strQuery);
					while ($remesadora = $strBuscaC->fetch_array()){
					?>
                    <div  class="col-sm-2 col-xs-2">
                        <div class="form-group" id="imas">
                            <label>
                            <input type="radio" name="remesadora" value="<?php echo $remesadora['id']; ?>" onchange="iniciarBuscaPagadores();"/>
							<img src="../remesador/<?php echo $remesadora['imagen']; ?>" height="100" width="100">
							<label>
                        </div>
                    </div>
					<?php } $strBuscaC->close(); ?>
                </div>
            </section>
			  <h3>Pagador</h3>
            <section>
			      <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="pagador"> Pagador</label>
                            <select id="pagador" name="pagador" class="pagador form-control required"></select>
                        </div>
                    </div>
					 <div class="col-md-12 text-center">
               <button type="submit" class="btn btn-info btn-raised"><i class="fa fa-save"></i> Guardar</button>
            </div>
                </div>
			</section>
			
        </div>
        </form>
    </div>
</div>