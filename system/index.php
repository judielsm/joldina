<?php
include ("../inc/config.php");
session_start();
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
?>
<!DOCTYPE html>
<html lang="es">
   <head>
      <?php 
	  include("../inc/header.php"); 
	  ?>
      <link href="../css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
      <link href="../css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
      <link href="../css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
      <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	  <link href="../css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />
      <link href="../css/core.css" rel="stylesheet" type="text/css" />
      <link href="../css/components.css" rel="stylesheet" type="text/css" />
	  <link href="../css/select.css" rel="stylesheet" type="text/css" />
	  <link href="../css/steps.css" rel="stylesheet" type="text/css" />
      <link href="../css/icons.css" rel="stylesheet" type="text/css" />
      <link href="../css/pages.css" rel="stylesheet" type="text/css" />
      <link href="../css/menu.css" rel="stylesheet" type="text/css" />
      <link href="../css/responsive.css" rel="stylesheet" type="text/css" /> 
	  <link href="../css/colm.css" rel="stylesheet" type="text/css" /> 
      <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
	  <script src="../js/jquery.min.js"></script>
      <script src="../js/modernizr.min.js"></script>
   </head>
   <body class="fixed-left">
      <script src="../js/clock.js"type="text/javascript"></script>
      <div id="wrapper">
         <div class="topbar">
            <div class="topbar-left">
               <a href="" class="logo"><img src="../img/brand.png"></a>
            </div>
            <div class="navbar navbar-default" role="navigation">
               <div class="container">
                  <ul class="nav navbar-nav navbar-left">
                     <li>
                        <button class="button-menu-mobile open-left waves-effect">
                        <i class="mdi mdi-altimeter"></i>
                        </button>
                     </li>
					 <li>
                        <button class="button-menu-mobile open-left waves-effect" id="btn-fullscreen">
                        <i class="glyphicon glyphicon-fullscreen"></i>
                        </button>
                     </li>
                     <li class="hidden-xs">
                        <a href="#" class="menu-item"><span id="liveclock"></span></a>
                     </li>
                  </ul>
                  <ul class="nav navbar-nav navbar-right">
                     <li class="dropdown user-box">
                        <a href="#" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                        @<?php echo $_SESSION["joldin_nom"];?>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
						   <li><a href="index.php?page=perfilActivo"><i class="ti-user m-r-5"></i> Perfil</a></li>
                           <li><a href="sysSalir.php"><i class="ti-power-off m-r-5"></i> Salir</a></li>
                        </ul>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
         <div class="left side-menu">
            <div class="sidebar-inner slimscrollleft">
               <?php include("../inc/menu.php"); ?>
            </div>
         </div>
         <div class="content-page">
            <div class="content">
               <div class="container">
                 <?php
						//Obtenemos el nombre del url
						$page = $_GET["page"];

						//Verificamos que no sea ninguno de los que estan excluidos
						if ($page == "" || $page == "index"){
							include ("sysHome.php");
							}else{

							//Verificamos que el archivo exista y parseamos los caracteres especiales
							if (file_exists("$page.php")){
								$page = htmlspecialchars(trim($_GET["page"]));
								$page = str_replace("<[^>]*>", "", $page);
								$page = str_replace(".*//", "", $page);

								//Si el archivo esta lo incluimos para ser mostrado
								include ("$page.php");
								}else{
								//Si no se encuentra mostramos el error 404
								include ("sysHome.php");
								}
							}
						?>
						<?php 
							if ($page == "" || $page == "index" || $page == "sysHome"){
								echo "&nbsp;";
							}else{
						?>
						<div align="center">
						<a href="index.php?page=sysHome" class="btn btn-info btn-sm w-sm waves-effect m-t-10 waves-light"><i class="mdi mdi-keyboard-backspace"></i> Volver</a>
						</div>
						<?php
							}
						?>
			  </div>
            </div>
         </div>
      </div>
      <script>
         var resizefunc = [];
      </script>
      
      <script src="../js/bootstrap.min.js"></script>
      <script src="../js/detect.js"></script>
      <script src="../js/fastclick.js"></script>
      <script src="../js/jquery.blockUI.js"></script>
      <script src="../js/waves.js"></script>
      <script src="../js/jquery.slimscroll.js"></script>
	  <script src="../js/jquery.select.min.js"></script>
      <script src="../js/jquery.select.es.min.js"></script>
	  <script src="../js/jquery.steps.min.js"></script>	  
	  <script src="../js/bootstrap-datepicker.min.js"></script> 
	  <script src="../js/parsley.min.js"></script>
      <script src="../js/jquery.dataTables.min.js"></script>
      <script src="../js/dataTables.bootstrap.js"></script>
      <script src="../js/dataTables.responsive.min.js"></script>
      <script src="../js/responsive.bootstrap.min.js"></script>
      <script src="../js/jquery.datatables.init.js"></script>
	  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCDmCCsjtLXbAVxqCl6vFZ938T_zwGyeI"></script>
	  <script src="../js/gmaps.min.js"></script>
      <script src="../js/jquery.core.js"></script>
      <script src="../js/jquery.app.js"></script>
   </body>
</html>