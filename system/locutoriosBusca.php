<?php
include ("../inc/config.php");

$sql = "SELECT
			pagos_locutorio.id AS id_lo,
			DATE_FORMAT(pagos_locutorio.fecha, '%d-%m-%Y') fecha_lo,
			locutorio.nombre AS locutorio_lo,
			sucursal.nombre AS sucursal_lo,
			pagos_locutorio.monto AS monto_lo
		FROM
			locutorio,
			pagos_locutorio,
			sucursal
		WHERE
			pagos_locutorio.id_locutorio = locutorio.id AND
			pagos_locutorio.id_sucursal = sucursal.id AND
			DATE_FORMAT(pagos_locutorio.fecha, '%d-%m-%Y') ='".date('d-m-Y')."'";

if($_GET['id'] !="100"){
	$sql .= " AND pagos_locutorio.id_sucursal='".$_GET['id']."'";
}

$result = $mysqli->query($sql);
$json = array();
while($row = $result->fetch_array()){
     $json['data'][] = $row;
}	

$result->close();		
echo json_encode($json);		

?>