<?php
if ($_SESSION["joldin_log"] == TRUE) {
} else {
    header("Location: ../index.php");
}
?>
<script type="text/javascript">
$selecPaises = null;
$selecPagador = null;
$(function() {
	iniciarBusquedaPais();
	iniciarBuscaPagadores();
});

function iniciarBusquedaPais() {
		$selecPaises = $('#pais').select2({
		tags:true,
        language: "es",
		allowClear: true,
        ajax: {
            url: 'enviosPais.php',
            dataType: 'json',
            processResults: function(data) {
                return {
                    results: data
                };
            },
			
        }
    });
}

function iniciarBuscaPagadores(){
		$("#pagador").val('').trigger('change');
		$selecPagador = $('#pagador').select2({
		tags:true,
        language: "es",
		allowClear: true,
		 placeholder: "Seleccione un pagador...",
        ajax: {
          url: "enviosPagador.php?id="+$('input:radio[name="remesadora"]:checked').val()+"&pais="+$('select[name=pais]').val(),
          dataType: 'json',
		  type:"GET",
          processResults: function (data) {
            return {
              results: data
            };
          },
        }
      });
}
</script>
<div class="row">
    <div class="col-xs-12">
        <div class="page-title-box">
            <h4 class="page-title">Datos Envío Express</h4>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="card-box">
		<form id="frm-envio" action="index.php?page=acciones&accion=enviosExpress" method="post" enctype="multipart/form-data">
		<input type="hidden" name="validator" id="validator" value="ok" />
		<input type="hidden" name="empleado" id="empleado" value="<?php echo $_SESSION["joldin_ide"];?>" />
		<input type="hidden" name="sucursal" id="sucursal" value="<?php echo $_SESSION["joldin_suc"];?>" />
                <div class="row">
                    <div class="col-md-3 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="fecha"> Fecha</label>
							<input id="fecha" name="fecha" type="date" class="form-control" value="<?php echo date("Y-m-d"); ?>" required>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="pais"> País</label>
                            <select id="pais" name="pais" class="pais form-control required" autocomplete="country-name"></select>
                        </div>
                    </div>
					 <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="tpago"> Tipo Pago</label>
                            <select id="tpago" name="tpago" class="form-control required">
                                <?php
                                    $strQuery = "SELECT * FROM tipo_pago ORDER BY nombre ASC";
                                    $Pag = $mysqli->query($strQuery);
                                    while ($TPag = $Pag->fetch_array()) { ?>
                                        <option value="<?php echo $TPag['id']; ?>"><?php echo $TPag['nombre']; ?></option>
                                        <?php
                                    }
                                    $Pag->close();
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="monto"> Monto</label>
                            <input type="number" class="form-control" name="monto" id="monto" placeholder="00.00"
                                   required min="0" step="0.01" pattern="^\d+(?:\.\d{1.2})?$"/>
                        </div>
                    </div>
                </div>
				 <div class="text-left"><strong>Remesadora</strong></div>
               <div class="row" style="padding-bottom:10px;">
				<?php
					$strQuery = "SELECT remesadora.id as id, remesadora.nombre as nombre, remesadora.imagen as imagen FROM remesadora,pagador WHERE pagador.id_remesadora=remesadora.id GROUP BY remesadora.nombre";
					$strBuscaC = $mysqli->query($strQuery);
					while ($remesadora = $strBuscaC->fetch_array()){
					?>
                    <div  class="col-sm-2 col-xs-2">
                        <div class="form-group" id="imas">
                            <label>
                            <input type="radio" name="remesadora" value="<?php echo $remesadora['id']; ?>" onchange="iniciarBuscaPagadores();"/>
							<img src="../remesador/<?php echo $remesadora['imagen']; ?>" height="100" width="100">
							<label>
                        </div>
                    </div>
					<?php } $strBuscaC->close(); ?>
                </div>
			      <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="pagador"> Pagador</label>
                            <select id="pagador" name="pagador" class="pagador form-control" required>
							<option value=""></option>
							</select>
                        </div>
                    </div>
                </div>
				         <div class="row mb-4">
            <div class="col-md-2 col-xs-1">&nbsp;</div>
            <div class="col-md-8 col-xs-10 text-center">
               <button type="submit" class="btn btn-info btn-raised"><i class="fa fa-save"></i> Guardar</button>
            </div>
            <div class="col-md-2 col-xs-1">&nbsp;</div>
         </div>
        </form>
    </div>
</div>