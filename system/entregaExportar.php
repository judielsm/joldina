<?php
include ("../inc/config.php");			

$fechaIni = $_POST['fecha_ini']; 
$fechaIni = explode('-', $fechaIni);
$fechaIni = $fechaIni[2].'-'.$fechaIni[1].'-'.$fechaIni[0];
$fechaFin = $_POST['fecha_fin']; 
$fechaFin = explode('-', $fechaFin);
$fechaFin = $fechaFin[2].'-'.$fechaFin[1].'-'.$fechaFin[0];
$idSucursal  = $_POST['sucursal']; 
$formatoTipo = $_POST['formato'];

if($idSucursal=="100"){
	
	$Query    = "SELECT
					entregas.id AS NUM,
					DATE_FORMAT(entregas.fecha, '%d-%m-%Y') FECHA,
					entregas.hora AS HORA,
					sucursal.nombre AS SUCURSAL,
					entregas.monto AS MONTO
				FROM
					entregas,
					sucursal
				WHERE
					entregas.id_sucursal = sucursal.id AND entregas.fecha BETWEEN('".$fechaIni."') AND ('".$fechaFin."') ORDER BY  NUM ASC";
	
}else{
		
	$Query    = "SELECT
					entregas.id AS NUM,
					DATE_FORMAT(entregas.fecha, '%d-%m-%Y') FECHA,
					entregas.hora AS HORA,
					sucursal.nombre AS SUCURSAL,
					entregas.monto AS MONTO
				FROM
					entregas,
					sucursal
				WHERE
					entregas.id_sucursal = sucursal.id AND entregas.fecha BETWEEN('".$fechaIni."') AND ('".$fechaFin."') AND entregas.id_sucursal = '".$idSucursal."' ORDER BY  NUM ASC";	
		
}
if($formatoTipo=="1"){
?>
<?php	
header('Content-type: application/vnd.ms-excel;charset=utf-8');
header('Content-Disposition: attachment; filename=entregas-'.date('d-m-Y').'.xls');

	$Reporte = $mysqli->query($Query); ?>

	<table border="1" cellpadding="2" cellspacing="0" width="100%"> 
	  <tr>
		<td bgcolor="#C4D79B" align="center"><b>NUM</b></td>
		<td bgcolor="#C4D79B" align="center"><b>FECHA</b></td>
		<td bgcolor="#C4D79B" align="center"><b>HORA</b></td>
		<td bgcolor="#C4D79B" align="center"><b>SUCURSAL</b></td>
		<td bgcolor="#C4D79B" align="center"><b>MONTO</b></td>
	  </tr>
	<?php while($row = $Reporte->fetch_array()){  
			if ($colordefila==0){
				$color= "#ffffff";
				$colordefila=1;
			 }else{
				$color="#DCE6F1";
				$colordefila=0;
			 }
	?>
	  <tr>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['NUM'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['FECHA'];?></td>	
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['HORA'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['SUCURSAL'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['MONTO'], 2, ',', '.');?></td>
<?php	} ?>
	</table>
<?php	
}else{

header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename=entregas-'.date('d-m-Y').'.csv');
?>
NUM;FECHA;HORA;SUCURSAL;MONTO
<?php
	$Reporte = $mysqli->query($Query);
	while($row = $Reporte->fetch_array()){   
echo$row['NUM'].";".$row['FECHA'].";".$row['HORA'].";".number_format($row['MONTO'], 2, ',', '.')."\n"; }		
}
?>
