<?php
include ("../inc/config.php");
$id = $_POST['id'];

$strQuery = "SELECT
				clientes.id id,
				clientes.tipo_doc, tipo_doc,
				clientes.documento documento,
				clientes.nombres nombres,
				clientes.direccion direccion,
				clientes.codigo_postal codigo_postal,
				clientes.residente residente,
				clientes.ciudad ciudad,
				clientes.telefono telefono,
				clientes.email email,
				clientes.nacionalidad nacionalidad,
				clientes.ocupacion ocupacion,
				clientes.archivo archivo,
				paises.id pais_id,
				paises.iso pais_iso,
				paises.nombre pais_nombre
			FROM
				 clientes
			LEFT OUTER JOIN
				paises
			ON
				paises.id = clientes.nacionalidad
			WHERE
				clientes.id='".$id."'";

$clientes= $mysqli->query($strQuery);
$datos = array('cant'=>0);

if($clientes->num_rows > 0){
    $datos = array("cant"=>1,"datos"=>$clientes->fetch_array());
}

$clientes->close();

$doc = trim($datos['datos']['documento']);

$banSql = "SELECT
				*
			FROM
				baneados
			WHERE	
				dni = '$doc'
";


$check = $mysqli->query($banSql);


if($check->num_rows > 0){
    $datos = array("cant"=>0 , 'banned' => true);
}

$check->close();

echo json_encode($datos);

?>
