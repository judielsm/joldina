<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
?>
<script type="text/javascript">
$(document).ready(function() {
    $('#datatable-responsive').dataTable({
        ajax: 'clientesBusca.php',
        columns: [
		{data: "cli_nombre",className:"text-left"},
        {data: "cli_tipo"},
        {data: "cli_doc"},
        {data: "cli_ciudad"},
        {searchable: false,sortable: false,render: function(data, type, row, meta) {
		return '<a href="index.php?page=clientesDetalle&id=' + row[0] + '" class="btn btn-icon waves-effect btn-info btn-xs" title="Ver"><i class="typcn typcn-eye"></i> </a>&nbsp;' +
               '<a href="index.php?page=clientesEdita&id=' + row[0] + '" class="btn btn-icon waves-effect btn-info btn-xs" title="Editar"> <i class="typcn typcn-pencil"></i> </a>'
                }
            },
        ]

    });
});

window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function() {
        $(this).remove();
    });
}, 5000);
</script>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Clientes</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<?php
   $strAccion = $_GET['menssage'];
   switch ($strAccion) {
   case "add":
   ?>
<div class="row">
   <div class="alert alert-success col-md-12" align="center">
      <i class="mdi mdi-check-all"></i> El Cliente Fue Agregado.
   </div>
</div>
<?php
   break;
   case "edit":
   ?>
<div class="row">
   <div class="alert alert-warning col-md-12" align="center">
      <i class="mdi mdi-alert"></i> El Cliente Fue Editado.
   </div>
</div>
<?php
   break;
   case "del":
   ?>
<div class="row">
   <div class="alert alert-danger col-md-12" align="center">
      <i class="mdi mdi-block-helper"></i> El Cliente Fue Eliminado.
   </div>
</div>
<?php
   break;
   case "dup":
   ?>
<div class="row">
   <div class="alert alert-danger col-md-12" align="center">
      <i class="mdi mdi-block-helper"></i> Cliente ya existe.
   </div>
</div>
<?php
   break;
   case "mor":
   ?>
<div class="row">
   <div class="alert alert-danger col-md-12" align="center">
      <i class="mdi mdi-block-helper"></i> Este Cliente Tiene Envios Registrados.
   </div>
</div>
<?php
   break;
   case "error":
   ?>
<div class="row">
   <div class="alert alert-danger col-md-12" align="center">
      <i class="mdi mdi-block-helper"></i> No Se Realizo Acción.
   </div>
</div>
<?php
   break;
   }
   ?>
<div class="row">
   <div class="card-box table-responsive">
      <div class="row">
         <div class="col-sm-12">
           <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap text-center text-tabla" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th class="text-center">Nombres</th>
					<th class="text-center">Tipo Doc</th>
					<th class="text-center">Documento</th>
					<th class="text-center">Ciudad</th>
					<th class="text-center">Opciones</th>
				</tr>
			</thead>
		  </table>
         </div>
      </div>
   </div>
</div>