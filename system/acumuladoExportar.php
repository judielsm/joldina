<?php
include ("../inc/config.php");			

$fecha = $_POST['fecha']; 
$fecha = explode('-', $fecha);
$fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
$sucursalID = $_POST['sucursal'];
$where = "  WHERE id='".$sucursalID."'";
if($sucursalID == 100) $where = " ";
$formatoTipo = $_POST['formato'];

$SucB = "SELECT nombre FROM sucursal". $where;
$SucE = $mysqli->query($SucB);
$Sucu = $SucE->fetch_array();
$SucE->close();
$NSucursal = $Sucu['nombre'];
if($sucursalID == 100)$NSucursal = "Todas";


if($formatoTipo==="1"){
?>
<?php	
header('Content-type: application/vnd.ms-excel;charset=utf-8');
header('Content-Disposition: attachment; filename=acumulado-'.date('d-m-Y').'.xls');
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td align="right" width="50%" style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'>Fecha: <b><?php echo $_POST['fecha'];?></b></td>
    <td align="left" width="50%" style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'>Sucursal: <b><?php echo $NSucursal; ?></b></td>
</tr>
  <tr>
    <td colspan="2" width="100%">
     <table width="100%" border="1" align='center' cellpadding="0" cellspacing="0" class='table' style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'>
                                                        <thead>
                                                            <tr  bgcolor="#DCE6F1">
																<th align="left" width="22%">Remesadora</th>
																<th align="center" width="13%">Cant</th>
																<th align="center" width="13%">Envios</th>
                                                                <th align="center" width="13%">Cant</th>
                                                                <th align="center" width="13%">Pagos</th>
                                                                <th align="center" width="13%">Cant</th>
                                                                <th align="center" width="13%">Devoluicones</th>
                                                            </tr>
														</thead>
                                                        <tbody>
														<?php
                                                        $where1 = "  AND id_sucursal='".$sucursalID."'";
                                                        $where2 = "  AND pagos.id_sucursal='".$sucursalID."'";
                                                        if($sucursalID == 100) {
                                                            $where1 = "";
                                                            $where2 = "";
                                                        }

															$SumaEnvios = "SELECT
																				remesadora.nombre,
																				IFNULL(a.cant, 0) AS cantEnvio,
																				remesadora.id,
																				IFNULL(a.total, 0) AS totalEnvio,
																				IFNULL(b.cant, 0) AS cantDevo,
																				IFNULL(b.total, 0) AS totalDevo,
																				IFNULL(COUNT(pagos.id_remesadora),0) AS cantPagos,
																				IFNULL(SUM(pagos.monto),0) AS totalPagos
																			FROM
																				remesadora
																			LEFT JOIN(
																				SELECT COUNT(*) AS cant,
																					SUM(monto) AS total,
																					id_remesadora AS idreme,
																					devolucion
																				FROM
																					envios
																				WHERE
																					fecha BETWEEN('".$fecha."') AND('".$fecha."')".$where1." AND devolucion = 0
																				GROUP BY
																					envios.id_remesadora) AS a
																			ON
																				a.idreme = remesadora.id
																			LEFT JOIN(
																				SELECT COUNT(*) AS cant,
																					SUM(monto) AS total,
																					id_remesadora AS idreme,
																					devolucion
																				FROM
																					envios
																				WHERE
																					fecha BETWEEN('".$fecha."') AND('".$fecha."') ".$where1." AND devolucion = 1
																				GROUP BY
																					envios.id_remesadora) AS b
																			ON
																				b.idreme = remesadora.id
																			LEFT JOIN pagos ON pagos.id_remesadora = remesadora.id AND 
																			pagos.fecha BETWEEN('".$fecha."') AND('".$fecha."')   ".$where2."
																			GROUP BY
																				remesadora.id";
															$Envios = $mysqli->query($SumaEnvios);
															while($Envio = $Envios->fetch_array()){
														?>
                                                            <tr>
																<td align="left" bgcolor='#C4D79B'><b><?php echo $Envio['nombre']; ?></b></td>
																<td align="center"><?php echo $Envio['cantEnvio']; ?></td>
																<td align="center"><?php echo number_format($Envio['totalEnvio'], 2, ',', '.'); ?></td>
                                                                <td align="center"><?php echo $Envio['cantPagos']; ?></td>
                                                                <td align="center"><?php echo number_format($Envio['totalPagos'], 2, ',', '.'); ?></td>
                                                                <td align="center"><?php echo $Envio['cantDevo']; ?></td>
                                                                <td align="center"><?php echo number_format($Envio['totalDevo'], 2, ',', '.'); ?></td>
                                                            </tr>
													<?php }
														$Envios->close();				
													?>
                                                        </tbody>
                                                    </table>
    </td>
  </tr>
  <tr>
    <td width="50%" valign="top">
    <table width="100%" border="1" align='center' cellpadding="0" cellspacing="0" class='table' style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'>
                                                        <thead>
                                                            <tr  bgcolor="#DCE6F1">
																<th width="40%" align="left">Locutorio</th>
																<th width="30%" align="center">Cantidad</th>
																<th width="30%" align="center">Monto</th>
                                                            </tr>
														</thead>
                                                        <tbody>
														<?php
                                                        $where3 = "  AND pagos_locutorio.id_sucursal='".$sucursalID."'";
                                                        if($sucursalID == 100) {
                                                            $where3 = "";

                                                        }
															$SumaServicios ="SELECT
																					IFNULL(COUNT(pagos_locutorio.id),0) AS cantidad,
																					IFNULL(SUM(pagos_locutorio.monto),0) AS monto,
																					locutorio.nombre AS locutorio
																				FROM
																					locutorio
																				LEFT JOIN pagos_locutorio ON pagos_locutorio.id_locutorio = locutorio.id AND 
																				pagos_locutorio.fecha BETWEEN('".$fecha."') AND('".$fecha."') 
																				".$where3."
																				GROUP BY
																					locutorio.id";
																					
															$Servicios = $mysqli->query($SumaServicios);
															while($Servicio = $Servicios->fetch_array()){
														?>
                                                            <tr>
																<td align="left" bgcolor='#C4D79B'><b><?php echo $Servicio['locutorio']; ?></b></td>
																<td align="center"><?php echo $Servicio['cantidad']; ?></td>
																<td align="center"><?php echo number_format($Servicio['monto'], 2, ',', '.'); ?></td>
                                                            </tr>
															<?php
																}
															$Servicios->close();
															?>
                                                        </tbody>
                                                    </table>
													<br/>
		<table width="100%" border="1" align='center' cellpadding="0" cellspacing="0" class='table' style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'><thead>
                                                             <tr  bgcolor="#DCE6F1">
																<th width="40%" align="left">Divisas</th>
																<th width="30%" align="center">Cambio</th>
																<th width="30%" align="center">Monto</th>
                                                            </tr>
														</thead>
                                                        <tbody>
														<?php

															$SumaDivisas ="SELECT
																				divisas.divisa divisa,
																				divisas.cambio cambio,
																				SUM(ventas.monto) monto,
																				IFNULL(SUM(ventas.monto),0) as total
																			FROM
																				divisas,
																				ventas
																			WHERE
																				ventas.fecha = '".$fecha."' AND 
																				ventas.id_sucursal = '".$sucursalID."' AND 
																				ventas.id_divisa = divisas.id
																			GROUP BY
																				divisas.id";
																					
															$Divisas = $mysqli->query($SumaDivisas);
															while($Divisa = $Divisas->fetch_array()){
														?>
                                                            <tr>
																<td class="text-left" bgcolor='#C4D79B'><?php echo $Divisa['divisa']; ?></td>
																<td class="text-center"><?php echo $Divisa['cambio']; ?></td>
																<td class="text-center"><?php echo number_format($Divisa['monto'], 2, ',', '.'); ?> €</td>
                                                            </tr>
															<?php
																}
															$Divisas->close();
															?>
														</tbody>
                                                    </table>	
													
													 <br>

						<table width="100%" border="1" align='center' cellpadding="0" cellspacing="0" class='table' style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'>
                                                        <thead>
                                                            <tr  bgcolor="#DCE6F1">
																<th class="text-left" width="50%">Productos</th>
																<th class="text-center" width="50%">Monto</th>
                                                            </tr>
														</thead>
                                                        <tbody>
														<?php

															$SumaPOS ="SELECT
																			COUNT(
																				productos_ventas_detalle.id_venta
																			) productos,
																			productos_ventas.total monto
																		FROM
																			productos_ventas,
																			productos_ventas_detalle
																		WHERE
																			productos_ventas.id = productos_ventas_detalle.id_venta AND
																			productos_ventas.fecha = '".$fecha."' AND 
																			productos_ventas.id_sucursal = '".$sucursalID."' 
																			GROUP BY
																				productos_ventas.id";
																					
															$VPOS = $mysqli->query($SumaPOS);
															while($POS = $VPOS->fetch_array()){
														?>
                                                            <tr>
																<td class="text-left"><?php echo $POS['productos']; ?></td>
																<td class="text-center"><?php echo number_format($POS['monto'], 2, ',', '.'); ?> €</td>
                                                            </tr>
															<?php
																}
															$VPOS->close();
															?>
														</tbody>
                                                    </table>
    </td>
    <td width="50%">
    <?php
    $where4 = "  AND cierres.id_sucursal='".$sucursalID."'";
    if($sucursalID == 100) {
        $where4 = "";
    }
												 $CierreB = "SELECT
																cierres.id as id,
																cierres.fecha as fecha,
																empleados.nombres AS empleado,
																sucursal.nombre AS sucursal,
																cono_monetario.total AS total,
																IFNULL(cono_monetario.cantidad_500,0) 0cantidad_500,
																IFNULL(500.00 * cono_monetario.cantidad_500,0) cantidad_500,
																IFNULL(cono_monetario.cantidad_200,0) 0cantidad_200,
																IFNULL(200.00 * cono_monetario.cantidad_200,0) cantidad_200,
																IFNULL(cono_monetario.cantidad_100,0) 0cantidad_100,
																IFNULL(100.00 * cono_monetario.cantidad_100,0) cantidad_100,
																IFNULL(cono_monetario.cantidad_50,0) 0cantidad_50,
																IFNULL(50.00 * cono_monetario.cantidad_50,0) cantidad_50,
																IFNULL(cono_monetario.cantidad_20,0) 0cantidad_20,
																IFNULL(20.00 * cono_monetario.cantidad_20,0) cantidad_20,
																IFNULL(cono_monetario.cantidad_10,0) 0cantidad_10,
																IFNULL(10.00 * cono_monetario.cantidad_10,0) cantidad_10,
																IFNULL(cono_monetario.cantidad_5,0) 0cantidad_5,
																IFNULL(5.00 * cono_monetario.cantidad_5,0) cantidad_5,
																IFNULL(cono_monetario.cantidad_2,0) 0cantidad_2,
																IFNULL(2.00 * cono_monetario.cantidad_2,0) cantidad_2,
																IFNULL(cono_monetario.cantidad_1,0) 0cantidad_1,
																IFNULL(1.00 * cono_monetario.cantidad_1,0) cantidad_1,
																IFNULL(cono_monetario.cantidad_050,0) 0cantidad_050,
																IFNULL(0.50 * cono_monetario.cantidad_050,0) cantidad_050,
																IFNULL(cono_monetario.cantidad_020,0) 0cantidad_020,
																IFNULL(0.20 * cono_monetario.cantidad_020,0) cantidad_020,
																IFNULL(cono_monetario.cantidad_010,0) 0cantidad_010,
																IFNULL(0.10 * cono_monetario.cantidad_010,0) cantidad_010,
																IFNULL(cono_monetario.cantidad_005,0) 0cantidad_005,
																IFNULL(0.05 * cono_monetario.cantidad_005,0) cantidad_005,
																IFNULL(cono_monetario.cantidad_002,0) 0cantidad_002,
																IFNULL(0.02 * cono_monetario.cantidad_002,0) cantidad_002,
																IFNULL(cono_monetario.cantidad_001,0) 0cantidad_001,
																IFNULL(0.01 * cono_monetario.cantidad_001,0) cantidad_001
															FROM
																cierres,
																cono_monetario,
																sucursal,
																empleados
															WHERE
																cierres.id = cono_monetario.id_cierre AND 
																cierres.id_sucursal = sucursal.id AND
																cierres.id_empleado = empleados.id AND 
																cierres.fecha = '".$fecha."' 
																".$where4;
																
												$Cierres = $mysqli->query($CierreB);
												$Cierre = $Cierres->fetch_array();
												$Cierres->close();
											?>
    <table width="100%" border="1" align='center' cellpadding="0" cellspacing="0" class='table' style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'>
										  <tr align="center"  bgcolor="#DCE6F1">
											<td><strong>VALOR</strong></td>
											<td><strong>CANTIDAD</strong></td>
											<td><strong>TOTAL</strong></td>
										  </tr>
										  <tr align="center">
											<td><strong>500,00 </strong></td>
											<td><?php echo number_format($Cierre['0cantidad_500'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_500'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>200,00 </strong></td>
											<td><?php echo number_format($Cierre['0cantidad_200'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_200'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>100,00 </strong></td>
											<td><?php echo number_format($Cierre['0cantidad_100'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_100'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>50,00 </strong></td>
											<td><?php echo number_format($Cierre['0cantidad_50'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_50'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>20,00 </strong></td>
											<td><?php echo number_format($Cierre['0cantidad_20'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_20'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>10,00 </strong></td>
											<td><?php echo number_format($Cierre['0cantidad_10'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_10'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>5,00 </strong></td>
											<td><?php echo number_format($Cierre['0cantidad_5'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_5'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>2,00 </strong></td>
											<td><?php echo number_format($Cierre['0cantidad_2'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_2'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>1,00 </strong></td>
											<td><?php echo number_format($Cierre['0cantidad_1'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_1'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,50 </strong></td>
											<td><?php echo number_format($Cierre['0cantidad_050'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_050'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,20 </strong></td>
											<td><?php echo number_format($Cierre['0cantidad_020'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_020'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,10 </strong></td>
											<td><?php echo number_format($Cierre['0cantidad_010'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_010'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,05 </strong></td>
											<td><?php echo number_format($Cierre['0cantidad_005'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_005'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,02 </strong></td>
											<td><?php echo number_format($Cierre['0cantidad_002'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_002'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,01 </strong></td>
											<td><?php echo number_format($Cierre['0cantidad_001'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_001'], 2, ',', '.'); ?></td>
										  </tr>
										</table>
    </td>
  </tr>
  </table>
<?php 
}else{
	include("../inc/header.php");
?>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 9px;
}

</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td align="right" width="50%" style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'>Fecha: <b><?php echo $_POST['fecha'];?></b></td>
    <td align="left" width="50%" style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'>Sucursal: <b><?php echo $NSucursal; ?></b></td>
</tr>
  <tr>
    <td colspan="2" width="100%">
     <table width="100%" border="1" align='center' cellpadding="0" cellspacing="0" class='table' style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'>
                                                        <thead>
                                                            <tr  bgcolor="#DCE6F1">
																<th align="left" width="22%">Remesadora</th>
																<th align="center" width="13%">Cant</th>
																<th align="center" width="13%">Envios</th>
                                                                <th align="center" width="13%">Cant</th>
                                                                <th align="center" width="13%">Pagos</th>
                                                                <th align="center" width="13%">Cant</th>
                                                                <th align="center" width="13%">Devoluicones</th>
                                                            </tr>
														</thead>
                                                        <tbody>
														<?php
                                                        $where1 = "  AND id_sucursal='".$sucursalID."'";
                                                        $where2 = "  AND pagos.id_sucursal='".$sucursalID."'";
                                                        if($sucursalID == 100) {
                                                            $where1 = "";
                                                            $where2 = "";
                                                        }

															$SumaEnvios = "SELECT
																				remesadora.nombre,
																				IFNULL(a.cant, 0) AS cantEnvio,
																				remesadora.id,
																				IFNULL(a.total, 0) AS totalEnvio,
																				IFNULL(b.cant, 0) AS cantDevo,
																				IFNULL(b.total, 0) AS totalDevo,
																				IFNULL(COUNT(pagos.id_remesadora),0) AS cantPagos,
																				IFNULL(SUM(pagos.monto),0) AS totalPagos
																			FROM
																				remesadora
																			LEFT JOIN(
																				SELECT COUNT(*) AS cant,
																					SUM(monto) AS total,
																					id_remesadora AS idreme,
																					devolucion
																				FROM
																					envios
																				WHERE
																					fecha BETWEEN('".$fecha."') AND('".$fecha."') ".$where1." AND devolucion = 0
																				GROUP BY
																					envios.id_remesadora) AS a
																			ON
																				a.idreme = remesadora.id
																			LEFT JOIN(
																				SELECT COUNT(*) AS cant,
																					SUM(monto) AS total,
																					id_remesadora AS idreme,
																					devolucion
																				FROM
																					envios
																				WHERE
																					fecha BETWEEN('".$fecha."') AND('".$fecha."') ".$where1." AND devolucion = 1
																				GROUP BY
																					envios.id_remesadora) AS b
																			ON
																				b.idreme = remesadora.id
																			LEFT JOIN pagos ON pagos.id_remesadora = remesadora.id AND 
																			pagos.fecha BETWEEN('".$fecha."') AND('".$fecha."') ".$where2."
																			GROUP BY
																				remesadora.id";
															$Envios = $mysqli->query($SumaEnvios);
															while($Envio = $Envios->fetch_array()){
														?>
                                                            <tr>
																<td align="left" bgcolor='#C4D79B'><b><?php echo $Envio['nombre']; ?></b></td>
																<td align="center"><?php echo $Envio['cantEnvio']; ?></td>
																<td align="center"><?php echo number_format($Envio['totalEnvio'], 2, ',', '.'); ?> €</td>
                                                                <td align="center"><?php echo $Envio['cantPagos']; ?></td>
                                                                <td align="center"><?php echo number_format($Envio['totalPagos'], 2, ',', '.'); ?> €</td>
                                                                <td align="center"><?php echo $Envio['cantDevo']; ?></td>
                                                                <td align="center"><?php echo number_format($Envio['totalDevo'], 2, ',', '.'); ?> €</td>
                                                            </tr>
													<?php }
														$Envios->close();				
													?>
                                                        </tbody>
                                                    </table>
    </td>
  </tr>
  <tr>
    <td width="50%" valign="top">
    <table width="100%" border="1" align='center' cellpadding="0" cellspacing="0" class='table' style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'>
                                                        <thead>
                                                            <tr  bgcolor="#DCE6F1">
																<th width="40%" align="left">Locutorio</th>
																<th width="30%" align="center">Cantidad</th>
																<th width="30%" align="center">Monto</th>
                                                            </tr>
														</thead>
                                                        <tbody>
														<?php
                                                        $where3 = "  AND pagos_locutorio.id_sucursal='".$sucursalID."'";

                                                        if($sucursalID == 100) {
                                                            $where3 = "";

                                                        }
															$SumaServicios ="SELECT
																					IFNULL(COUNT(pagos_locutorio.id),0) AS cantidad,
																					IFNULL(SUM(pagos_locutorio.monto),0) AS monto,
																					locutorio.nombre AS locutorio
																				FROM
																					locutorio
																				LEFT JOIN pagos_locutorio ON pagos_locutorio.id_locutorio = locutorio.id AND 
																				pagos_locutorio.fecha BETWEEN('".$fecha."') AND('".$fecha."')
																				".$where3."
																				GROUP BY
																					locutorio.id";
																					
															$Servicios = $mysqli->query($SumaServicios);
															while($Servicio = $Servicios->fetch_array()){
														?>
                                                            <tr>
																<td align="left" bgcolor='#C4D79B'><b><?php echo $Servicio['locutorio']; ?></b></td>
																<td align="center"><?php echo $Servicio['cantidad']; ?></td>
																<td align="center"><?php echo number_format($Servicio['monto'], 2, ',', '.'); ?> €</td>
                                                            </tr>
															<?php
																}
															$Servicios->close();
															?>
                                                        </tbody>
                                                    </table>
													
													<br/>
		<table width="100%" border="1" align='center' cellpadding="0" cellspacing="0" class='table' style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'><thead>
                                                             <tr  bgcolor="#DCE6F1">
																<th width="40%" align="left">Divisas</th>
																<th width="30%" align="center">Cambio</th>
																<th width="30%" align="center">Monto</th>
                                                            </tr>
														</thead>
                                                        <tbody>
														<?php

															$SumaDivisas ="SELECT
																				divisas.divisa divisa,
																				divisas.cambio cambio,
																				SUM(ventas.monto) monto,
																				IFNULL(SUM(ventas.monto),0) as total
																			FROM
																				divisas,
																				ventas
																			WHERE
																				ventas.fecha = '".$fecha."' AND 
																				ventas.id_sucursal = '".$sucursalID."' AND 
																				ventas.id_divisa = divisas.id
																			GROUP BY
																				divisas.id";
																					
															$Divisas = $mysqli->query($SumaDivisas);
															while($Divisa = $Divisas->fetch_array()){
														?>
                                                            <tr>
																<td class="text-left" bgcolor='#C4D79B'><?php echo $Divisa['divisa']; ?></td>
																<td class="text-center"><?php echo $Divisa['cambio']; ?></td>
																<td class="text-center"><?php echo number_format($Divisa['monto'], 2, ',', '.'); ?> €</td>
                                                            </tr>
															<?php
																}
															$Divisas->close();
															?>
														</tbody>
                                                    </table>	
													
													 <br>

						<table width="100%" border="1" align='center' cellpadding="0" cellspacing="0" class='table' style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'>
                                                        <thead>
                                                            <tr  bgcolor="#DCE6F1">
																<th class="text-left" width="50%">Productos</th>
																<th class="text-center" width="50%">Monto</th>
                                                            </tr>
														</thead>
                                                        <tbody>
														<?php

															$SumaPOS ="SELECT
																			COUNT(
																				productos_ventas_detalle.id_venta
																			) productos,
																			productos_ventas.total monto
																		FROM
																			productos_ventas,
																			productos_ventas_detalle
																		WHERE
																			productos_ventas.id = productos_ventas_detalle.id_venta AND
																			productos_ventas.fecha = '".$fecha."' AND 
																			productos_ventas.id_sucursal = '".$sucursalID."' 
																			GROUP BY
																				productos_ventas.id";
																					
															$VPOS = $mysqli->query($SumaPOS);
															while($POS = $VPOS->fetch_array()){
														?>
                                                            <tr>
																<td class="text-left"><?php echo $POS['productos']; ?></td>
																<td class="text-center"><?php echo number_format($POS['monto'], 2, ',', '.'); ?> €</td>
                                                            </tr>
															<?php
																}
															$VPOS->close();
															?>
														</tbody>
                                                    </table>
													
													
    </td>
    <td width="50%">
    <?php
    $where4 = "  AND cierres.id_sucursal='".$sucursalID."'";
    if($sucursalID == 100) {
        $where4 = "";
    }
												 $CierreB = "SELECT
																cierres.id as id,
																cierres.fecha as fecha,
																empleados.nombres AS empleado,
																sucursal.nombre AS sucursal,
																cono_monetario.total AS total,
																IFNULL(cono_monetario.cantidad_500,0) 0cantidad_500,
																IFNULL(500.00 * cono_monetario.cantidad_500,0) cantidad_500,
																IFNULL(cono_monetario.cantidad_200,0) 0cantidad_200,
																IFNULL(200.00 * cono_monetario.cantidad_200,0) cantidad_200,
																IFNULL(cono_monetario.cantidad_100,0) 0cantidad_100,
																IFNULL(100.00 * cono_monetario.cantidad_100,0) cantidad_100,
																IFNULL(cono_monetario.cantidad_50,0) 0cantidad_50,
																IFNULL(50.00 * cono_monetario.cantidad_50,0) cantidad_50,
																IFNULL(cono_monetario.cantidad_20,0) 0cantidad_20,
																IFNULL(20.00 * cono_monetario.cantidad_20,0) cantidad_20,
																IFNULL(cono_monetario.cantidad_10,0) 0cantidad_10,
																IFNULL(10.00 * cono_monetario.cantidad_10,0) cantidad_10,
																IFNULL(cono_monetario.cantidad_5,0) 0cantidad_5,
																IFNULL(5.00 * cono_monetario.cantidad_5,0) cantidad_5,
																IFNULL(cono_monetario.cantidad_2,0) 0cantidad_2,
																IFNULL(2.00 * cono_monetario.cantidad_2,0) cantidad_2,
																IFNULL(cono_monetario.cantidad_1,0) 0cantidad_1,
																IFNULL(1.00 * cono_monetario.cantidad_1,0) cantidad_1,
																IFNULL(cono_monetario.cantidad_050,0) 0cantidad_050,
																IFNULL(0.50 * cono_monetario.cantidad_050,0) cantidad_050,
																IFNULL(cono_monetario.cantidad_020,0) 0cantidad_020,
																IFNULL(0.20 * cono_monetario.cantidad_020,0) cantidad_020,
																IFNULL(cono_monetario.cantidad_010,0) 0cantidad_010,
																IFNULL(0.10 * cono_monetario.cantidad_010,0) cantidad_010,
																IFNULL(cono_monetario.cantidad_005,0) 0cantidad_005,
																IFNULL(0.05 * cono_monetario.cantidad_005,0) cantidad_005,
																IFNULL(cono_monetario.cantidad_002,0) 0cantidad_002,
																IFNULL(0.02 * cono_monetario.cantidad_002,0) cantidad_002,
																IFNULL(cono_monetario.cantidad_001,0) 0cantidad_001,
																IFNULL(0.01 * cono_monetario.cantidad_001,0) cantidad_001
															FROM
																cierres,
																cono_monetario,
																sucursal,
																empleados
															WHERE
																cierres.id = cono_monetario.id_cierre AND 
																cierres.id_sucursal = sucursal.id AND
																cierres.id_empleado = empleados.id AND 
																cierres.fecha = '".$fecha."' 
																".$where4;
																
												$Cierres = $mysqli->query($CierreB);
												$Cierre = $Cierres->fetch_array();
												$Cierres->close();
											?>
    <table width="100%" border="1" align='center' cellpadding="0" cellspacing="0" class='table' style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'>
										  <tr align="center"  bgcolor="#DCE6F1">
											<td><strong>VALOR</strong></td>
											<td><strong>CANTIDAD</strong></td>
											<td><strong>TOTAL</strong></td>
										  </tr>
										  <tr align="center">
											<td><strong>500,00 €</strong></td>
											<td><?php echo number_format($Cierre['0cantidad_500'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_500'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>200,00 €</strong></td>
											<td><?php echo number_format($Cierre['0cantidad_200'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_200'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>100,00 €</strong></td>
											<td><?php echo number_format($Cierre['0cantidad_100'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_100'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>50,00 €</strong></td>
											<td><?php echo number_format($Cierre['0cantidad_50'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_50'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>20,00 €</strong></td>
											<td><?php echo number_format($Cierre['0cantidad_20'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_20'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>10,00 €</strong></td>
											<td><?php echo number_format($Cierre['0cantidad_10'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_10'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>5,00 €</strong></td>
											<td><?php echo number_format($Cierre['0cantidad_5'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_5'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>2,00 €</strong></td>
											<td><?php echo number_format($Cierre['0cantidad_2'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_2'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>1,00 €</strong></td>
											<td><?php echo number_format($Cierre['0cantidad_1'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_1'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,50 €</strong></td>
											<td><?php echo number_format($Cierre['0cantidad_050'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_050'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,20 €</strong></td>
											<td><?php echo number_format($Cierre['0cantidad_020'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_020'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,10 €</strong></td>
											<td><?php echo number_format($Cierre['0cantidad_010'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_010'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,05 €</strong></td>
											<td><?php echo number_format($Cierre['0cantidad_005'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_005'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,02 €</strong></td>
											<td><?php echo number_format($Cierre['0cantidad_002'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_002'], 2, ',', '.'); ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,01 €</strong></td>
											<td><?php echo number_format($Cierre['0cantidad_001'], 2, ',', '.'); ?></td>
											<td><?php echo number_format($Cierre['cantidad_001'], 2, ',', '.'); ?></td>
										  </tr>
										</table>
    </td>
  </tr>
  <tr>
    <td colspan="2" valign="top" align="center" style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'><a href="javascript:window.close();" style="text-decoration:none;color:black;"><b>Regresar</b></a> </td>
  </tr>
  </table>

<?php 
} 
?>