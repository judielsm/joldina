<?php
   if($_SESSION["joldin_log"] == TRUE) {
   } else {
   header("Location: ../index.php");
   }
?>
<script>
   saldoSucursal();
   
   function saldoSucursal(){
	   <?php if($_SESSION["joldin_rol"] =="1"){  ?>
		   var id = $('#sucursal option:selected').val();
		<?php }else{ ?> 
		   var id = <?php echo $_SESSION["joldin_suc"]; ?>;
		<?php } ?>
		  var dataString = 'id=' + id;
           $.ajax({
               url: 'sucursalSaldo.php',
               dataType: "json",
               data: dataString,
               cache: false,
               success: function(dat) {
				   console.log(dat)
                   if (dat=="") {
   					$("#envios").text("");
					$("#devoluciones").text("");
   					$("#pagos").text("");
   					$("#locutorios").text("");
					$("#divisas").text("");
					$("#ventas").text("");
					$("#entregas").text("");
   					$("#disponible").text("");
   					} else {
                    $("#envios").text(dat.envios);
					$("#devoluciones").text(dat.devoluciones);
   					$("#pagos").text(dat.pagos);
   					$("#locutorios").text(dat.locutorios);
					$("#divisas").text(dat.divisas);
					$("#ventas").text(dat.ventas);
					$("#entregas").text(dat.entregas);
   					$("#disponible").text(dat.disponible);
                   }
               }
           });
   }
</script>
<body onload="saldoSucursal();">
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Panel de Control</h4>
         <div class="clearfix">
         </div>
      </div>
   </div>
</div>
 <?php if($_SESSION["joldin_rol"]=="2"){ ?>
<div class="row">
   <div class="col-md-3">
      <div class="card-box">
         <h4 class="header-title text-center">Envios</h4>
         <div class="text-center">
            <a href="index.php?page=enviosNuevo" class="btn btn-info waves-effect waves-light m-b-5"> <i class="fa fa-plus"></i> <span>Nuevo</span> </a>
			<a href="index.php?page=enviosNuevoExpress" class="btn btn-info waves-effect waves-light m-b-5"> <i class="fa fa-plus"></i> <span>Express</span> </a>
         </div>
      </div>
   </div>
   <div class="col-md-3">
      <div class="card-box">
         <h4 class="header-title text-center">Pagos</h4>
         <div class="text-center">
            <a href="index.php?page=pagosNuevo" class="btn btn-info waves-effect waves-light m-b-5"> <i class="fa fa-plus"></i> <span>Nuevo</span> </a>
			 <a href="index.php?page=pagosNuevoExpress" class="btn btn-info waves-effect waves-light m-b-5"> <i class="fa fa-plus"></i> <span>Express</span> </a>
         </div>
      </div>
   </div>
   <div class="col-md-6">
      <div class="card-box">
         <h4 class="header-title text-center">Locutorios</h4>
         <div class="text-center">
		 <?php
			$locubusca = "SELECT id,nombre FROM locutorio";
			$locuencu = $mysqli->query($locubusca);
			while($locu = $locuencu->fetch_array()){
				 echo '<a href="index.php?page=locutoriosNuevo&id='.$locu['id'].'" class="btn btn-info waves-effect waves-light m-b-5"> <i class="fa fa-plus"></i> <span>'.strtoupper($locu['nombre']).'</span> </a> ';
			}	
		  echo '<a href="index.php?page=productosPOS" class="btn btn-info waves-effect waves-light m-b-5"> <i class="fa fa-plus"></i> <span>
		  POS</span> </a>&nbsp;<a href="index.php?page=comprasNueva" class="btn btn-info waves-effect waves-light m-b-5"> <i class="fa fa-plus"></i> <span>
		  Divisas</span> </a>';
		  ?>
         </div>
      </div>
   </div>
</div>
<?php } ?>
<?php if($_SESSION["joldin_rol"] =="1"){  ?>
<div class="row">
   <div class="col-md-12 col-xs-12">
      <div class="form-group">
         <label class="control-label" for="sucursal"> Sucursal</label>
         <select class="form-control" name="sucursal" id="sucursal" onchange="saldoSucursal();">
            <option value="100">TODAS</option>
            <?php
               $strQuery = "SELECT id,nombre FROM sucursal  ORDER BY id ASC";
               $Sucursales= $mysqli->query($strQuery);
               while($Suc = $Sucursales->fetch_array()) {?>
            <option value="<?php echo $Suc['id']; ?>"><?php echo $Suc['nombre']; ?></option>
            <?php	 
               }
               $Sucursales->close();
               ?>	
         </select>
      </div>
   </div>
</div>
<?php } ?>
<div class="row">
   <div class="col-md-3 col-sm-6">
      <div class="card-box widget-box-two widget-two-default">
         <i class="mdi mdi-arrow-up widget-two-icon"></i>
         <div class="wigdet-two-content">
            <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Locutorios</p>
            <h5 class="text-success"><span id="locutorios"></span> €</h5>
            <p class="text-muted m-0">&nbsp;</p>
         </div>
      </div>
   </div>
    <div class="col-md-3 col-sm-6">
      <div class="card-box widget-box-two widget-two-default">
         <i class="mdi mdi-arrow-up widget-two-icon"></i>
         <div class="wigdet-two-content">
            <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Divisas</p>
            <h5 class="text-success"><span id="divisas"></span> €</h5>
            <p class="text-muted m-0">&nbsp;</p>
         </div>
      </div>
   </div>
    <div class="col-md-3 col-sm-6">
      <div class="card-box widget-box-two widget-two-default">
         <i class="mdi mdi-arrow-up widget-two-icon"></i>
         <div class="wigdet-two-content">
            <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Ventas</p>
            <h5 class="text-success"><span id="ventas"></span> €</h5>
            <p class="text-muted m-0">&nbsp;</p>
         </div>
      </div>
   </div>
   <div class="col-md-3 col-sm-6">
      <div class="card-box widget-box-two widget-two-default">
         <i class="mdi mdi-arrow-up widget-two-icon"></i>
         <div class="wigdet-two-content">
            <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Envíos</p>
            <h5 class="text-success"><span id="envios"></span> €</h5>
            <p class="text-muted m-0">&nbsp;</p>
         </div>
      </div>
   </div>
    <div class="col-md-3 col-sm-6">
      <div class="card-box widget-box-two widget-two-default">
         <i class="mdi mdi-arrow-down widget-two-icon"></i>
         <div class="wigdet-two-content">
            <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Devoluciones</p>
            <h5 class="text-danger"><span id="devoluciones"></span> €</h5>
            <p class="text-muted m-0">&nbsp;</p>
         </div>
      </div>
   </div>
   <div class="col-md-3 col-sm-6">
      <div class="card-box widget-box-two widget-two-default">
         <i class="mdi mdi-arrow-down widget-two-icon"></i>
         <div class="wigdet-two-content">
            <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Pagos</p>
            <h5 class="text-danger"><span id="pagos"></span> €</h5>
            <p class="text-muted m-0">&nbsp;</p>
         </div>
      </div>
   </div>
      <div class="col-md-3 col-sm-6">
      <div class="card-box widget-box-two widget-two-default">
         <i class="mdi mdi-arrow-down widget-two-icon"></i>
         <div class="wigdet-two-content">
            <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Entregas</p>
            <h5 class="text-danger"><span id="entregas"></span> €</h5>
            <p class="text-muted m-0">&nbsp;</p>
         </div>
      </div>
   </div>
   <div class="col-md-3 col-sm-6">
      <div class="card-box widget-box-two widget-two-default">
         <i class="mdi mdi-cash-usd widget-two-icon"></i>
         <div class="wigdet-two-content">
            <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Disponible</p>
            <h5 class="text-success"><span id="disponible"></span> €</h5>
            <p class="text-muted m-0">&nbsp;</p>
         </div>
      </div>
   </div>
</div>