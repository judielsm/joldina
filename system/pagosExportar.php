<?php
include ("../inc/config.php");			

$fechaIni = $_POST['fecha_ini']; 
$fechaIni = explode('-', $fechaIni);
$fechaIni = $fechaIni[2].'-'.$fechaIni[1].'-'.$fechaIni[0];
$fechaFin = $_POST['fecha_fin']; 
$fechaFin = explode('-', $fechaFin);
$fechaFin = $fechaFin[2].'-'.$fechaFin[1].'-'.$fechaFin[0];
$idSucursal  = $_POST['sucursal']; 
$formatoTipo = $_POST['formato'];

if($idSucursal=="100"){
	
	$Query    = "SELECT pagos.id AS NUM, DATE_FORMAT(pagos.fecha, '%d-%m-%Y') FECHA, sucursal.nombre AS SUCURSAL, pagos.n_pago AS NPAGO, tipo_doc.nombre AS TDOC,"; 
	$Query   .= "clientes.documento AS DOCUMENTO, UPPER(clientes.nombres) REMITENTE, UPPER(clientes.direccion) DIRECCION, UPPER(clientes.ciudad) CIUDAD, clientes.telefono AS TELEFONO,"; 
	$Query   .= "pagos.monto AS MONTO FROM pagos, clientes, sucursal, tipo_doc WHERE pagos.id_sucursal = sucursal.id AND pagos.id_cliente = clientes.id AND ";
	$Query   .= "clientes.tipo_doc = tipo_doc.id AND pagos.fecha BETWEEN('".$fechaIni."') AND ('".$fechaFin."') ORDER BY  NUM ASC";
	
}else{
		
	$Query    = "SELECT pagos.id AS NUM, DATE_FORMAT(pagos.fecha, '%d-%m-%Y') FECHA, sucursal.nombre AS SUCURSAL, pagos.n_pago AS NPAGO, tipo_doc.nombre AS TDOC,"; 
	$Query   .= "clientes.documento AS DOCUMENTO, UPPER(clientes.nombres) REMITENTE, UPPER(clientes.direccion) DIRECCION, UPPER(clientes.ciudad) CIUDAD, clientes.telefono AS TELEFONO,"; 
	$Query   .= "pagos.monto AS MONTO FROM pagos, clientes, sucursal, tipo_doc WHERE pagos.id_sucursal = sucursal.id AND pagos.id_cliente = clientes.id AND ";
	$Query   .= "clientes.tipo_doc = tipo_doc.id AND pagos.fecha BETWEEN('".$fechaIni."') AND ('".$fechaFin."') AND pagos.id_sucursal = '".$idSucursal."' ORDER BY  NUM ASC";
		
}
if($formatoTipo=="1"){
?>
<?php	
header('Content-type: application/vnd.ms-excel;charset=utf-8');
header('Content-Disposition: attachment; filename=pagos-'.date('d-m-Y').'.xls');

	$Reporte = $mysqli->query($Query); ?>

	<table border="1" cellpadding="2" cellspacing="0" width="100%"> 
	  <tr>
		<td bgcolor="#C4D79B" align="center"><b>NUM</b></td>
		<td bgcolor="#C4D79B" align="center"><b>FECHA</b></td>
		<td bgcolor="#C4D79B" align="center"><b>SUCURSAL</b></td>
		<td bgcolor="#C4D79B" align="center"><b>N PAGO</b></td>
		<td bgcolor="#C4D79B" align="center"><b>T. DOC</b></td>
		<td bgcolor="#C4D79B" align="center"><b>DOCUMENTO</b></td>
		<td bgcolor="#C4D79B" align="center"><b>REMITENTE</b></td>
		<td bgcolor="#C4D79B" align="center"><b>DIRECCI&Oacute;N</b></td>
		<td bgcolor="#C4D79B" align="center"><b>CIUDAD</b></td>
		<td bgcolor="#C4D79B" align="center"><b>TEL&Eacute;FONO</b></td>
		<td bgcolor="#C4D79B" align="center"><b>MONTO</b></td>
	  </tr>
	<?php while($row = $Reporte->fetch_array()){  
			if ($colordefila==0){
				$color= "#ffffff";
				$colordefila=1;
			 }else{
				$color="#DCE6F1";
				$colordefila=0;
			 }
	?>
	  <tr>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['NUM'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['FECHA'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['SUCURSAL'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['NPAGO'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['TDOC'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['DOCUMENTO'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo utf8_decode($row['REMITENTE']);?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo utf8_decode($row['DIRECCION']);?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo utf8_decode($row['CIUDAD']);?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['TELEFONO'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['MONTO'], 2, ',', '.');?></td>
	  </tr>
<?php	} ?>
	</table>
<?php	
}else{

header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename=pagos-'.date('d-m-Y').'.csv');
?>
NUM;FECHA;SUCURSAL;N PAGO;T DOC;DOCUMENTO;REMITENTE;DIRECCION;CIUDAD;TELEFONO;MONTO
<?php
	$Reporte = $mysqli->query($Query);
	while($row = $Reporte->fetch_array()){   
	
echo$row['NUM'].";".$row['FECHA'].";".$row['SUCURSAL'].";".$row['NPAGO'].";".$row['TDOC'].";".$row['DOCUMENTO'].";".utf8_decode($row['REMITENTE']).";".utf8_decode($row['DIRECCION']).";".utf8_decode($row['CIUDAD']).";".$row['TELEFONO'].";".number_format($row['MONTO'], 2, ',', '.')."\n"; }
		
}
?>
