<?php
   if ($_SESSION["joldin_log"] == true){
   }else{
       header("Location: ../index.php");
   }
   ?>
<script type="text/javascript">
$(document).ready(function() {
    $('#datatable-responsive thead th:lt(6)').each(function() {
        var title = $(this).text();
       if(title=='Fecha'){
			$(this).html('<input type="text" placeholder="Filtrar por ' + title + '" data-provide="datepicker"/>');
		}else{
			$(this).html('<input type="text" placeholder="Filtrar por ' + title + '" />');
		}
    });
    var table = $('#datatable-responsive').DataTable({
        ajax: 'devolucionesBusca.php?id=<?php echo $_SESSION["joldin_suc"]; ?>',
        order: [
            [0, 'desc']
        ],
        columns: [{
                orderable: false,
                visible: false,
                data: "id_en"
            },
            {
                orderable: false,
                data: "fecha_en"
            },
            {
                orderable: false,
                data: "numero_en"
            },
            {
                orderable: false,
                data: "sucursal_en"
            },
            {
                orderable: false,
                data: "remesadora_en"
            },
            {
                orderable: false,
                data: "monto_en",
            },
            {
                orderable: false,
                render: function(data, type, row, meta) {
                     return  '<span class="label label-info">DEVOLUCIÓN</span>'
					}
            },
            {
                searchable: false,
                orderable: false,
                render: function(data, type, row, meta) {
					if(row[1]=='<?php echo date('d-m-Y')?>'){
								 return '<a href="index.php?page=devolucionesDetalle&id=' + row[0] + '" class="btn btn-icon waves-effect btn-info btn-xs" title="Ver"><i class="typcn typcn-eye"></i> </a>&nbsp;' +
								        '<a href="index.php?page=acciones&val=del&accion=devolucionesElimina&id=' + row[0] + ' " class="btn btn-icon waves-effect btn-info btn-xs" title="Eliminar"> <i class="typcn typcn-trash"></i> </a>'
						
							}else{
								return '<a href="index.php?page=devolucionesDetalle&id=' + row[0] + '" class="btn btn-icon waves-effect btn-info btn-xs" title="Ver"><i class="typcn typcn-eye"></i> </a>' 
							} 
                   }
            },
        ]

    });
    table.columns().every(function() {
        var that = this;
        $('input', this.header()).on('keyup change', function() {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
});
window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function() {
        $(this).remove();
    });
}, 5000);
</script>
<style>
.dataTables_filter {
display: none; 
}
thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
		text-align: center;
    }
</style>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Devoluciones</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<?php
   $strAccion = $_GET['menssage'];
   switch ($strAccion) {
   case "add":
   ?>
<div class="row">
   <div class="alert alert-success col-md-12" align="center">
      <i class="mdi mdi-check-all"></i> La Devolución Fue Agregada.
   </div>
</div>
<?php
   break;
   case "edit":
   ?>
<div class="row">
   <div class="alert alert-warning col-md-12" align="center">
      <i class="mdi mdi-alert"></i> La Devolución Fue Editada.
   </div>
</div>
<?php
   break;
   case "del":
   ?>
<div class="row">
   <div class="alert alert-danger col-md-12" align="center">
      <i class="mdi mdi-block-helper"></i>La Devolución Fue Eliminada.
   </div>
</div>
<?php
   break;
   case "dup":
   ?>
<div class="row">
   <div class="alert alert-danger col-md-12" align="center">
      <i class="mdi mdi-block-helper"></i> Envío ya existe.
   </div>
</div>
<?php
   break;
   case "mor":
   ?>
<div class="row">
   <div class="alert alert-danger col-md-12" align="center">
      <i class="mdi mdi-block-helper"></i> Esta Envío Tiene Registros.
   </div>
</div>
<?php
   break;
   case "error":
   ?>
<div class="row">
   <div class="alert alert-danger col-md-12" align="center">
      <i class="mdi mdi-block-helper"></i> No Se Realizo Acción.
   </div>
</div>
<?php
   break;
   }
   ?>
<?php if($_SESSION["joldin_rol"]=="1"){ ?>
<form id="frm-exportar" action="devolucionesExportar.php" method="post" enctype="multipart/form-data">
<div class="row">
	<div class="col-md-3">
	<label class="control-label" for="fecha_ini"> Fecha Inicio</label>
				<div class="input-group date" data-provide="datepicker">
				<input type="text" id="fecha_ini" name="fecha_ini" class="form-control required" value="<?php echo date("d-m-Y"); ?>">
				<div class="input-group-addon">
			<span class="glyphicon glyphicon-th"></span>
		</div>
	</div>						
	</div>
	<div class="col-md-3">
	<label class="control-label" for="fecha_fin"> Fecha Fin</label>
				<div class="input-group date" data-provide="datepicker">
				<input type="text" id="fecha_fin" name="fecha_fin" class="form-control required" value="<?php echo date("d-m-Y"); ?>">
				<div class="input-group-addon">
			<span class="glyphicon glyphicon-th"></span>
		</div>
	</div>			
	</div>
	<div class="col-md-2">
		<div class="form-group">
                     <label class="control-label" for="sucursal"> Sucursal</label>
                     <select class="form-control" name="sucursal" id="sucursal">
                        <?php
								$strQuery = "SELECT * FROM sucursal ORDER BY id ASC";
								$Sucursales= $mysqli->query($strQuery);
								while($Suc = $Sucursales->fetch_array()) {?>
									<option value="<?php echo $Suc['id']; ?>"><?php echo $Suc['nombre']; ?></option>
								<?php	 
								}
								$Sucursales->close();
							?>	
                     </select>
                  </div>		
	</div>
	<div class="col-md-4 text-center" style="padding-top: 27px;">
		<button type="submit" id="formato" name="formato" value="1" class="btn w-lg btn-teal waves-effect w-md waves-light m-b-5"><i class="mdi mdi-file-excel-box"></i> Exportar Excel</button>	
	</div>
</div>
</form>
 <?php } ?>
<div class="row">
   <div class="card-box table-responsive">
      <div class="row">
         <div class="col-sm-12">
           <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap text-center text-tabla" cellspacing="0" width="100%">
			<thead>
				<tr>
				    <th class="text-center">ID</th>
					<th class="text-center">Fecha</th>
					<th class="text-center">N Envio</th>
					<th class="text-center">Sucursal</th>
					<th class="text-center">Remesadora</th>
					<th class="text-center">Monto</th>
					<th class="text-center">Tipo</th>
					<th class="text-center">Opciones</th>
				</tr>
			</thead>
			<tfoot>
            <tr>	
					<th class="text-center">ID</th>
					<th class="text-center">Fecha</th>
					<th class="text-center">N Envio</th>
					<th class="text-center">Sucursal</th>
					<th class="text-center">Remesadora</th>
					<th class="text-center">Monto</th>
					<th class="text-center">Tipo</th>
					<th class="text-center">Opciones</th>
            </tr>
        </tfoot>
		  </table>
         </div>
      </div>
   </div>
</div>											