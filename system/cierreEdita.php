<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
$CierreB = "SELECT
				cierres.id as id,
				DATE_FORMAT(cierres.fecha, '%d-%m-%Y') fecha,
				empleados.nombres AS empleado,
				sucursal.nombre AS sucursal,
				cono_monetario.total AS total,
				IFNULL(cono_monetario.cantidad_500,0) 0cantidad_500,
				IFNULL(500.00 * cono_monetario.cantidad_500,0) cantidad_500,
				IFNULL(cono_monetario.cantidad_200,0) 0cantidad_200,
				IFNULL(200.00 * cono_monetario.cantidad_200,0) cantidad_200,
				IFNULL(cono_monetario.cantidad_100,0) 0cantidad_100,
				IFNULL(100.00 * cono_monetario.cantidad_100,0) cantidad_100,
				IFNULL(cono_monetario.cantidad_50,0) 0cantidad_50,
				IFNULL(50.00 * cono_monetario.cantidad_50,0) cantidad_50,
				IFNULL(cono_monetario.cantidad_20,0) 0cantidad_20,
				IFNULL(20.00 * cono_monetario.cantidad_20,0) cantidad_20,
				IFNULL(cono_monetario.cantidad_10,0) 0cantidad_10,
				IFNULL(10.00 * cono_monetario.cantidad_10,0) cantidad_10,
				IFNULL(cono_monetario.cantidad_5,0) 0cantidad_5,
				IFNULL(5.00 * cono_monetario.cantidad_5,0) cantidad_5,
				IFNULL(cono_monetario.cantidad_2,0) 0cantidad_2,
				IFNULL(2.00 * cono_monetario.cantidad_2,0) cantidad_2,
				IFNULL(cono_monetario.cantidad_1,0) 0cantidad_1,
				IFNULL(1.00 * cono_monetario.cantidad_1,0) cantidad_1,
				IFNULL(cono_monetario.cantidad_050,0) 0cantidad_050,
				IFNULL(0.50 * cono_monetario.cantidad_050,0) cantidad_050,
				IFNULL(cono_monetario.cantidad_020,0) 0cantidad_020,
				IFNULL(0.20 * cono_monetario.cantidad_020,0) cantidad_020,
				IFNULL(cono_monetario.cantidad_010,0) 0cantidad_010,
				IFNULL(0.10 * cono_monetario.cantidad_010,0) cantidad_010,
				IFNULL(cono_monetario.cantidad_005,0) 0cantidad_005,
				IFNULL(0.05 * cono_monetario.cantidad_005,0) cantidad_005,
				IFNULL(cono_monetario.cantidad_002,0) 0cantidad_002,
				IFNULL(0.02 * cono_monetario.cantidad_002,0) cantidad_002,
				IFNULL(cono_monetario.cantidad_001,0) 0cantidad_001,
				IFNULL(0.01 * cono_monetario.cantidad_001,0) cantidad_001
			FROM
				cierres,
				cono_monetario,
				sucursal,
				empleados
			WHERE
				cierres.id = cono_monetario.id_cierre AND 
				cierres.id_sucursal = sucursal.id AND
				cierres.id_empleado = empleados.id AND 
				cierres.id = '".$_GET['id']."'";
				
$Cierres = $mysqli->query($CierreB);
$Cierre = $Cierres->fetch_array();
$Cierres->close();
?>
<script>
$(document).ready(function() {

    var cantidad_500 = $("#cantidad_500");
    cantidad_500.keyup(function() {
        var total_500 = isNaN(parseInt(cantidad_500.val() * $("#precio_500").val())) ? 0 : (cantidad_500.val() * $("#precio_500").val())
        $("#total_500").val(total_500);
    });

    var cantidad_200 = $("#cantidad_200");
    cantidad_200.keyup(function() {
        var total_200 = isNaN(parseInt(cantidad_200.val() * $("#precio_200").val())) ? 0 : (cantidad_200.val() * $("#precio_200").val())
        $("#total_200").val(total_200);
    });

    var cantidad_100 = $("#cantidad_100");
    cantidad_100.keyup(function() {
        var total_100 = isNaN(parseInt(cantidad_100.val() * $("#precio_100").val())) ? 0 : (cantidad_100.val() * $("#precio_100").val())
        $("#total_100").val(total_100);
    });

    var cantidad_50 = $("#cantidad_50");
    cantidad_50.keyup(function() {
        var total_50 = isNaN(parseInt(cantidad_50.val() * $("#precio_50").val())) ? 0 : (cantidad_50.val() * $("#precio_50").val())
        $("#total_50").val(total_50);
    });

    var cantidad_20 = $("#cantidad_20");
    cantidad_20.keyup(function() {
        var total_20 = isNaN(parseInt(cantidad_20.val() * $("#precio_20").val())) ? 0 : (cantidad_20.val() * $("#precio_20").val())
        $("#total_20").val(total_20);
    });

    var cantidad_10 = $("#cantidad_10");
    cantidad_10.keyup(function() {
        var total_10 = isNaN(parseInt(cantidad_10.val() * $("#precio_10").val())) ? 0 : (cantidad_10.val() * $("#precio_10").val())
        $("#total_10").val(total_10);
    });

    var cantidad_5 = $("#cantidad_5");
    cantidad_5.keyup(function() {
        var total_5 = isNaN(parseInt(cantidad_5.val() * $("#precio_5").val())) ? 0 : (cantidad_5.val() * $("#precio_5").val())
        $("#total_5").val(total_5);
    });

    var cantidad_2 = $("#cantidad_2");
    cantidad_2.keyup(function() {
        var total_2 = isNaN(parseInt(cantidad_2.val() * $("#precio_2").val())) ? 0 : (cantidad_2.val() * $("#precio_2").val())
        $("#total_2").val(total_2);
    });

    var cantidad_1 = $("#cantidad_1");
    cantidad_1.keyup(function() {
        var total_1 = isNaN(parseInt(cantidad_1.val() * $("#precio_1").val())) ? 0 : (cantidad_1.val() * $("#precio_1").val())
        $("#total_1").val(total_1);
    });

    var cantidad_050 = $("#cantidad_050");
    cantidad_050.keyup(function() {
        var total_050 = isNaN(parseInt(cantidad_050.val() * $("#precio_050").val())) ? 0 : (cantidad_050.val() * $("#precio_050").val())
        $("#total_050").val(total_050);
    });

    var cantidad_020 = $("#cantidad_020");
    cantidad_020.keyup(function() {
        var total_020 = isNaN(parseInt(cantidad_020.val() * $("#precio_020").val())) ? 0 : (cantidad_020.val() * $("#precio_020").val())
        $("#total_020").val(parseFloat(total_020).toFixed(2));
    });

    var cantidad_010 = $("#cantidad_010");
    cantidad_010.keyup(function() {
        var total_010 = isNaN(parseInt(cantidad_010.val() * $("#precio_010").val())) ? 0 : (cantidad_010.val() * $("#precio_010").val())
       $("#total_010").val(parseFloat(total_010).toFixed(2));
    });

    var cantidad_005 = $("#cantidad_005");
    cantidad_005.keyup(function() {
        var total_005 = isNaN(parseInt(cantidad_005.val() * $("#precio_005").val())) ? 0 : (cantidad_005.val() * $("#precio_005").val())
       $("#total_005").val(parseFloat(total_005).toFixed(2));
    });

    var cantidad_002 = $("#cantidad_002");
    cantidad_002.keyup(function() {
        var total_002 = isNaN(parseInt(cantidad_002.val() * $("#precio_002").val())) ? 0 : (cantidad_002.val() * $("#precio_002").val())
        $("#total_002").val(parseFloat(total_002).toFixed(2));

    });

    var cantidad_001 = $("#cantidad_001");
    cantidad_001.keyup(function() {
        var total_001 = isNaN(parseInt(cantidad_001.val() * $("#precio_001").val())) ? 0 : (cantidad_001.val() * $("#precio_001").val())
        $("#total_001").val(parseFloat(total_001).toFixed(2));


    });

    $("#cantidad_500,#cantidad_200,#cantidad_100,#cantidad_50,#cantidad_20,#cantidad_10,#cantidad_5,#cantidad_2,#cantidad_1,#cantidad_050,#cantidad_020,#cantidad_010,#cantidad_005,#cantidad_002,#cantidad_001").on("change paste keyup", function() {
        var total = parseFloat($("#total_500").val()) + parseFloat($("#total_200").val()) + parseFloat($("#total_100").val()) + parseFloat($("#total_50").val()) + parseFloat($("#total_20").val()) + parseFloat($("#total_10").val()) + parseFloat($("#total_5").val()) + parseFloat($("#total_2").val()) + parseFloat($("#total_1").val()) + parseFloat($("#total_050").val()) + parseFloat($("#total_020").val()) + parseFloat($("#total_010").val()) + parseFloat($("#total_005").val()) + parseFloat($("#total_002").val()) + parseFloat($("#total_001").val());
        $("#total").val(parseFloat(total).toFixed(2));
    });
	
});
</script>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Datos Cierre</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<div class="row">
	<div class="col-md-3">&nbsp;</div>
	<div class="col-md-6">
		 <div class="card-box">
			<form action="index.php?page=acciones&accion=cierreEdita" method="post" enctype="multipart/form-data">
			<input type="hidden" name="id" id="id" value="<?php echo $_GET['id']; ?>" />
			 <input type="hidden" name="validator" id="validator" value="ok" />
			 <div class="row" style="padding-bottom:20px;">
			   <div class="col-md-4">
                 &nbsp;
				</div>
				<div class="col-md-4 text-center">
                   <b>Fecha</b>
				</div>
				<div class="col-md-4">
                  <input type="text" class="form-control" name="fecha" id="fecha" value="<?php echo $Cierre['fecha'];?>" readonly />
				</div>
			 </div>
			 
			 <div class="row">
				<div class="col-md-4 text-center">
				<b>VALOR</b>
				</div>
				<div class="col-md-4 text-center">
				<b>CANTIDAD</b>
				</div>
				<div class="col-md-4 text-center">
				<b>TOTAL</b>
				</div>
			</div>
			
			 <div class="row">
				<div class="col-md-4 text-center">
				<b>500,00 €</b>
				</div>
				<div class="col-md-4 text-center">
				<input type="number" class="form-control" name="cantidad_500" id="cantidad_500" value="<?php echo $Cierre['0cantidad_500'];?>" required />
				<input type="hidden" name="precio_500" id="precio_500" value="500" required />
				</div>
				<div class="col-md-4 text-center">
				<input type="text" class="form-control" name="total_500" id="total_500"  value="<?php echo $Cierre['cantidad_500'];?>"  readonly />
				</div>
			</div>
			
			 <div class="row">
				<div class="col-md-4 text-center">
				<b>200,00 €</b>
				</div>
				<div class="col-md-4 text-center">
				<input type="number" class="form-control" name="cantidad_200" id="cantidad_200" value="<?php echo $Cierre['0cantidad_200'];?>" required />
				<input type="hidden" name="precio_200" id="precio_200" value="200" required />
				</div>
				<div class="col-md-4 text-center">
				<input type="text" class="form-control" name="total_200" id="total_200" value="<?php echo $Cierre['cantidad_200'];?>" readonly />
				</div>
			</div>
			
			 <div class="row">
				<div class="col-md-4 text-center">
				<b>100,00 €</b>
				</div>
				<div class="col-md-4 text-center">
				<input type="number" class="form-control" name="cantidad_100" id="cantidad_100" value="<?php echo $Cierre['0cantidad_100'];?>" required />
				<input type="hidden" name="precio_100" id="precio_100" value="100" required />
				</div>
				<div class="col-md-4 text-center">
				<input type="text" class="form-control" name="total_100" id="total_100"  value="<?php echo $Cierre['cantidad_100'];?>" readonly />
				</div>
			</div>
			
			 <div class="row">
				<div class="col-md-4 text-center">
				<b>50,00 €</b>
				</div>
				<div class="col-md-4 text-center">
				<input type="number" class="form-control" name="cantidad_50" id="cantidad_50" value="<?php echo $Cierre['0cantidad_50'];?>" required />
				<input type="hidden" name="precio_50" id="precio_50" value="50" required />
				</div>
				<div class="col-md-4 text-center">
				<input type="text" class="form-control" name="total_50" id="total_50"  value="<?php echo $Cierre['cantidad_50'];?>" readonly />
				</div>
			</div>
			
			 <div class="row">
				<div class="col-md-4 text-center">
				<b>20,00 €</b>
				</div>
				<div class="col-md-4 text-center">
				<input type="number" class="form-control" name="cantidad_20" id="cantidad_20" value="<?php echo $Cierre['0cantidad_20'];?>" required />
				<input type="hidden" name="precio_20" id="precio_20" value="20" required />
				</div>
				<div class="col-md-4 text-center">
				<input type="text" class="form-control" name="total_20" id="total_20"  value="<?php echo $Cierre['cantidad_20'];?>" readonly />
				</div>
			</div>
			
			 <div class="row">
				<div class="col-md-4 text-center">
				<b>10,00 €</b>
				</div>
				<div class="col-md-4 text-center">
				<input type="number" class="form-control" name="cantidad_10" id="cantidad_10" value="<?php echo $Cierre['0cantidad_10'];?>" required />
				<input type="hidden" name="precio_10" id="precio_10" value="10" required />
				</div>
				<div class="col-md-4 text-center">
				<input type="text" class="form-control" name="total_10" id="total_10"  value="<?php echo $Cierre['cantidad_10'];?>" readonly />
				</div>
			</div>
			
			 <div class="row">
				<div class="col-md-4 text-center">
				<b>5,00 €</b>
				</div>
				<div class="col-md-4 text-center">
				<input type="number" class="form-control" name="cantidad_5" id="cantidad_5" value="<?php echo $Cierre['0cantidad_5'];?>" required />
				<input type="hidden" name="precio_5" id="precio_5" value="5" required />
				</div>
				<div class="col-md-4 text-center">
				<input type="text" class="form-control" name="total_5" id="total_5"  value="<?php echo $Cierre['cantidad_5'];?>" readonly />
				</div>
			</div>
			
			 <div class="row">
				<div class="col-md-4 text-center">
				<b>2,00 €</b>
				</div>
				<div class="col-md-4 text-center">
				<input type="number" class="form-control" name="cantidad_2" id="cantidad_2" value="<?php echo $Cierre['0cantidad_2'];?>" required />
				<input type="hidden" name="precio_2" id="precio_2" value="2" required />
				</div>
				<div class="col-md-4 text-center">
				<input type="text" class="form-control" name="total_2" id="total_2"  value="<?php echo $Cierre['cantidad_2'];?>" readonly />
				</div>
			</div>
			
			 <div class="row">
				<div class="col-md-4 text-center">
				<b>1,00 €</b>
				</div>
				<div class="col-md-4 text-center">
				<input type="number" class="form-control" name="cantidad_1" id="cantidad_1" value="<?php echo $Cierre['0cantidad_1'];?>" required />
				<input type="hidden" name="precio_1" id="precio_1" value="1" required />
				</div>
				<div class="col-md-4 text-center">
				<input type="text" class="form-control" name="total_1" id="total_1"  value="<?php echo $Cierre['cantidad_1'];?>" readonly />
				</div>
			</div>
			
			 <div class="row">
				<div class="col-md-4 text-center">
				<b>0,50 €</b>
				</div>
				<div class="col-md-4 text-center">
				<input type="number" class="form-control" name="cantidad_050" id="cantidad_050" value="<?php echo $Cierre['0cantidad_050'];?>" required />
				<input type="hidden" name="precio_050" id="precio_050" value="0.50" required />
				</div>
				<div class="col-md-4 text-center">
				<input type="text" class="form-control" name="total_050" id="total_050"  value="<?php echo $Cierre['cantidad_050'];?>" readonly />
				</div>
			</div>
			
			 <div class="row">
				<div class="col-md-4 text-center">
				<b>0,20 €</b>
				</div>
				<div class="col-md-4 text-center">
				<input type="number" class="form-control" name="cantidad_020" id="cantidad_020" value="<?php echo $Cierre['0cantidad_020'];?>" required />
				<input type="hidden" name="precio_020" id="precio_020" value="0.20" required />
				</div>
				<div class="col-md-4 text-center">
				<input type="text" class="form-control" name="total_020" id="total_020"  value="<?php echo $Cierre['cantidad_020'];?>" readonly />
				</div>
			</div>
			
			 <div class="row">
				<div class="col-md-4 text-center">
				<b>0,10 €</b>
				</div>
				<div class="col-md-4 text-center">
				<input type="number" class="form-control" name="cantidad_010" id="cantidad_010" value="<?php echo $Cierre['0cantidad_010'];?>" required />
				<input type="hidden" name="precio_010" id="precio_010" value="0.10" required />
				</div>
				<div class="col-md-4 text-center">
				<input type="text" class="form-control" name="total_010" id="total_010"  value="<?php echo $Cierre['cantidad_010'];?>" readonly />
				</div>
			</div>
			
			 <div class="row">
				<div class="col-md-4 text-center">
				<b>0,05 €</b>
				</div>
				<div class="col-md-4 text-center">
				<input type="number" class="form-control" name="cantidad_005" id="cantidad_005" value="<?php echo $Cierre['0cantidad_005'];?>" required />
				<input type="hidden" name="precio_005" id="precio_005" value="0.05" required />
				</div>
				<div class="col-md-4 text-center">
				<input type="text" class="form-control" name="total_005" id="total_005"  value="<?php echo $Cierre['cantidad_005'];?>" readonly />
				</div>
			</div>
			
			 <div class="row">
				<div class="col-md-4 text-center">
				<b>0,02 €</b>
				</div>
				<div class="col-md-4 text-center">
				<input type="number" class="form-control" name="cantidad_002" id="cantidad_002" value="<?php echo $Cierre['0cantidad_002'];?>" required />
				<input type="hidden" name="precio_002" id="precio_002" value="0.02" required />
				</div>
				<div class="col-md-4 text-center">
				<input type="text" class="form-control" name="total_002" id="total_002"  value="<?php echo $Cierre['cantidad_002'];?>" readonly />
				</div>
			</div>
			
			 <div class="row">
				<div class="col-md-4 text-center">
				<b>0,01 €</b>
				</div>
				<div class="col-md-4 text-center">
				<input type="number" class="form-control" name="cantidad_001" id="cantidad_001" value="<?php echo $Cierre['0cantidad_001'];?>" required />
				<input type="hidden" name="precio_001" id="precio_001" value="0.01" required />
				</div>
				<div class="col-md-4 text-center">
				<input type="text" class="form-control" name="total_001" id="total_001"  value="<?php echo $Cierre['cantidad_001'];?>" readonly />
				</div>
			</div>
			
			<div class="row" style="padding-top:20px;">
			   <div class="col-md-4">
                 &nbsp;
				</div>
				<div class="col-md-4 text-center">
                   <b>TOTAL</b>
				</div>
				<div class="col-md-4">
                  <input type="text" class="form-control" name="total" id="total"  value="<?php echo $Cierre['total'];?>"  readonly />
				</div>
			 </div>
			
			<div class="row" style="padding-top:20px;">
				<div class="col-md-12 text-center">
                  <button type="submit" class="btn btn-info btn-raised"><i class="fa fa-pencil"></i> Editar</button>
				</div>
			 </div>
			
			</form>
		</div>
	</div>
	<div class="col-md-3">&nbsp;</div>
</div>