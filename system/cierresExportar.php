<?php
include ("../inc/config.php");			

$fechaIni = $_POST['fecha_ini']; 
$fechaIni = explode('-', $fechaIni);
$fechaIni = $fechaIni[2].'-'.$fechaIni[1].'-'.$fechaIni[0];
$fechaFin = $_POST['fecha_fin']; 
$fechaFin = explode('-', $fechaFin);
$fechaFin = $fechaFin[2].'-'.$fechaFin[1].'-'.$fechaFin[0];
$idSucursal  = $_POST['sucursal']; 
$formatoTipo = $_POST['formato'];

if($idSucursal=="100"){
	
	$Query    = "SELECT
				cierres.fecha as fecha,
				sucursal.nombre as sucursal,
				IFNULL(500.00 * cono_monetario.cantidad_500,0) cantidad_500,
				IFNULL(200.00 * cono_monetario.cantidad_200,0) cantidad_200,
				IFNULL(100.00 * cono_monetario.cantidad_100,0) cantidad_100,
				IFNULL(50.00 * cono_monetario.cantidad_50,0) cantidad_50,
				IFNULL(20.00 * cono_monetario.cantidad_20,0) cantidad_20,
				IFNULL(10.00 * cono_monetario.cantidad_10,0) cantidad_10,
				IFNULL(5.00 * cono_monetario.cantidad_5,0) cantidad_5,
				IFNULL(2.00 * cono_monetario.cantidad_2,0) cantidad_2,
				IFNULL(1.00 * cono_monetario.cantidad_1,0) cantidad_1,
				IFNULL(0.50 * cono_monetario.cantidad_050,0) cantidad_050,
				IFNULL(0.20 * cono_monetario.cantidad_020,0) cantidad_020,
				IFNULL(0.10 * cono_monetario.cantidad_010,0) cantidad_010,
				IFNULL(0.05 * cono_monetario.cantidad_005,0) cantidad_005,
				IFNULL(0.02 * cono_monetario.cantidad_002,0) cantidad_002,
				IFNULL(0.01 * cono_monetario.cantidad_001,0) cantidad_001,
                cono_monetario.total AS total
			FROM
				cierres,
				cono_monetario,
				sucursal
			WHERE
				cierres.id = cono_monetario.id_cierre AND 
				cierres.id_sucursal = sucursal.id AND 
                cierres.fecha BETWEEN('".$fechaIni."') AND ('".$fechaFin."')  
                ORDER BY fecha DESC";
	
}else{
		
	$Query    = "SELECT
				cierres.fecha as fecha,
				sucursal.nombre as sucursal,
				IFNULL(500.00 * cono_monetario.cantidad_500,0) cantidad_500,
				IFNULL(200.00 * cono_monetario.cantidad_200,0) cantidad_200,
				IFNULL(100.00 * cono_monetario.cantidad_100,0) cantidad_100,
				IFNULL(50.00 * cono_monetario.cantidad_50,0) cantidad_50,
				IFNULL(20.00 * cono_monetario.cantidad_20,0) cantidad_20,
				IFNULL(10.00 * cono_monetario.cantidad_10,0) cantidad_10,
				IFNULL(5.00 * cono_monetario.cantidad_5,0) cantidad_5,
				IFNULL(2.00 * cono_monetario.cantidad_2,0) cantidad_2,
				IFNULL(1.00 * cono_monetario.cantidad_1,0) cantidad_1,
				IFNULL(0.50 * cono_monetario.cantidad_050,0) cantidad_050,
				IFNULL(0.20 * cono_monetario.cantidad_020,0) cantidad_020,
				IFNULL(0.10 * cono_monetario.cantidad_010,0) cantidad_010,
				IFNULL(0.05 * cono_monetario.cantidad_005,0) cantidad_005,
				IFNULL(0.02 * cono_monetario.cantidad_002,0) cantidad_002,
				IFNULL(0.01 * cono_monetario.cantidad_001,0) cantidad_001,
                cono_monetario.total AS total
			FROM
				cierres,
				cono_monetario,
				sucursal
			WHERE
				cierres.id = cono_monetario.id_cierre AND 
				cierres.id_sucursal = sucursal.id AND 
                cierres.fecha BETWEEN('".$fechaIni."') AND ('".$fechaFin."') AND 
                cierres.id_sucursal = '".$idSucursal."' 
                ORDER BY fecha DESC";  
		
}
if($formatoTipo=="1"){
?>
<?php	
header('Content-type: application/vnd.ms-excel;charset=utf-8');
header('Content-Disposition: attachment; filename=cierres-'.date('d-m-Y').'.xls');

	$Reporte = $mysqli->query($Query); ?>

	<table border="1" cellpadding="2" cellspacing="0" width="100%"> 
	  <tr>
		<td bgcolor="#C4D79B" align="center"><b>FECHA</b></td>
		<td bgcolor="#C4D79B" align="center"><b>SUCURSAL</b></td>
		<td bgcolor="#C4D79B" align="center"><b>500.00</b></td> 
		<td bgcolor="#C4D79B" align="center"><b>200.00</b></td>
		<td bgcolor="#C4D79B" align="center"><b>100.00</b></td>
		<td bgcolor="#C4D79B" align="center"><b>50.00</b></td>
		<td bgcolor="#C4D79B" align="center"><b>20.00</b></td>
		<td bgcolor="#C4D79B" align="center"><b>10.00</b></td>
		<td bgcolor="#C4D79B" align="center"><b>5.00</b></td>
		<td bgcolor="#C4D79B" align="center"><b>2.00</b></td>
		<td bgcolor="#C4D79B" align="center"><b>1.00</b></td>
		<td bgcolor="#C4D79B" align="center"><b>0.50</b></td>
		<td bgcolor="#C4D79B" align="center"><b>0.20</b></td>
		<td bgcolor="#C4D79B" align="center"><b>0.10</b></td>
		<td bgcolor="#C4D79B" align="center"><b>0.05</b></td>
		<td bgcolor="#C4D79B" align="center"><b>0.02</b></td>
		<td bgcolor="#C4D79B" align="center"><b>0.01</b></td>
		<td bgcolor="#C4D79B" align="center"><b>TOTAL</b></td>
	  </tr>
	<?php while($row = $Reporte->fetch_array()){  
			if ($colordefila==0){
				$color= "#ffffff";
				$colordefila=1;
			 }else{
				$color="#DCE6F1";
				$colordefila=0;
			 }
	?>
	  <tr>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['fecha'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['sucursal'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['cantidad_500'], 2, '.', '');?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['cantidad_200'], 2, '.', '');?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['cantidad_100'], 2, '.', '');?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['cantidad_50'], 2, '.', '');?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['cantidad_20'], 2, '.', '');?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['cantidad_10'], 2, '.', '');?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['cantidad_5'], 2, '.', '');?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['cantidad_2'], 2, '.', '');?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['cantidad_1'], 2, '.', '');?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['cantidad_050'], 2, '.', '');?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['cantidad_020'], 2, '.', '');?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['cantidad_010'], 2, '.', '');?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['cantidad_005'], 2, '.', '');?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['cantidad_002'], 2, '.', '');?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['cantidad_001'], 2, '.', '');?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['total'], 2, '.', '');?></td>
	  </tr>
<?php	} ?>
	</table>
<?php	
}else{

header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename=cierres-'.date('d-m-Y').'.csv');
?>
FECHA;SUCURSAL;500.00;200.00;100.00;50.00;20.00;10.00;5.00;2.00;1.00;0.50;0.20;0.10;0.05;0.02;0.01;TOTAL
<?php
	$Reporte = $mysqli->query($Query);
	while($row = $Reporte->fetch_array()){   
 echo$row['fecha'].";".$row['sucursal'].";".number_format($row['cantidad_500'], 2, '.', '').";".number_format($row['cantidad_200'], 2, '.', '').";".number_format($row['cantidad_100'], 2, '.', '').";".number_format($row['cantidad_50'], 2, '.', '').";".number_format($row['cantidad_20'], 2, '.', '').";".number_format($row['cantidad_10'], 2, '.', '').";".number_format($row['cantidad_5'], 2, '.', '').";".number_format($row['cantidad_2'], 2, '.', '').";".number_format($row['cantidad_1'], 2, '.', '').";".number_format($row['cantidad_050'], 2, '.', '').";".number_format($row['cantidad_020'], 2, '.', '').";".number_format($row['cantidad_010'], 2, '.', '').";".number_format($row['cantidad_005'], 2, '.', '').";".number_format($row['cantidad_002'], 2, '.', '').";".number_format($row['cantidad_001'], 2, '.', '').";".number_format($row['total'], 2, '.', '')."\n"; }		
}
?>
