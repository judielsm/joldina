<?php
include ("../inc/config.php");

	
$sql = "SELECT
			empleados.nombres AS empleado,
			sucursal.nombre AS sucursal,
			acceso.log AS acceso
		FROM
			acceso,
			empleados,
			sucursal
		WHERE
			acceso.id_empleado=empleados.id AND
			acceso.id_sucursal=sucursal.id";
$result = $mysqli->query($sql);
$json = array();
while($row = $result->fetch_array()){
     $json['data'][] = $row;
}	

$result->close();		
echo json_encode($json);		

?>