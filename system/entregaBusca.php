<?php
include ("../inc/config.php");

$sql = "SELECT
			entregas.id AS id,
			DATE_FORMAT(entregas.fecha, '%d-%m-%Y') fecha,
			entregas.hora AS hora,
			sucursal.nombre AS sucursal,
			entregas.comentario AS comentario,
			entregas.monto AS monto
		FROM
			entregas,
			sucursal
		WHERE
			entregas.id_sucursal = sucursal.id";

if($_GET['id'] !="100"){
	$sql .= " AND entregas.id_sucursal='".$_GET['id']."'";
}

$result = $mysqli->query($sql);
$json = array();
while($row = $result->fetch_array()){
     $json['data'][] = $row;
}	

$result->close();		
echo json_encode($json);		

?>