<?php
include ("../inc/config.php");

$sql = "SELECT
			pagos.id AS id_pa,
			DATE_FORMAT(pagos.fecha, '%d-%m-%Y') fecha_pa,
			pagos.n_pago AS numero_pa,
			sucursal.nombre AS sucursal_pa,
			remesadora.nombre AS remesadora_pa,
			pagos.monto AS monto_pa,
			pagos.id_cliente AS cliente_pa
		FROM
			pagos,
			sucursal,
			remesadora
		WHERE
			pagos.id_sucursal = sucursal.id AND
			pagos.id_remesadora = remesadora.id AND
			DATE_FORMAT(pagos.fecha, '%d-%m-%Y') ='".date('d-m-Y')."'";

if($_GET['id'] !="100"){
	$sql .= " AND pagos.id_sucursal='".$_GET['id']."'";
}


$result = $mysqli->query($sql);
$json = array();
while($row = $result->fetch_array()){
     $json['data'][] = $row;
}	

$result->close();		
echo json_encode($json);		

?>