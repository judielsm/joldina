<?php
   if($_SESSION["joldin_log"] == TRUE) {
   	} else {
   	header("Location: ../index.php");
   }
   
   $Query = "SELECT
				ventas.id NUM,
				DATE_FORMAT(ventas.fecha, '%d/%m/%Y') FECHA,
                tipo_doc.nombre TIPO_DOC,
                clientes.documento DOCUMENTO,
                IF(clientes.residente='1', 'SI', 'NO') RESIDENTE,
                clientes.nombres NOMBRES,
                paises.iso NACIONALIDAD,
                ventas.monto IMPORTE,
				divisas.codigo CODIGO,
                divisas.divisa MONEDA,
                divisas.cambio TIPO_CAMBIO,
                FORMAT(ventas.monto * divisas.cambio,2) TOTAL,
                clientes.direccion DIRECCION,
                clientes.telefono TELEFONO,
                divisas.descripcion DESCRIPCION,
                sucursal.nombre SUCURSAL,
                clientes.ocupacion OCUPACION,
                ventas.hora HORA
			FROM
				ventas,
				clientes,
				divisas,
                sucursal,
                tipo_doc,
                paises
			WHERE
				ventas.id_cliente = clientes.id AND
				ventas.id_divisa = divisas.id AND
                ventas.id_sucursal =sucursal.id AND
                clientes.tipo_doc=tipo_doc.id AND
                clientes.nacionalidad=paises.id AND  
				ventas.id = '".$_GET['id']."'";
					
   $Busca = $mysqli->query($Query);
   $Pago  = $Busca->fetch_array();
   $Busca -> close();
   
   
   ?>
   <div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Datos Compra</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
   <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
									<div class="clearfix">
                                            <div align="center">
                                               <img src="../img/logo_sm.png"> <br/>
											   <br/>
											   <div style="font-weight:900; text-transform: capitalize;">
											   <?php
											   setlocale(LC_TIME, 'es_ES.UTF-8');
												echo strftime("%A %d de %B del %Y");
												
												?>
												</div>
                                            </div>
                                        </div>
                                        <hr style="margin-top:9px; margin-bottom:1px;">
                                        <div class="row">
                                            <div class="col-md-6 pull-left">
												<address>
                                                      <strong>DATOS DEL CLIENTE</strong><br>
                                                      <b>Documento:</b> <?php echo $Pago['DOCUMENTO']; ?>&nbsp;&nbsp;&nbsp;<b>Tipo:</b><?php echo $Pago['TIPO_DOC']; ?><br>
													  <b>Residente:</b> <?php echo $Pago['RESIDENTE']; ?>&nbsp;&nbsp;&nbsp;<b>Nacionalidad:</b> <?php echo $Pago['NACIONALIDAD']; ?><br>
													  <b>Cliente:</b> <?php echo $Pago['NOMBRES']; ?><br>
													  <b>Direcciòn:</b> <?php echo $Pago['DIRECCION']; ?><br>
													  <b>Telèfono:</b> <?php echo $Pago['TELEFONO']; ?>
                                                      </address>
											</div>
											<div class="col-md-6 pull-right">
												<address>
                                                      <b>Factura:</b> <?php echo $Pago['NUM']; ?><br>
													  <b>Cod Divisa:</b> <?php echo $Pago['CODIGO']; ?><br>
													  <b>Moneda:</b> <?php echo $Pago['MONEDA']; ?><br>
													  <b>Cambio:</b> <?php echo $Pago['TIPO_CAMBIO']; ?><br>
													  <b>Importe:</b> <?php echo $Pago['IMPORTE']; ?><br>
													   <b>Total:</b> <?php echo $Pago['TOTAL']; ?>
                                                      </address>
											</div>
                                        </div>
										
										
                                        <div class="hidden-print" style="padding-top:20px;">
                                            <div class="pull-right">
                                                <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i> Imprimir</a>
                                                <a href="index.php?page=divisasVentas" class="btn btn-info waves-effect waves-light"><i class="mdi mdi-keyboard-backspace"></i> Atras</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div> 