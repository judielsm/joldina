<?php
if ($_SESSION["joldin_log"] == TRUE) {
} else {
    header("Location: ../index.php");
}
?>
<script type="text/javascript">
$selecEnvio = null;
$(function() {
iniciarBusquedaEnvio();

    $("#n_envio").on("select2:select", function() {
        cargarEnvio();
    });

});


function cargarEnvio() {
    var id = $("#n_envio").val();
    console.log(id);
    $.ajax({
        url: 'enviosCargaD.php',
        type: 'POST',
        data: "id=" + id,
        dataType: "JSON",
        success: function(per) {
            console.log(per);
            if (per.cant > 0) {
                var dat = per.datos;
                $("#id-envio").val(dat.id);
				 var data = [{
                    id: dat.n_envio,
                    text: dat.n_envio
                }];
			    $('#n_envio').select2({
                    data: data
                });
                $("#fecha").val(dat.fecha);
                $("#monto").val(dat.monto);
				  iniciarBusquedaEnvio();
            } else {
                $("#id-envio").val(0);
                $("#fecha").val('');
                $("#monto").val('');
			    $("#n_envio").val('');
            }
        }
    });
}
function iniciarBusquedaEnvio() {
		$selecEnvio = $('#n_envio').select2({
		tags:true,
		allowClear: true,
        language: "es",
        ajax: {
            url: 'enviosNumeros.php',
            dataType: 'json',
            processResults: function(data) {
                return {
                    results: data
                };
            },
			
        }
    });
}
</script>
<div class="row">
    <div class="col-xs-12">
        <div class="page-title-box">
            <h4 class="page-title">Datos Devolución</h4>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="card-box">
		<form id="frm-envio" action="index.php?page=acciones&accion=devolucionesNuevo" method="post" enctype="multipart/form-data">
		<input type="hidden" name="validator" id="validator" value="ok" />
		<input type="hidden" name="empleado" id="empleado" value="<?php echo $_SESSION["joldin_ide"];?>" />
		<input type="hidden" name="fech" id="fech" value="<?php echo date("Y-m-d");?>" />
		<input type="hidden" name="id-envio" id="id-envio" />
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="fecha"> Fecha</label>
							<input id="fecha" name="fecha" type="date" class="form-control"  required readonly />
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="n_envio"> Nro Envio</label>
                            <select id="n_envio" name="n_envio" class="form-control" required></select>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="monto"> Monto</label>
                            <input type="number" class="form-control" name="monto" id="monto" placeholder="00.00" required readonly />
                        </div>
                    </div>
                </div>
						   <div class="row">
            <div class="col-md-12 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="comentario"> Comentario</label>
                  <textarea rows="2" class="form-control" name="comentario" id="comentario"></textarea>
               </div>
            </div>
         </div>
			      
				         <div class="row mb-4">
            <div class="col-md-2 col-xs-1">&nbsp;</div>
            <div class="col-md-8 col-xs-10 text-center">
               <button type="submit" class="btn btn-info btn-raised"><i class="fa fa-save"></i> Guardar</button>
            </div>
            <div class="col-md-2 col-xs-1">&nbsp;</div>
         </div>
        </form>
    </div>
</div>