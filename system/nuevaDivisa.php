<?php
if ($_SESSION["joldin_log"] == TRUE) {
} else {
    header("Location: ../index.php");
}
?>
<div class="row">
    <div class="col-xs-12">
        <div class="page-title-box">
            <h4 class="page-title">Datos Divisa</h4>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="card-box">
		<form id="frm-envio" action="index.php?page=acciones&accion=divisasNueva" method="post" enctype="multipart/form-data">
		 <input type="hidden" name="validator" id="validator" value="ok" />
		 <div>
            <section>
                <div class="row">
					<div class="col-md-3 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="divisa"> Codigo</label>
                            <input id="v" name="codigo" type="text" class="form-control" placeholder="USD" />
                        </div>
                    </div>
					 <div class="col-md-3 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="divisa"> Divisa</label>
                            <input id="divisa" name="divisa" type="text" class="form-control" placeholder="USD" />
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="descripcion"> Descripción</label>
                            <input id="descripcion" name="descripcion" type="text" class="form-control" placeholder="DOLAR" />
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="cambio"> Cambio</label>
                            <input id="text" name="cambio" type="text" class="form-control required" placeholder="00.0000"  />
                        </div>
                    </div>
					<div class="col-md-12 text-center">
               <button type="submit" class="btn btn-info btn-raised"><i class="fa fa-save"></i> Guardar</button>
            </div>
                </div>
            </section>
        </div>
        </form>
    </div>
</div>