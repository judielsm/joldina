<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
$strQuery = "SELECT e.id as id_em,e.nombres as nombre_emp,s.id as id_su,s.nombre as nombre_su,e.email as email_em,e.ingo as clave_em,e.rol as rol_em FROM empleados as e, sucursal as s WHERE e.id=".$_SESSION["joldin_ide"]." AND e.id_sucursal=s.id";
 $strEmpleado = $mysqli->query($strQuery);
 $Emp = $strEmpleado->fetch_array(); 
?>
<script type="text/javascript">
   window.setTimeout(function () {
   	$(".alert").fadeTo(500, 0).slideUp(500, function () {
   		$(this).remove();
   	});
   }, 5000);	
</script>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Perfil</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<?php  if ($_GET['menssage']=="ok") { ?>
<div class="row">
   <div class="alert alert-success col-md-12" align="center">
      <i class="mdi mdi-check-all"></i> La Clave Fue Actualizada.
   </div>
</div>
<?php } ?>
<div class="row">
<div class="col-md-4 col-xs-12">&nbsp;</div>
<div class="col-md-4 col-xs-12">
<div class="card-box">
   <div class="row">
      <div class="text-center">
         <div class="member-card">
            <div class="thumb-xl member-thumb m-b-10 center-block">
               <img src="../img/avatar.jpg" class="img-circle img-thumbnail" alt="profile-image">
            </div>
            <div class="text-center">
               <p class="text-muted font-13"> <span class="m-l-15"><?php echo $Emp['nombre_emp']; ?></span></p>
               <p class="text-muted font-13"><span class="m-l-15"><?php echo $Emp['email_em']; ?></span></p>
               <p class="text-muted font-13"> <span class="m-l-15"><?php  if($Emp['rol_em']=="1"){ echo "Administrador"; }else{ echo "Empleado"; } ?></span></p>
            </div>
            <hr/>
			<?php  if ($_GET['menssage']=="ok") { ?>
			<a href="index.php?page=sysHome" class="btn btn-info btn-sm w-sm waves-effect m-t-10 waves-light"><i class="mdi mdi-keyboard-backspace"></i> Volver</a>
			<?php }else{ ?>
            <a href="index.php?page=perfilClave" class="btn btn-info btn-sm w-sm waves-effect m-t-10 waves-light"><i class="mdi mdi-lock-open"></i> Cambiar Clave</a>
			<?php } ?>
         </div>
      </div>
   </div>
</div>
</div>
<div class="col-md-4 col-xs-12">&nbsp;</div>
</div>