<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
$strQuery = "SELECT * FROM locutorio WHERE id='".$_GET['id']."'";
$strLocu = $mysqli->query($strQuery);
$Locutorio = $strLocu->fetch_array();
$strLocutorio = $Locutorio['nombre'];
$strLocu->close();
	
?>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Datos Locutorio</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<div class="row">
   <div class="card-box">
      <form action="index.php?page=acciones&accion=locutoriosNuevo" method="post" enctype="multipart/form-data">
         <input type="hidden" name="validator" id="validator" value="ok" />
		 <input type="hidden" name="empleado" id="empleado" value="<?php echo $_SESSION["joldin_ide"];?>" />
		 <input type="hidden" name="sucursal" id="sucursal" value="<?php echo $_SESSION["joldin_suc"];?>" />
		  <input type="hidden" name="id" id="id" value="<?php echo $_GET["id"];?>" />
         <div class="row">
		 <div class="col-md-3">
              <label class="control-label" for="fecha"> Fecha</label>
					<div class="input-group date" data-provide="datepicker">
						<input type="text" id="fecha" name="fecha" class="form-control required" value="<?php echo date("d-m-Y"); ?>">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-th"></span>
						</div>
					</div>
            </div>		
		 <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label" for="producto"> Producto</label>
                  <input type="text" class="form-control" name="producto" id="producto" value="<?php echo $strLocutorio; ?>"  readonly  />
               </div>
            </div>
			<div class="col-md-3">
					<div class="form-group">
                            <label class="control-label" for="tpago"> Tipo Pago</label>
                            <select id="tpago" name="tpago" class="form-control required">
                                <?php
                                    $strQuery = "SELECT * FROM tipo_pago ORDER BY nombre ASC";
                                    $Pag = $mysqli->query($strQuery);
                                    while ($TPag = $Pag->fetch_array()) { ?>
                                        <option value="<?php echo $TPag['id']; ?>"><?php echo $TPag['nombre']; ?></option>
                                        <?php
                                    }
                                    $Pag->close();
                                ?>
                            </select>
                        </div>
            </div>
			<div class="col-md-3">
                 <div class="form-group">
                  <label class="control-label" for="monto"> Monto</label>
                   <input type="text" class="form-control" name="monto" id="monto" placeholder="00.00"
                                   required min="0" step="0.01" pattern="^\d+(?:\.\d{1.2})?$" />
               </div>
            </div>
         </div>
         <div class="row mb-4">
            <div class="col-md-2 col-xs-1">&nbsp;</div>
            <div class="col-md-8 col-xs-10 text-center">
               <button type="submit" class="btn btn-info btn-raised"><i class="fa fa-save"></i> Guardar</button>
            </div>
            <div class="col-md-2 col-xs-1">&nbsp;</div>
         </div>
      </form>
   </div>
</div>