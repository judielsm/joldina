<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
 if (is_numeric($_GET['id'])){
$strQuery = "SELECT beneficiarios.id AS id_be, beneficiarios.nombres AS nombres_be, paises.id AS pais_id_be,  paises.iso AS pais_iso_be, paises.nombre AS pais_be, beneficiarios.ciudad AS ciudad_be FROM beneficiarios, paises WHERE beneficiarios.id_pais=paises.id AND beneficiarios.id='".$_GET['id']."'";
 $strBeneficiario = $mysqli->query($strQuery);
 $Beneficiario = $strBeneficiario->fetch_array(); 
?>
<script type="text/javascript">
$selecPaises = null;
$(function () {
	iniciarBusquedaPais();
});
function iniciarBusquedaPais(){
		$selecPaises = $('#pais').select2({
        ajax: {
          url: 'beneficiariosPais.php',
          dataType: 'json',
          processResults: function (data) {
            return {
              results: data
            };
          },
        }
      });
}
</script>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Datos Beneficiario</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<div class="row">
   <div class="card-box">
       <form action="index.php?page=acciones&accion=beneficiariosEdita" method="post" enctype="multipart/form-data">
         <input type="hidden" name="validator" id="validator" value="ok" />
		 <input type="hidden" name="id" id="id" value="<?php echo $_GET['id']; ?>" />
         <div class="row">
            <div class="col-md-4 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="nombres"> Nombres</label>
                  <input type="text" class="form-control" name="nombres" id="nombres" placeholder="Pedro Jose" required value="<?php echo $Beneficiario['nombres_be'];?>"/>
               </div>
            </div>
            <div class="col-md-4 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="ciudad"> Ciudad</label>
                  <input type="text" class="form-control" name="ciudad" id="ciudad" placeholder="MADRID" required value="<?php echo $Beneficiario['ciudad_be'];?>"/>
               </div>
            </div>
			<div class="col-md-4 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="pais"> Paìs</label>
                   <select class="pais form-control" name="pais" id="pais">
						<option value="<?php echo $Beneficiario['pais_id_be'];?>"><?php echo $Beneficiario['pais_iso_be']." - ".$Beneficiario['pais_be'];?></option>
				   </select>
               </div>
            </div>
         </div>
         <div class="row mb-4">
            <div class="col-md-2 col-xs-1">&nbsp;</div>
            <div class="col-md-8 col-xs-10 text-center">
               <button type="submit" class="btn btn-warning btn-raised"><i class="fa fa-pencil"></i> Editar</button>
            </div>
            <div class="col-md-2 col-xs-1">&nbsp;</div>
         </div>
      </form>
   </div>
</div>
<?php
	 $strBeneficiario->close();
	 }else{
		   echo '<meta http-equiv=Refresh content="0; url=index.php?page=beneficiariosLista&menssage=error">';
 }
?>