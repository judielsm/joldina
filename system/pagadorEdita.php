<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
 if (is_numeric($_GET['id'])){
$strQuery = "SELECT pagador.id as id,pagador.nombre as nombre,remesadora.id as id_remesadora,remesadora.nombre as remesadora, paises.id as id_pais, CONCAT_WS(' - ', paises.iso, paises.nombre) AS pais FROM pagador,remesadora,paises WHERE pagador.id_remesadora=remesadora.id AND pagador.id_pais=paises.id AND pagador.id='".$_GET['id']."'";
 $Pagador = $mysqli->query($strQuery);
 $Paga = $Pagador->fetch_array(); 
?>
<script type="text/javascript">
$selecPaises = null;
$selecRemesadora = null;
$(function() {
    
    iniciarBusquedaPais();
	iniciarBuscaRemesadora();

});


function iniciarBusquedaPais(){
		$selecRemesadora = $('#pais').select2({
		tags:true,
        language: "es",
		allowClear: true,
		 placeholder: "Escriba el nombre del pais...",
        ajax: {
          url: "pagadorPais.php",
          dataType: 'json',
		  type:"GET",
          processResults: function (data) {
            return {
              results: data
            };
          },
        }
      });
}

function iniciarBuscaRemesadora(){
		$("#pagador").val('').trigger('change');
		$selecRemesadora = $('#remesadora').select2({
		tags:true,
        language: "es",
		allowClear: true,
		 placeholder: "Escriba el nombre de la remesadora...",
        ajax: {
          url: "pagadorRemesador.php",
          dataType: 'json',
		  type:"GET",
          processResults: function (data) {
            return {
              results: data
            };
          },
        }
      });
}
</script>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Datos Pagador</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<div class="row">
   <div class="card-box">
       <form action="index.php?page=acciones&accion=pagadorEdita" method="post" enctype="multipart/form-data">
	  <input type="hidden" name="validator" id="validator" value="ok" />
	  <input type="hidden" name="id" id="id" value="<?php echo $_GET['id'];?>" />
         <div class="row">
            <div class="col-md-4 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="nombre"> Nombre</label>
                  <input type="text" class="form-control" name="nombre" id="nombre" placeholder="RIA" required value="<?php echo $Paga['nombre']; ?>"/>
               </div>
            </div>
			<div class="col-md-4 col-xs-12">
             <div class="form-group">
                            <label class="control-label" for="remesadora"> Remesadora</label>
                            <select id="remesadora" name="remesadora" class="remesadora form-control" required>
							<option value="<?php echo $Paga['id_remesadora']; ?>"><?php echo $Paga['remesadora']; ?></option>
							</select>
                        </div>
            </div>
			<div class="col-md-4 col-xs-12">
             <div class="form-group">
                            <label class="control-label" for="pais"> Pais</label>
                           <select id="pais" name="pais" class="pais form-control" required>
							<option value="<?php echo $Paga['id_pais']; ?>"><?php echo $Paga['pais']; ?></option>
							</select>
                        </div>
            </div>
         </div>
         <div class="row mb-4">
            <div class="col-md-2 col-xs-1">&nbsp;</div>
            <div class="col-md-8 col-xs-10 text-center">
               <button type="submit" class="btn btn-warning btn-raised"><i class="fa fa-pencil"></i> Editar</button>
            </div>
            <div class="col-md-2 col-xs-1">&nbsp;</div>
         </div>
      </form>
   </div>
</div>
<?php
	 }else{
		   echo '<meta http-equiv=Refresh content="0; url=index.php?page=pagadorLista&menssage=error">';
 }
?>