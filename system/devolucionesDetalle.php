<?php
   if($_SESSION["joldin_log"] == TRUE) {
   	} else {
   	header("Location: ../index.php");
   }
   
   $Query = "SELECT
					DATE_FORMAT(envios.fecha, '%d-%m-%Y') fecha_en,
					sucursal.nombre AS sucursal_en,
					envios.n_envio AS envios_en,
					empleados.nombres AS empleado_en,
					tipo_doc.nombre AS tipod_cl,
					clientes.documento AS documento_cl,
					clientes.nombres AS nombre_cl,
					UPPER(clientes.ciudad) ciudad_cl,
					clientes.telefono AS telefono_cl,
					beneficiarios.nombres AS nombre_be,
					UPPER(beneficiarios.ciudad) ciudad_be,
					paises.iso AS pais_be,
					remesadora.nombre AS reme,
					pagador.nombre AS paga,
					envios.monto AS monto_en,
					envios.nota AS nota_en
				FROM
					envios,
					sucursal,
					clientes,
					empleados,
					beneficiarios,
					remesadora,
					pagador,
					paises,
					tipo_doc,
					tipo_pago
				WHERE
					envios.id_sucursal = sucursal.id AND 
					envios.id_cliente = clientes.id AND 
					envios.id_empleado = empleados.id AND
					clientes.tipo_doc = tipo_doc.id AND 
					envios.id_beneficiario = beneficiarios.id AND 
					beneficiarios.id_pais = paises.id AND 
					envios.id_pago = tipo_pago.id AND 
					envios.id_remesadora = remesadora.id AND 
					envios.id_pagador = pagador.id AND 
					envios.id = '".$_GET['id']."'";
					
   $Busca = $mysqli->query($Query);
   $Pago  = $Busca->fetch_array();
   $Busca -> close();
   
   
   ?>
   <div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Datos Devolución</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
   <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
									<div class="clearfix">
                                            <div class="pull-left">
                                               <img src="../img/logo_sm.png"> 
                                            </div>
                                            <div class="pull-right">
											<p>&nbsp;</p>
                                                <h3 style="color:red;"><strong># <?php echo $Pago['envios_en']; ?></strong></h3>
                                            </div>
                                        </div>
                                        <hr style="margin-top:9px; margin-bottom:1px;">
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="pull-left m-t-30">
                                                    <address>
                                                      <strong>Remesadora</strong><br>
                                                      <?php echo $Pago['reme']; ?>
                                                      </address>
                                                </div>
                                                <div class="pull-right m-t-30">
                                                    <p><strong>Fecha: </strong> <?php echo $Pago['fecha_en']; ?></p>
                                                    <p><strong>Sucursal: </strong> <span class="label label-info"><?php echo $Pago['sucursal_en']; ?></span></p>
                                                </div>
                                            </div>
                                        </div>
										
										<div class="row">
                                            <div class="col-md-12">
											<span class="label label-info"> <strong>Cliente</strong></span>
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
																<th width="50%">Nombres</th>
																<th width="10%" class="text-center">T. Doc.</th>
																<th width="20%" class="text-center">Documento</th>
																<th width="10%" class="text-center">Ciudad</th>
																<th width="10%" class="text-center">Teléfono</th>
                                                            </tr>
														</thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><?php echo $Pago['nombre_cl']; ?></td>
																<td class="text-center"><?php echo $Pago['tipod_cl']; ?></td>
																<td class="text-center"><?php echo $Pago['documento_cl']; ?></td>
																<td class="text-center"><?php echo $Pago['ciudad_cl']; ?></td>
																<td class="text-center"><?php echo $Pago['telefono_cl']; ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
											<span class="label label-info"> <strong>Beneficiario</strong></span>
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
																<th width="60%" class="text-left">Nombres</th>
																<th width="20%" class="text-center">Ciudad</th>
																<th width="20%" class="text-center">Pais</th>
                                                            </tr>
														</thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><?php echo $Pago['nombre_be']; ?></td>
																<td class="text-center"><?php echo $Pago['ciudad_be']; ?></td>
																<td class="text-center"><?php echo $Pago['pais_be']; ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
											<div class="col-md-6">
												 <div class="pull-left">
												 <b>Nota:</b> <?php echo $Pago['nota_en']; ?>
												 </div>
											</div>
											<div class="col-md-6">
												 <div class="pull-right">
												 <b>Pagador:</b> <?php echo $Pago['paga']; ?>
												 </div>
											</div>
                                        </div>
                                       
											<div class="row">
                                            <div class="col-md-9">
                                                   &nbsp; 
                                            </div>
                                            <div class="col-md-3">
											 <hr>
                                                <h4 class="text-right text-info"><b>Monto: <?php echo $Pago['monto_en']; ?>  €</b></h4>
                                            </div>
											</div>
                                        <div class="hidden-print" style="padding-top:20px;">
                                            <div class="pull-right">
                                                <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i> Imprimir</a>
                                                <a href="index.php?page=devolucionesLista" class="btn btn-info waves-effect waves-light"><i class="mdi mdi-keyboard-backspace"></i> Atras</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div> 