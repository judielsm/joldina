<?php
include ("../inc/config.php");

	$sql = "SELECT
				cierres.id as id,
				DATE_FORMAT(cierres.fecha, '%d-%m-%Y') fecha,
				cono_monetario.total AS total,
				IFNULL(500.00 * cono_monetario.cantidad_500,0) cantidad_500,
				IFNULL(200.00 * cono_monetario.cantidad_200,0) cantidad_200,
				IFNULL(100.00 * cono_monetario.cantidad_100,0) cantidad_100,
				IFNULL(50.00 * cono_monetario.cantidad_50,0) cantidad_50,
				IFNULL(20.00 * cono_monetario.cantidad_20,0) cantidad_20,
				IFNULL(10.00 * cono_monetario.cantidad_10,0) cantidad_10,
				IFNULL(5.00 * cono_monetario.cantidad_5,0) cantidad_5,
				IFNULL(2.00 * cono_monetario.cantidad_2,0) cantidad_2,
				IFNULL(1.00 * cono_monetario.cantidad_1,0) cantidad_1,
				IFNULL(0.50 * cono_monetario.cantidad_050,0) cantidad_050,
				IFNULL(0.20 * cono_monetario.cantidad_020,0) cantidad_020,
				IFNULL(0.10 * cono_monetario.cantidad_010,0) cantidad_010,
				IFNULL(0.05 * cono_monetario.cantidad_005,0) cantidad_005,
				IFNULL(0.02 * cono_monetario.cantidad_002,0) cantidad_002,
				IFNULL(0.01 * cono_monetario.cantidad_001,0) cantidad_001
			FROM
				cierres,
				cono_monetario,
				sucursal
			WHERE
				cierres.id = cono_monetario.id_cierre AND 
				cierres.id_sucursal = sucursal.id";

if($_GET['id'] !="100"){
	$sql .= " AND sucursal.id='".$_GET['id']."'";
}

$result = $mysqli->query($sql);
$json = array();
while($row = $result->fetch_array()){
     $json['data'][] = $row;
}	

$result->close();		
echo json_encode($json);		

?>