<?php
if ($_SESSION["joldin_log"] == TRUE) {
} else {
    header("Location: ../index.php");
}
$strQuery = "SELECT
				*
			FROM
				divisas
			WHERE
				id='".$_GET['id']."'";
	
$strDivisas = $mysqli->query($strQuery);
$Envios = $strDivisas->fetch_array(); 
?>
<div class="row">
    <div class="col-xs-12">
        <div class="page-title-box">
            <h4 class="page-title">Datos Divisa</h4>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="card-box">
		<form id="frm-envio" action="index.php?page=acciones&accion=divisasEdita" method="post" enctype="multipart/form-data">
		 <input type="hidden" name="validator" id="validator" value="ok" />
		 <input type="hidden" name="id" id="id" value="<?php echo $Envios['id'];?>" />
		 <div>
            <section>
                <div class="row">
					<div class="col-md-3 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="divisa"> Codigo</label>
                            <input id="v" name="codigo" type="text" class="form-control" placeholder="USD" value="<?php echo $Envios['codigo'];?>"/>
                        </div>
                    </div>
					 <div class="col-md-3 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="divisa"> Divisa</label>
                            <input id="divisa" name="divisa" type="text" class="form-control" placeholder="USD" value="<?php echo $Envios['divisa'];?>"/>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="descripcion"> Descripción</label>
                            <input id="descripcion" name="descripcion" type="text" class="form-control" placeholder="DOLAR" value="<?php echo $Envios['descripcion'];?>"/>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="cambio"> Cambio</label>
                            <input id="text" name="cambio" type="text" class="form-control required" placeholder="00.0000" value="<?php echo $Envios['cambio'];?>" />
                        </div>
                    </div>
					<div class="col-md-12 text-center">
               <button type="submit" class="btn btn-info btn-raised"><i class="fa fa-save"></i> Guardar</button>
            </div>
                </div>
            </section>
        </div>
        </form>
    </div>
</div>