<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
?>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Datos Empleado</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<div class="row">
   <div class="card-box">
      <form action="index.php?page=acciones&accion=empleadosNuevo" method="post" enctype="multipart/form-data">
	  <input type="hidden" name="validator" id="validator" value="ok" />
         <div class="row">
            <div class="col-md-4 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="nombres"> Nombres</label>
                  <input type="text" class="form-control" name="nombres" id="nombres" placeholder="Pedro Jose" required />
               </div>
            </div>
			  <div class="col-md-4 col-xs-6">
                  <div class="form-group">
                     <label class="control-label" for="sucursal"> Sucursal</label>
                     <select class="form-control" name="sucursal" id="sucursal">
                        <?php
								$strQuery = "SELECT * FROM sucursal";
								$Sucursales= $mysqli->query($strQuery);
								while($Suc = $Sucursales->fetch_array()) {?>
									<option value="<?php echo $Suc['id']; ?>"><?php echo $Suc['nombre']; ?></option>
								<?php	 
								}
								$Sucursales->close();
							?>	
                     </select>
                  </div>
               </div>
            <div class="col-md-4 col-xs-6">
               <div class="form-group">
                  <label class="control-label" for="tipo_id"> Rol</label>
                  <select class="form-control" name="rol" id="rol">
                     <option value="1">Administrador</option>
                     <option value="2">Empleado</option>
                  </select>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-6 col-xs-6">
               <div class="form-group">
                  <label class="control-label" for="email"> E-mail</label>
                  <input type="email" class="form-control" name="mail" id="mail" placeholder="nombre@gmail.com" required />
               </div>
            </div>
            <div class="col-md-6 col-xs-6">
               <div class="form-group">
                  <label class="control-label" for="clave"> Clave</label>
                  <input type="password" class="form-control" name="clave" id="clave" placeholder="********" required />
               </div>
            </div>
         </div>
         <div class="row mb-4">
            <div class="col-md-2 col-xs-1">&nbsp;</div>
            <div class="col-md-8 col-xs-10 text-center">
               <button type="submit" class="btn btn-info btn-raised"><i class="fa fa-save"></i> Guardar</button>
            </div>
            <div class="col-md-2 col-xs-1">&nbsp;</div>
         </div>
      </form>
   </div>
</div>