<?php
if ($_SESSION["joldin_log"] == TRUE) {
} else {
    header("Location: ../index.php");
}
?>
<script type="text/javascript">
$selecClientes = null;
$(function() {
    var form = $("#frm-pago");


form.validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
});


    iniciarBusquedaCliente();
	
    $('input:radio[name="optradio"]').on('click', function() {
        iniciarBusquedaCliente();
    });
    $("#buscarCliente").on("select2:select", function() {
        cargarCliente();
    });
	
});
function iniciarBusquedaCliente() {
    $selecClientes = $("#buscarCliente").select2({
        allowClear: true,
        tags: true,
        language: "es",
        ajax: {
            url: 'enviosClientes.php?tipo=' + $('input:radio[name="optradio"]:checked').val(),
            dataType: 'json'

        }
    });
}
function cargarCliente() {
    var id = $("#buscarCliente").val();
    console.log(id);
    $.ajax({
        url: 'enviosCargaC.php',
        type: 'POST',
        data: "id=" + id,
        dataType: "JSON",
        success: function(per) {
            console.log(per);
            if (per.cant > 0) {
                var dat = per.datos;
                $("#id-cliente").val(dat.id);
                $("#nombres").val(dat.nombres);
                $("#tipo_id").val(dat.tipo_doc);
                $("#n_doc").val(dat.documento);
                $("#direccion").val(dat.direccion);
                $("#ciudad").val(dat.ciudad);
                $("#telefono").val(dat.telefono);
            } else {
                $("#id-cliente").val(0);
                $("#nombres").val('');
                $("#tipo_id").val('');
                $("#n_doc").val('');
                $("#direccion").val('');
                $("#ciudad").val('');
                $("#telefono").val('');
            }
            $selecClientes.val(null).trigger("change");
        }
    });
}
</script>
<div class="row">
    <div class="col-xs-12">
        <div class="page-title-box">
            <h4 class="page-title">Datos Pago</h4>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row" style="padding:10px;">
    <a href="index.php?page=pagosNuevoExpress" class="btn waves-effect btn-xlg btn-info"><i class="mdi mdi-plus-box"></i> PAGO EXPRESS</a>
</div>
<div class="row">
    <div class="card-box">
		<form id="frm-pago" action="index.php?page=acciones&accion=pagosNuevo" method="post" enctype="multipart/form-data">
		<input type="hidden" name="validator" id="validator" value="ok" />
		   <input type="hidden" name="empleado" id="empleado" value="<?php echo $_SESSION["joldin_ide"];?>" />
		 <input type="hidden" name="sucursal" id="sucursal" value="<?php echo $_SESSION["joldin_suc"];?>" />
		 <div>
		             <h3>Cliente</h3>
            <section>
                <div class="row" style="padding-bottom:10px;">
                    <div class="col-md-6">
                        Buscar Cliente Por:
                    </div>
                    <div class="col-md-3">
                        <label><input type="radio" name="optradio" value="d" checked="">Documento</label>
                    </div>
                    <div class="col-md-3">
                        <label><input type="radio" name="optradio" value="n">Nombres</label>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <select id="buscarCliente" name="buscarCliente" class="buscarCliente form-control"></select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <input type="hidden" id="id-cliente" name="id-cliente">
                            <label class="control-label" for="nombres"> Nombres</label>
                            <input id="nombres" name="nombres" type="text" class="form-control required" placeholder="Pedro Jose" autocomplete="name"/>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="tipo_id"> Tipo Doc</label>
                            <select id="tipo_id" name="tipo_id" class="form-control required">
                                <?php
                                $strQuery = "SELECT * FROM tipo_doc";
                                $strBuscaTD = $mysqli->query($strQuery);
                                while ($strTD = $strBuscaTD->fetch_array()) {

                                    ?>
                                    <option value="<?php echo $strTD['id']; ?>"><?php echo $strTD['nombre']; ?></option>
                                    <?php
                                }
                                $strBuscaTD->close();
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="n_doc"> N Doc</label>
                            <input id="n_doc" name="n_doc" type="text" class="form-control required" placeholder="0000000000"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="direccion"> Dirección</label>
                            <textarea id="direccion" name="direccion" class="form-control required" rows="2" autocomplete="street-address"> </textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="ciudad"> Ciudad</label>
                            <input id="ciudad" name="ciudad" type="text" class="form-control required" autocomplete="address-level2" placeholder="Madrid"/>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="telefono"> Teléfono</label>
                            <input id="telefono" name="telefono" type="number" class="form-control required" autocomplete="tel-national" placeholder="000000000"/>
                        </div>
                    </div>
                </div>
            </section>
            <h3>Pago</h3>
            <section>
                <div class="row">
                    <div class="col-md-4 col-xs-12">
					<label class="control-label" for="fecha"> Fecha</label>
					<div class="input-group date" data-provide="datepicker">
						<input type="text" id="fecha" name="fecha" class="form-control required" value="<?php echo date("d-m-Y"); ?>">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-th"></span>
						</div>
					</div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="npago"> Nro Pago</label>
                            <input id="npago" name="npago" type="text" class="form-control" placeholder="000000"/>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="monto"> Monto</label>
                            <input id="text" name="monto" type="number" class="form-control required" placeholder="00.00"  pattern="^\d+(?:\.\d{1,2})?$"/>
                        </div>
                    </div>
                </div>
            </section>
			 <h3>Remesadora</h3>
            <section>
                <div class="row" style="padding-bottom:10px;">
				<?php
					$strQuery = "SELECT remesadora.id as id, remesadora.nombre as nombre, remesadora.imagen as imagen FROM remesadora,pagador WHERE pagador.id_remesadora=remesadora.id GROUP BY remesadora.nombre";
					$strBuscaC = $mysqli->query($strQuery);
					while ($remesadora = $strBuscaC->fetch_array()){
					?>
                    <div  class="col-sm-2 col-xs-2">
                        <div class="form-group" id="imas">
                            <label>
                            <input type="radio" name="remesadora" value="<?php echo $remesadora['id']; ?>" onchange="iniciarBuscaPagadores();"/>
							<img src="../remesador/<?php echo $remesadora['imagen']; ?>" height="100" width="100">
							<label>
                        </div>
                    </div>
					<?php } $strBuscaC->close(); ?>
					<div class="col-md-12 text-center">
							<button type="submit" class="btn btn-info btn-raised"><i class="fa fa-save"></i> Guardar</button>
					</div>
                </div>
				
            </section>
        </div>
        </form>
    </div>
</div>