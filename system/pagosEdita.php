<?php
if ($_SESSION["joldin_log"] == TRUE) {
} else {
    header("Location: ../index.php");
}
$strQuery = "SELECT pagos.id AS id, sucursal.id AS sucursal, empleados.id AS empleado,  pagos.n_pago AS npago, DATE_FORMAT(pagos.fecha, '%d-%m-%Y') fecha, pagos.monto AS monto FROM pagos, sucursal, empleados, tipo_doc WHERE pagos.id_sucursal = sucursal.id AND pagos.id_empleado = empleados.id AND pagos.id ='".$_GET['id']."'";
 $strPagos = $mysqli->query($strQuery);
 $Pagos = $strPagos->fetch_array(); 
?>
<script type="text/javascript">
$selecClientes = null;
$(function() {
    var form = $("#frm-pago");


form.validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
});


    iniciarBusquedaCliente();
	
    $('input:radio[name="optradio"]').on('click', function() {
        iniciarBusquedaCliente();
    });
    $("#buscarCliente").on("select2:select", function() {
        cargarCliente();
    });
	
});
function iniciarBusquedaCliente() {
    $selecClientes = $("#buscarCliente").select2({
        allowClear: true,
        tags: true,
        language: "es",
        ajax: {
            url: 'enviosClientes.php?tipo=' + $('input:radio[name="optradio"]:checked').val(),
            dataType: 'json'

        }
    });
}
function cargarCliente() {
    var id = $("#buscarCliente").val();
    console.log(id);
    $.ajax({
        url: 'enviosCargaC.php',
        type: 'POST',
        data: "id=" + id,
        dataType: "JSON",
        success: function(per) {
            console.log(per);
            if (per.cant > 0) {
                var dat = per.datos;
                $("#id-cliente").val(dat.id);
                $("#nombres").val(dat.nombres);
                $("#tipo_id").val(dat.tipo_doc);
                $("#n_doc").val(dat.documento);
                $("#direccion").val(dat.direccion);
                $("#ciudad").val(dat.ciudad);
                $("#telefono").val(dat.telefono);
            } else {
                $("#id-cliente").val(0);
                $("#nombres").val('');
                $("#tipo_id").val('');
                $("#n_doc").val('');
                $("#direccion").val('');
                $("#ciudad").val('');
                $("#telefono").val('');
            }
            $selecClientes.val(null).trigger("change");
        }
    });
}
</script>
<div class="row">
    <div class="col-xs-12">
        <div class="page-title-box">
            <h4 class="page-title">Datos Pago</h4>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="card-box">
		<form id="frm-pago" action="index.php?page=acciones&accion=pagosEdita" method="post" enctype="multipart/form-data">
		<input type="hidden" name="validator" id="validator" value="ok" />
		<input type="hidden" name="id" id="id" value="<?php echo $Pagos['id'];?>" />
		 <input type="hidden" name="empleado" id="empleado" value="<?php echo $Pagos['empleado'];?>" />
		 <input type="hidden" name="sucursal" id="sucursal" value="<?php echo $Pagos['sucursal'];?>" />
		 <div>
		             <h3>Cliente</h3>
            <section>
                <div class="row" style="padding-bottom:10px;">
                    <div class="col-md-6">
                        Buscar Cliente Por:
                    </div>
                    <div class="col-md-3">
                        <label><input type="radio" name="optradio" value="d" checked="">Documento</label>
                    </div>
                    <div class="col-md-3">
                        <label><input type="radio" name="optradio" value="n">Nombres</label>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <select id="buscarCliente" name="buscarCliente" class="buscarCliente form-control"></select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <input type="hidden" id="id-cliente" name="id-cliente">
                            <label class="control-label" for="nombres"> Nombres</label>
                            <input id="nombres" name="nombres" type="text" class="form-control required" placeholder="Pedro Jose" autocomplete="name"/>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="tipo_id"> Tipo Doc</label>
                            <select id="tipo_id" name="tipo_id" class="form-control required">
                                <?php
                                $strQuery = "SELECT * FROM tipo_doc";
                                $strBuscaTD = $mysqli->query($strQuery);
                                while ($strTD = $strBuscaTD->fetch_array()) {

                                    ?>
                                    <option value="<?php echo $strTD['id']; ?>"><?php echo $strTD['nombre']; ?></option>
                                    <?php
                                }
                                $strBuscaTD->close();
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="n_doc"> N Doc</label>
                            <input id="n_doc" name="n_doc" type="text" class="form-control required" placeholder="0000000000"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="direccion"> Dirección</label>
                            <textarea id="direccion" name="direccion" class="form-control required" rows="2" autocomplete="street-address"> </textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="ciudad"> Ciudad</label>
                            <input id="ciudad" name="ciudad" type="text" class="form-control required" autocomplete="address-level2" placeholder="Madrid"/>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="telefono"> Teléfono</label>
                            <input id="telefono" name="telefono" type="number" class="form-control required" autocomplete="tel-national" placeholder="000000000"/>
                        </div>
                    </div>
                </div>
            </section>
            <h3>Pago</h3>
            <section>
                <div class="row">
                    <div class="col-md-4 col-xs-12">
					<label class="control-label" for="fecha"> Fecha</label>
					<div class="input-group date" data-provide="datepicker">
						<input type="text" id="fecha" name="fecha" class="form-control required" value="<?php echo $Pagos['fecha'];?>" readonly>
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-th"></span>
						</div>
					</div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="npago"> Nro Pago</label>
                            <input id="npago" name="npago" type="text" class="form-control" placeholder="000000"/>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="monto"> Monto</label>
                           <input id="text" name="monto" type="number" class="form-control required" placeholder="00.00"  pattern="^\d+(?:\.\d{1,2})?$" value="<?php echo $Pagos['monto'];?>" readonly />
                        </div>
                    </div>
					<div class="col-md-12 text-center">
							<button type="submit" class="btn btn-info btn-raised"><i class="fa fa-save"></i> Guardar</button>
					</div>
                </div>
            </section>
        </div>
        </form>
    </div>
</div>