<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
?>
<script type="text/javascript">
$(document).ready(function() {
    $('#datatable-responsive').dataTable({
        ajax: 'accesoBusca.php',
        columns: [
		{data: "empleado",className:"text-left"},
        {data: "sucursal"},
        {data: "acceso"}
        ]

    });
});

</script>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Accesos</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<div class="row">
   <div class="card-box table-responsive">
      <div class="row">
         <div class="col-sm-12">
           <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap text-center text-tabla" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th class="text-center">Empleado</th>
					<th class="text-center">Sucursal</th>
					<th class="text-center">Acceso</th>
				</tr>
			</thead>
		  </table>
         </div>
      </div>
   </div>
</div>