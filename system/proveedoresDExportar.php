<?php
include ("../inc/config.php");			

$fechaIni = $_POST['fecha_ini']; 
$fechaIni = explode('-', $fechaIni);
$fechaI = $fechaIni[2].'-'.$fechaIni[1].'-'.$fechaIni[0];
$fechaFin = $_POST['fecha_fin']; 
$fechaFin = explode('-', $fechaFin);
$fechaF = $fechaFin[2].'-'.$fechaFin[1].'-'.$fechaFin[0];
$formatoTipo = $_POST['formato'];

	

if($formatoTipo=="1"){
?>
<?php	
header('Content-type: application/vnd.ms-excel;charset=utf-8');
header('Content-Disposition: attachment; filename=operaciones-'.date('d-m-Y').'.xls');

$query = "SELECT
				DATE_FORMAT(e.fecha, '%d-%m-%Y') fec,
				s.nombre AS agencia,
				r.nombre AS empresa,
				pa.iso AS destino,
				COUNT(*) AS cant,
				IF(e.devolucion = 0,'Envio','Devolucion') AS tipo_env
			FROM
				envios AS e
			JOIN remesadora AS r
			ON
				r.id = e.id_remesadora
			JOIN pagador AS p
			ON
				p.id = e.id_pagador
			JOIN sucursal AS s
			ON
				s.id = e.id_sucursal
			JOIN paises AS pa
			ON
				pa.id = p.id_pais
			WHERE
				fecha BETWEEN '".$fechaI."' AND '".$fechaF."'
			GROUP BY
				agencia,
				empresa,
				destino,
				tipo_env
			ORDER BY
				agencia,
				tipo_env,
				empresa";
				
$queryP ="SELECT
				DATE_FORMAT(p.fecha, '%d-%m-%Y') fec,
				s.nombre AS agencia,
				r.nombre AS empresa,
				COUNT(*) AS cant
			FROM
				pagos AS p
			JOIN remesadora AS r
			ON
				r.id = p.id_remesadora
			JOIN sucursal AS s
			ON
				s.id = p.id_sucursal
			WHERE
				fecha BETWEEN '".$fechaI."' AND '".$fechaF."'
			GROUP BY
				agencia,
				empresa
			ORDER BY
				agencia,
				empresa";
?>
<table  width="100%" border="1" align='center' cellpadding="0" cellspacing="0" style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'>
	<thead>
		<tr>
			<th align="center" bgcolor="#C4D79B">Fecha</th>
			<th align="center" bgcolor="#C4D79B">Agencia</th>
			<th align="center" bgcolor="#C4D79B">Tipo</th>
			<th align="center" bgcolor="#C4D79B">Empresa</th>
			<th align="center" bgcolor="#C4D79B">Destino</th>
			<th align="center" bgcolor="#C4D79B">Cantidad</th>
		</tr>
	</thead>
<tbody>
<?php
$SucE = $mysqli->query($query);

if($SucE ->num_rows>0){
    while($fila = $SucE->fetch_array()){
?>
        <tr align="center">
			<td><?php echo $fila['fec']; ?></td>
			<td><?php echo $fila['agencia']; ?></td>
			<td><?php echo $fila['tipo_env']; ?></td>
			<td><?php echo $fila['empresa']; ?></td>
			<td><?php echo $fila['destino']; ?></td>
			<td><?php 
						if($fila['tipo_env']=="Devolucion"){
							echo "-".$fila['cant'];
						}else{
							echo $fila['cant'];
						} ?>
			</td>
		</tr>
<?php
    }
}
$SucE->close();
$pagos = $mysqli->query($queryP);
if($pagos ->num_rows>0){
    while($fila = $pagos->fetch_array()){
?>
        <tr align="center">
			<td><?php echo $fila['fec']; ?></td>
			<td><?php echo $fila['agencia']; ?></td>
			<td>Pago</td>
			<td><?php echo $fila['empresa']; ?></td>
			<td>&nbsp;</td>
			<td><?php echo $fila['cant']; ?></td>
		</tr>
<?php
    }

}
if($pagos->num_rows == 0 && $SucE->num_rows == 0) {
?>
<tr>
	<td colspan="6">Sin registros</td>
</tr>
<?php
}
$pagos->close();
?>
</tbody>
</table>
<?php 
}else{

include("../inc/header.php");
?>
<style>
table{
   border-collapse: collapse;
   width: 100%;
   }
   th, td {
   padding: 9px;
   }

</style>
<?php
$query = "SELECT
				DATE_FORMAT(e.fecha, '%d-%m-%Y') fec,
				s.nombre AS agencia,
				r.nombre AS empresa,
				pa.iso AS destino,
				COUNT(*) AS cant,
				IF(e.devolucion = 0,'Envio','Devolucion') AS tipo_env
			FROM
				envios AS e
			JOIN remesadora AS r
			ON
				r.id = e.id_remesadora
			JOIN pagador AS p
			ON
				p.id = e.id_pagador
			JOIN sucursal AS s
			ON
				s.id = e.id_sucursal
			JOIN paises AS pa
			ON
				pa.id = p.id_pais
			WHERE
				fecha BETWEEN '".$fechaI."' AND '".$fechaF."'
			GROUP BY
				agencia,
				empresa,
				destino,
				tipo_env
			ORDER BY
				agencia,
				tipo_env,
				empresa";
				
$queryP ="SELECT
				DATE_FORMAT(p.fecha, '%d-%m-%Y') fec,
				s.nombre AS agencia,
				r.nombre AS empresa,
				COUNT(*) AS cant
			FROM
				pagos AS p
			JOIN remesadora AS r
			ON
				r.id = p.id_remesadora
			JOIN sucursal AS s
			ON
				s.id = p.id_sucursal
			WHERE
				fecha BETWEEN '".$fechaI."' AND '".$fechaF."'
			GROUP BY
				agencia,
				empresa
			ORDER BY
				agencia,
				empresa";
?>
<table  width="100%" border="1" align='center' cellpadding="0" cellspacing="0" style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'>
	<thead>
		<tr>
			<th align="center" bgcolor="#C4D79B">Fecha</th>
			<th align="center" bgcolor="#C4D79B">Agencia</th>
			<th align="center" bgcolor="#C4D79B">Tipo</th>
			<th align="center" bgcolor="#C4D79B">Empresa</th>
			<th align="center" bgcolor="#C4D79B">Destino</th>
			<th align="center" bgcolor="#C4D79B">Cantidad</th>
		</tr>
	</thead>
<tbody>
<?php
$SucE = $mysqli->query($query);

if($SucE ->num_rows>0){
    while($fila = $SucE->fetch_array()){
?>
        <tr align="center">
			<td><?php echo $fila['fec']; ?></td>
			<td><?php echo $fila['agencia']; ?></td>
			<td><?php echo $fila['tipo_env']; ?></td>
			<td><?php echo $fila['empresa']; ?></td>
			<td><?php echo $fila['destino']; ?></td>
			<td><?php 
						if($fila['tipo_env']=="Devolucion"){
							echo "-".$fila['cant'];
						}else{
							echo $fila['cant'];
						} ?></td>
		</tr>
<?php
    }
}
$SucE->close();
$pagos = $mysqli->query($queryP);
if($pagos ->num_rows>0){
    while($fila = $pagos->fetch_array()){
?>
        <tr align="center">
			<td><?php echo $fila['fec']; ?></td>
			<td><?php echo $fila['agencia']; ?></td>
			<td>Pago</td>
			<td><?php echo $fila['empresa']; ?></td>
			<td>&nbsp;</td>
			<td><?php echo $fila['cant']; ?></td>
		</tr>
<?php
    }

}
if($pagos->num_rows == 0 && $SucE->num_rows == 0) {
?>
<tr>
	<td colspan="6">Sin registros</td>
</tr>
<?php
}
$pagos->close();
?>
</tbody>
</table>
<?php } ?>