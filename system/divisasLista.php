<?php
   if ($_SESSION["joldin_log"] == true){
   }else{
       header("Location: ../index.php");
   }
   ?>
<script type="text/javascript">
$(document).ready(function() {
    $('#datatable-responsive thead th:lt(5)').each(function() {
        var title = $(this).text();
		$(this).html('<input type="text" placeholder="Filtrar por ' + title + '" />');
		
    });
    var table = $('#datatable-responsive').DataTable({
        ajax: 'divisasBusca.php',
        order: [
            [0, 'desc']
        ],
        columns: [{
                orderable: false,
                visible: false,
                data: "id"
            },
            {
                orderable: false,
                data: "codigo"
            },
            {
                orderable: false,
                data: "divisa"
            },
            {
                orderable: false,
                data: "descripcion"
            },
            {
                orderable: false,
                data: "cambio"
            },
            {
                searchable: false,
                orderable: false,
                render: function(data, type, row, meta) {
                   
								return '<a href="index.php?page=divisasEdita&id=' + row[0] + '" class="btn btn-icon waves-effect btn-info btn-xs" title="Editar"><i class="typcn typcn-edit"></i> </a>' 
						
							
                }
            },
        ]

    });
    table.columns().every(function() {
        var that = this;
        $('input', this.header()).on('keyup change', function() {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
});
window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function() {
        $(this).remove();
    });
}, 5000);
</script>
<style>
.dataTables_filter {
display: none; 
}
thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
		text-align: center;
    }
</style>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Divisas</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<?php
   $strAccion = $_GET['menssage'];
   switch ($strAccion) {
   case "add":
   ?>
<div class="row">
   <div class="alert alert-success col-md-12" align="center">
      <i class="mdi mdi-check-all"></i> La Divisa Fue Agregada.
   </div>
</div>
<?php
   break;
   case "edit":
   ?>
<div class="row">
   <div class="alert alert-warning col-md-12" align="center">
      <i class="mdi mdi-alert"></i> La Divisa Fue Editada.
   </div>
</div>
<?php
   break;
   case "edit":
   ?>
<div class="row">
   <div class="alert alert-warning col-md-12" align="center">
      <i class="mdi mdi-alert"></i> La Divisa Ya Existe.
   </div>
</div>   
<?php
   break;
   }
   ?>
<div class="row">
   <div class="card-box table-responsive">
      <div class="row">
         <div class="col-sm-12">
           <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap text-center text-tabla" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th class="text-center">ID</th>
				    <th class="text-center">Codigo</th>
					<th class="text-center">Divisa</th>
					<th class="text-center">Descripciòn</th>
					<th class="text-center">Cambio</th>
					<th class="text-center">Opciones</th>
				</tr>
			</thead>
		  </table>
         </div>
      </div>
   </div>
</div>											