<?php
   if ($_SESSION["joldin_log"] == true){
   }else{
       header("Location: ../index.php");
   }
   ?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" integrity="sha256-uKEg9s9/RiqVVOIWQ8vq0IIqdJTdnxDMok9XhiqnApU=" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js" integrity="sha256-Ld9+920D/5mOtGgV8hjGd4hX5fRTl0i3kjA2IDwvrqc=" crossorigin="anonymous"></script>
<script type="text/javascript">
$(document).ready(function() {
    window.commentModal = function (id) {
        $.get('buscarCliente.php?id='+id, function(res) {
            var comments = JSON.parse(res).data[0] || { comments : ''};
            $('#commentInput').val(comments.comments);
            $('#hiddenIdComment').val(comments.id)
            $('#comment').modal();
        });
    }
    window.labelModal = function (id) {
        $.get('buscarCliente.php?id='+id, function(res) {
            var labels = JSON.parse(res).data[0] || { labels : ''};
            $('#labelInput').val(labels.labels);
            $('#hiddenIdLabel').val(labels.id);

            $('#label').modal();
        });
    }

    $('#saveComment').click(saveComment);
    $('#saveLabel').click(saveLabel);


    function saveLabel() {
        $.post('clientesActualizar.php', {label: $('#labelInput').val(), id: $('#hiddenIdLabel').val() }, function(res) {
            $('#label').modal('hide');
        });
    }

    function saveComment() {
        $.post('clientesActualizar.php', {comment: $('#commentInput').val(), id: $('#hiddenIdComment').val() }, function(res) {
            $('#comment').modal('hide');
        });
    }
    $('#datatable-responsive thead th:lt(10)').each(function() {
        var title = $(this).text();
       if(title=='Fecha'){
			$(this).html('<input type="text" placeholder="Filtrar por ' + title + '" data-provide="datepicker"/>');
		}else{
			$(this).html('<input type="text" placeholder="Filtrar por ' + title + '" />');
		}
    });
    var table = $('#datatable-responsive').DataTable({
        ajax: 'filtrarClientes.php',
        order: [
            [0, 'desc']
        ],
        columns: [{
                orderable: false,
                visible: false,
                data: "id"
            },
            {
                orderable: false,
                data: "nombres"
            },
            {
                orderable: false,
                data: "pais"
            },
            {
                orderable: false,
                data: "ocupacion"
            },
            {
                orderable: false,
                data: "enviosCount"
            },
            {
                orderable: false,
                data: "enviosMonto"
            },
            {
                orderable: false,
                data: "comprasCount"
            },
            {
                orderable: false,
                data: "comprasMonto"
            },
            {
                orderable: false,
                data: "pagosCount"
            },
            {
                orderable: false,
                data: "pagoMonto"
            },
            {
                searchable: false,
                orderable: false,
                render: function(data, type, row, meta) {
                    
								return '<button type="button" class="btn " onClick="commentModal('+row.id+')">comentario</button>   <button type="button" class="btn" onClick="labelModal('+row.id+')">etiquetas</button>'
					
                }
            }
        ]

    });
    table.columns().every(function() {
        var that = this;
        $('input', this.header()).on('keyup change', function() {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
});
window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function() {
        $(this).remove();
    });
}, 5000);
</script>
<style>
.dataTables_filter {
display: none; 
}
thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
		text-align: center;
    }
</style>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Ventas</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<?php
   $strAccion = $_GET['menssage'];
   switch ($strAccion) {
   case "add":
   ?>
<div class="row">
   <div class="alert alert-success col-md-12" align="center">
      <i class="mdi mdi-check-all"></i> La Venta Fue Agregada.
   </div>
</div>  
<?php
   break;
   }
   ?>
<div class="row">
   <div class="card-box table-responsive">
      <div class="row">
         <div class="col-sm-12">
           <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap text-center text-tabla" cellspacing="0" width="100%">
			<thead>
				<tr>
				    <th class="text-center">ID</th>
					<th class="text-center">Nombres</th>
					<th class="text-center">Nacionalidad</th>
					<th class="text-center">Ocupacion</th>
					<th class="text-center">Envios (cantidad)</th>
					<th class="text-center">Envios (montos)</th>

					<th class="text-center">Compras(cantidad)</th>
					<th class="text-center">Compras(monto)</th>

					<th class="text-center">Pagos(cantidad)</th>
					<th class="text-center">Pagos(monto)</th>

					<th class="text-center">Opciones</th>
				</tr>
			</thead>
		  </table>
         </div>
      </div>
   </div>
</div>

<div id="comment" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Comentario</h4>
      </div>
      <div class="modal-body">
        <textarea name="commentInput" class="form-control" id="commentInput" cols="30" rows="10"></textarea>
        <input type="hidden" name="" id="hiddenIdComment">

      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-success" id="saveComment" >Guardar</button>

        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

<div id="label" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Etiquetas</h4>
      </div>
      <div class="modal-body">
        <input type="text" data-role="tagsinput" class="form-control" name="labelInput" id="labelInput">
        <input type="hidden" name="" id="hiddenIdLabel">
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-success" id="saveLabel" >Guardar</button>

        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
