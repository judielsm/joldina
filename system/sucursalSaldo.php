<?php
include ("../inc/config.php");

$Hoy = date('Y-m-d');
$Sucursal = $_GET['id'];

		// Total Entregas
	    $SumaEntregas = "SELECT SUM(monto) AS total FROM entregas WHERE fecha='" . $Hoy . "'"; 
		
		if($_GET['id'] !="100"){
			
		$SumaEntregas .= " AND id_sucursal = '" . $Sucursal . "'";
		
		}

		$Entregas = $mysqli->query($SumaEntregas);
		$MontoEntrega = $Entregas->fetch_array();
		$Entregas->close();
		$TotalEntregas = $MontoEntrega['total'];
		
		
		// Total Divisas
	    $SumaDivisas = "SELECT SUM(monto) AS total FROM ventas WHERE fecha='" . $Hoy . "'"; 
		
		if($_GET['id'] !="100"){
			
		$SumaDivisas .= " AND id_sucursal = '" . $Sucursal . "'";
		
		}

		$Divisas = $mysqli->query($SumaDivisas);
		$MontoDivisas = $Divisas->fetch_array();
		$Divisas->close();
		$TotalDivisas = $MontoDivisas['total'];
		
		
		// Total Ventas
	    $SumaVentas = "SELECT SUM(total) AS total FROM productos_ventas WHERE fecha='" . $Hoy . "'"; 
		
		if($_GET['id'] !="100"){
			
		$SumaVentas .= " AND id_sucursal = '" . $Sucursal . "'";
		
		}

		$Ventas = $mysqli->query($SumaVentas);
		$MontoVentas = $Ventas->fetch_array();
		$Ventas->close();
		$TotalVentas = $MontoVentas['total'];
		
		

		// Total Envios
		$SumaEnvios = "SELECT SUM(monto) AS total FROM envios WHERE devolucion = 0 AND fecha='" . $Hoy . "' AND id_pago = '3'"; 
		
		if($_GET['id'] !="100"){
			
		$SumaEnvios .= " AND id_sucursal = '" . $Sucursal . "'";
		
		}
 
		$Envios = $mysqli->query($SumaEnvios);
		$MontoEnvio = $Envios->fetch_array();
		$Envios->close();
		$TotalEnvios = $MontoEnvio['total'];
		
		
		// Total Devoluciones
		$SumaDevoluciones = "SELECT SUM(monto) AS total FROM envios WHERE devolucion = 1 AND fecha='" . $Hoy . "'"; 
		
		if($_GET['id'] !="100"){
			
		$SumaDevoluciones .= " AND id_sucursal = '" . $Sucursal . "'";
		
		}
 
		$Devoluciones = $mysqli->query($SumaDevoluciones);
		$MontoDevoluciones = $Devoluciones->fetch_array();
		$Devoluciones->close();
		$TotalDevoluciones = $MontoDevoluciones['total'];
		
		
		// Total Pagos
		$SumaPagos = "SELECT SUM(monto) AS total FROM pagos WHERE fecha='" . $Hoy . "'"; 
		
		if($_GET['id'] !="100"){
			
		$SumaPagos .= " AND id_sucursal = '" . $Sucursal . "'";
		
		} 

		$Pagos = $mysqli->query($SumaPagos);
		$MontoPago = $Pagos->fetch_array();
		$Pagos->close();
		$TotalPagos = $MontoPago['total'];

		// Total Locutorios
		$SumaLocutorios = "SELECT SUM(monto) AS total FROM pagos_locutorio WHERE fecha='" . $Hoy . "' AND id_pago = '3'"; 
		
		if($_GET['id'] !="100"){
			
		$SumaLocutorios .= " AND id_sucursal = '" . $Sucursal . "'";
		
		}  
		
		$Locutorios = $mysqli->query($SumaLocutorios);
		$MontoLocutorios = $Locutorios->fetch_array();
		$Locutorios->close();
		$TotalLocutorios = $MontoLocutorios['total'];
		
		$TotalDisponible = ($TotalEnvios + $TotalLocutorios + $TotalVentas + $TotalDivisas)- $TotalPagos - $TotalEntregas - $TotalDevoluciones;

		if($TotalDisponible == ""){
			$TotalDisponible = "00,00";
		}
		if($TotalEnvios == ""){
			$TotalEnvios = "00,00";
		}
		if($TotalDevoluciones == ""){
			$TotalDevoluciones = "00,00";
		}
		if($TotalLocutorios == ""){
			$TotalLocutorios = "00,00";
		}
		if($TotalPagos == ""){
			$TotalPagos = "00,00";
		}
		if($TotalEntregas == ""){
			$TotalEntregas = "00,00";
		}
		if($TotalDivisas == ""){
			$TotalDivisas = "00,00";
		}
		if($TotalVentas == ""){
			$TotalVentas = "00,00";
		}
	
$dat = array(
	"pagos" => number_format($TotalPagos, 2, ',', '.'),
	"envios" => number_format($TotalEnvios, 2, ',', '.'),
	"devoluciones" => number_format($TotalDevoluciones, 2, ',', '.'),
	"divisas" => number_format($TotalDivisas, 2, ',', '.'),
	"ventas" => number_format($TotalVentas, 2, ',', '.'),
	"locutorios" => number_format($TotalLocutorios, 2, ',', '.'),
	"entregas" => number_format($TotalEntregas, 2, ',', '.'),
	"disponible" => number_format($TotalDisponible, 2, ',', '.')
);
echo json_encode($dat);

?>