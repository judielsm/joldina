<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
 if (is_numeric($_GET['id'])){
$strQuery = "SELECT * FROM sucursal WHERE id=".$_GET['id']."";
 $strSucursales = $mysqli->query($strQuery);
 $Suc = $strSucursales->fetch_array(); 
?>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Datos Sucursal</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<div class="row">
   <div class="card-box">
       <form action="index.php?page=acciones&accion=sucursalesEdita" method="post" method="post" enctype="multipart/form-data">
	  <input type="hidden" name="validator" id="validator" value="ok" />
	  <input type="hidden" name="id" id="id" value="<?php echo $_GET['id'];?>" />
         <div class="row">
            <div class="col-md-3 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="nombre"> Nombre</label>
                  <input type="text" class="form-control" name="nombre" id="nombre" placeholder="MADRID" value="<?php echo $Suc['nombre']; ?>"  />
               </div>
            </div>
            <div class="col-md-3 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="long"> Longitud</label>
                  <input type="text" class="form-control" name="long" id="long" placeholder="0.0000" value="<?php echo $Suc['lng']; ?>"  />
               </div>
            </div>
            <div class="col-md-3 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="lat"> Latitud</label>
                  <input type="text" class="form-control" name="lat" id="lat" placeholder="0.000" value="<?php echo $Suc['lat']; ?>"  />
               </div>
            </div>
			<div class="col-md-3 col-xs-12">
               <div class="form-group">
                <label class="control-label" for="limite"> Limite</label>
                  <input type="number" class="form-control" name="limite" id="limite" placeholder="00.00"  value="<?php echo $Suc['limite']; ?>"  pattern="^\d+(?:\.\d{1,2})?$"/>
               </div>
            </div>
         </div>
         <div class="row mb-4">
            <div class="col-md-2 col-xs-1">&nbsp;</div>
            <div class="col-md-8 col-xs-10 text-center">
               <button type="submit" class="btn btn-warning btn-raised"><i class="fa fa-pencil"></i> Editar</button>
            </div>
            <div class="col-md-2 col-xs-1">&nbsp;</div>
         </div>
      </form>
   </div>
</div>
<?php
	 }else{
		   echo '<meta http-equiv=Refresh content="0; url=index.php?page=sucursalesLista&menssage=error">';
 }
?>