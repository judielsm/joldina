<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
 if (is_numeric($_GET['id'])){
$strQuery = "SELECT c.id as ID,c.tipo_doc as tdoc, t.nombre as tdocumento,c.documento as documento_c, c.nombres as nombres_c, c.direccion as direccion_c, c.ciudad as ciudad_c, c.telefono as telefono_c FROM clientes as c, tipo_doc as t WHERE c.id='".$_GET['id']."' AND t.id=c.tipo_doc";
 $strCliente = $mysqli->query($strQuery);
 $Cliente = $strCliente->fetch_array(); 
?>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Datos Cliente</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<div class="row">
   <div class="card-box">
      <form action="index.php?page=acciones&accion=clientesEdita" method="post" enctype="multipart/form-data">
         <input type="hidden" name="validator" id="validator" value="ok" />
		 <input type="hidden" name="id" id="id" value="<?php echo $_GET['id']; ?>" />
         <div class="row">
            <div class="col-md-6 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="nombres"> Nombres</label>
                  <input type="text" class="form-control" name="nombres" id="nombres" placeholder="Pedro Jose" required value="<?php echo $Cliente['nombres_c'];?>"/>
               </div>
            </div>
            <div class="col-md-3 col-xs-6">
               <div class="form-group">
                  <label class="control-label" for="tipo_id"> Tipo Doc</label>
                  <select class="form-control" name="tipo_id" id="tipo_id">
				 <option value="<?php echo $Cliente['tdoc'];?>"><?php echo $Cliente['tdocumento'];?></option>	
                     <?php
                        $strQuery = "SELECT * FROM tipo_doc WHERE id != ".$Cliente['tdoc']."";
                        $strBuscaTD = $mysqli->query($strQuery);
                        while($strTD = $strBuscaTD->fetch_array()) {
                        
                        ?>
                     <option value="<?php echo $strTD['id'];?>"><?php echo $strTD['nombre'];?></option>
                     <?php } 
						$strBuscaTD->close();
					 ?>
                  </select>
               </div>
            </div>
            <div class="col-md-3 col-xs-6">
               <div class="form-group">
                  <label class="control-label" for="n_doc"> N Doc</label>
                  <input type="text" class="form-control" name="n_doc" id="n_doc" placeholder="0000000000" required value="<?php echo $Cliente['documento_c'];?>"/>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="direccion"> Dirección</label>
                  <textarea rows="2" class="form-control" name="direccion" id="direccion" required><?php echo $Cliente['direccion_c'];?></textarea>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-6 col-xs-6">
               <div class="form-group">
                  <label class="control-label" for="ciudad"> Ciudad</label>
                  <input type="text" class="form-control" name="ciudad" id="ciudad" placeholder="Madrid" required value="<?php echo $Cliente['ciudad_c'];?>"/>
               </div>
            </div>
            <div class="col-md-6 col-xs-6">
               <div class="form-group">
                  <label class="control-label" for="telefono"> Teléfono</label>
                  <input type="number" class="form-control" name="telefono" id="telefono" placeholder="000000000" required value="<?php echo $Cliente['telefono_c'];?>"/>
               </div>
            </div>
         </div>
         <div class="row mb-4">
            <div class="col-md-2 col-xs-1">&nbsp;</div>
            <div class="col-md-8 col-xs-10 text-center">
               <button type="submit" class="btn btn-warning btn-raised"><i class="fa fa-pencil"></i> Editar</button>
            </div>
            <div class="col-md-2 col-xs-1">&nbsp;</div>
         </div>
      </form>
   </div>
</div>
<?php
	 $strCliente->close();
	 }else{
		   echo '<meta http-equiv=Refresh content="0; url=index.php?page=clientesLista">';
 }
?>