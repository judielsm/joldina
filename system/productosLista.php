<?php
   if ($_SESSION["joldin_log"] == true){
   }else{
       header("Location: ../index.php");
   }
   ?>
<script type="text/javascript">
$(document).ready(function() {
    $('#datatable-responsive thead th:lt(5)').each(function() {
        var title = $(this).text();
		if(title=='Fecha'){
			$(this).html('<input type="text" placeholder="Filtrar por ' + title + '" data-provide="datepicker"/>');
		}else{
			$(this).html('<input type="text" placeholder="Filtrar por ' + title + '" />');
		}
		
    });
    var table = $('#datatable-responsive').DataTable({
        ajax: 'productosBusca.php',
        order: [
            [0, 'desc']
        ],
        columns: [{
                orderable: false,
                visible: false,
                data: "id"
            },
            {
                orderable: false,
                data: "nombre"
            },
			{
                orderable: false,
                data: "categoria"
            },
            {
                orderable: false,
                data: "stock"
            },
            {
                orderable: false,
                data: "importe"
            }
        ]

    });
    table.columns().every(function() {
        var that = this;
        $('input', this.header()).on('keyup change', function() {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
});
window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function() {
        $(this).remove();
    });
}, 5000);
</script>
<style>
.dataTables_filter {
display: none; 
}
thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
		text-align: center;
    }
</style>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Productos</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<?php
   $strAccion = $_GET['menssage'];
   switch ($strAccion) {
   case "add":
   ?>
<div class="row">
   <div class="alert alert-success col-md-12" align="center">
      <i class="mdi mdi-check-all"></i> EL Producto fue agregado.
   </div>
</div>  
<?php
   break;
   case "edit":
   ?>
<div class="row">
   <div class="alert alert-success col-md-12" align="center">
      <i class="mdi mdi-check-all"></i> EL Producto fue editado.
   </div>
</div>  
<?php
   break;
   }
   ?>
<div class="row">
   <div class="card-box table-responsive">
      <div class="row">
         <div class="col-sm-12">
           <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap text-center text-tabla" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th class="text-center">ID</th>
				    <th class="text-center">Nombre</th>
					<th class="text-center">Categoria</th>
					<th class="text-center">Stock</th>
					<th class="text-center">Importe</th>
				</tr>
			</thead>
		  </table>
         </div>
      </div>
   </div>
</div>											