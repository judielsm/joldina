<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Reporte Operaciones</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<form id="frm-exportar" action="proveedoresDExportar.php" method="post" enctype="multipart/form-data" target="_blank">
<div class="row">
	<div class="col-md-3">
	<label class="control-label" for="fecha_ini"> Fecha Inicio</label>
				<div class="input-group date" data-provide="datepicker">
				<input type="text" id="fecha_ini" name="fecha_ini" class="form-control required" required value="<?php echo date("d-m-Y"); ?>">
				<div class="input-group-addon">
			<span class="glyphicon glyphicon-th"></span>
		</div>
	</div>						
	</div>
	<div class="col-md-3">
	<label class="control-label" for="fecha_fin"> Fecha Fin</label>
				<div class="input-group date" data-provide="datepicker">
				<input type="text" id="fecha_fin" name="fecha_fin" class="form-control required" required value="<?php echo date("d-m-Y"); ?>">
				<div class="input-group-addon">
			<span class="glyphicon glyphicon-th"></span>
		</div>
	</div>			
	</div>
	<div class="col-md-6 text-center" style="padding-top: 27px;">
		<button type="submit" id="formato" name="formato" value="2" class="btn w-lg btn-info waves-effect w-md waves-light m-b-5"><i class="mdi mdi-eye"></i> Visualizar</button>	
		<button type="submit" id="formato" name="formato" value="1" class="btn w-lg btn-teal waves-effect w-md waves-light m-b-5"><i class="mdi mdi-file-excel-box"></i> Exportar Excel</button>
	</div>
</div>
</form>