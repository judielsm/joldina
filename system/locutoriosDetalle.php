<?php
   if($_SESSION["joldin_log"] == TRUE) {
   	} else {
   	header("Location: ../index.php");
   }

//Reporte   
$CierreB = "SELECT
				pagos_locutorio.fecha AS fecha,
				empleados.nombres AS empleado,
				sucursal.nombre AS sucursal,
				pagos_locutorio.monto AS monto,
				locutorio.nombre AS locu
			FROM
				locutorio,
				pagos_locutorio,
				empleados,
				sucursal
			WHERE
				pagos_locutorio.id_locutorio = locutorio.id AND
				pagos_locutorio.id_sucursal = sucursal.id AND 
				pagos_locutorio.id_empleado = empleados.id AND 
				pagos_locutorio.id = '".$_GET['id']."'";
				
$Cierres = $mysqli->query($CierreB);
$Cierre = $Cierres->fetch_array();
$Cierres->close();


 
   
   ?>
   <div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Datos Entrega</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
   <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="pull-left">
                                               <img src="../img/logo_sm.png">
                                            </div>
                                            <div class="pull-right">
                                                <h4><strong><?php echo $Cierre['fecha']; ?></strong></h4>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6"> 
											  <p><strong>Empleado:</strong><br> <?php echo $Cierre['empleado']; ?></p>
											</div>
											 <div class="col-md-6 text-right"> 
											  <p><strong>Sucusal:</strong> <span class="label label-info"><?php echo $Cierre['sucursal']; ?></span></p>
											</div>
                                        </div>
										 <div class="clearfix m-t-10"></div>
									
										
										<hr>
										
                                        <div class="row">
                                            <div class="col-md-6 text-justify">
										    <p><?php echo $Cierre['locu']; ?></p>
											</div>
                                            <div class="col-md-6 text-right">
											<b>Monto:</b> <?php echo $Cierre['monto']; ?> €
											</div>
                                            </div>
                                        <hr class="hidden-print">
                                     <div class="hidden-print">
                                            <div class="pull-right">
                                                <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i> Imprimir</a>
                                                <a href="index.php?page=locutoriosLista" class="btn btn-info waves-effect waves-light"><i class="mdi mdi-keyboard-backspace"></i> Atras</a>
                                            </div>
                                        </div>
										
                                    </div>
                                </div>

                            </div>

                        </div>
