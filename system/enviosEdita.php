<?php
if ($_SESSION["joldin_log"] == TRUE) {
} else {
    header("Location: ../index.php");
}
$strQuery = "SELECT
				envios.id AS id_envio,
				DATE_FORMAT(envios.fecha, '%d-%m-%Y') fecha_envio,
				sucursal.id AS id_sucursal,
				sucursal.nombre AS sucursal_envio,
				empleados.id AS id_empleado,
				paises.id AS id_pais,
				CONCAT_WS(' | ', paises.iso, paises.nombre) AS pais_beneficiario,
				tipo_pago.id AS id_pago,
				UPPER(tipo_pago.nombre) tpago_envio,
				envios.monto AS monto,
				remesadora.id AS id_remesadora,
				remesadora.imagen AS img_remesadora,
				remesadora.nombre AS remesadora,
				pagador.id AS id_pagador,
				pagador.nombre AS pagador
			FROM
				envios,
				sucursal,
				remesadora,
				pagador,
				paises,
				tipo_pago,
				empleados
			WHERE
				envios.id_sucursal = sucursal.id AND
				envios.id_empleado = empleados.id AND
				envios.id_pais = paises.id AND 
				envios.id_pago = tipo_pago.id AND 
				envios.id_remesadora = remesadora.id AND 
				envios.id_pagador = pagador.id AND 
				envios.id='".$_GET['id']."'";
	
$strEnvios = $mysqli->query($strQuery);
$Envios = $strEnvios->fetch_array(); 
?>
<script type="text/javascript">
$selecClientes = null;
$selecBene = null;
$(function() {
	

	
    var form = $("#frm-envio");


form.validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
});
    iniciarBusquedaCliente();
    iniciarBusquedaBene();
	
    $('input:radio[name="optradio"]').on('click', function() {
        iniciarBusquedaCliente();
    });
    $('input:radio[name="optradioB"]').on('click', function() {
        iniciarBusquedaBene();
    });


    $("#buscarCliente").on("select2:select", function() {
        cargarCliente();
    });

    $("#buscarBeneficiario").on("select2:select", function() {
        cargarBene();
    });
});




function iniciarBusquedaCliente() {
    $selecClientes = $("#buscarCliente").select2({
        allowClear: true,
        tags: true,
        language: "es",
        ajax: {
            url: 'enviosClientes.php?tipo=' + $('input:radio[name="optradio"]:checked').val(),
            dataType: 'json'

        }
    });
}

function iniciarBusquedaBene() {
	$("#id-bene").val(0);
	$("#nombresB").val('');
	$("#ciudadB").val('');
    $selecBene = $("#buscarBeneficiario").select2({
        allowClear: true,
        tags: true,
        language: "es",
        ajax: {
            url: 'enviosBeneficiarios.php?cliente='+$("#id-cliente").val() +'&tipo=' + $('input:radio[name="optradioB"]:checked').val(),
            dataType: 'json'
        }
    });
}

function cargarCliente() {
    var id = $("#buscarCliente").val();
    console.log(id);
    $.ajax({
        url: 'enviosCargaC.php',
        type: 'POST',
        data: "id=" + id,
        dataType: "JSON",
        success: function(per) {
            console.log(per);
            if (per.cant > 0) {
                var dat = per.datos;
                $("#id-cliente").val(dat.id);
                $("#nombres").val(dat.nombres);
                $("#tipo_id").val(dat.tipo_doc);
                $("#n_doc").val(dat.documento);
                $("#direccion").val(dat.direccion);
                $("#ciudad").val(dat.ciudad);
                $("#telefono").val(dat.telefono);
				iniciarBusquedaBene();
            } else {
                $("#id-cliente").val(0);
                $("#nombres").val('');
                $("#tipo_id").val('');
                $("#n_doc").val('');
                $("#direccion").val('');
                $("#ciudad").val('');
                $("#telefono").val('');
            }
            $selecClientes.val(null).trigger("change");
        }
    });
}

function cargarBene() {
    var id = $("#buscarBeneficiario").val();
    console.log(id);
    $.ajax({
        url: 'enviosCargaB.php',
        type: 'POST',
        data: "id=" + id,
        dataType: "JSON",
        success: function(per) {
            console.log(per);
            if (per.cant > 0) {
                var dat = per.datos;
                $("#id-bene").val(dat.id);
                $("#nombresB").val(dat.nombres_b);
                $("#ciudadB").val(dat.ciudad_b);
            } else {
                $("#id-bene").val(0);
                $("#nombresB").val('');
                $("#ciudadB").val('');
                $("#pais").val('');

            }
            $selecBene.val(null).trigger("change");
        }
    });
}
</script>
<div class="row">
    <div class="col-xs-12">
        <div class="page-title-box">
            <h4 class="page-title">Datos Envío</h4>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="card-box">
		<form id="frm-envio" action="index.php?page=acciones&accion=enviosEdita" method="post" enctype="multipart/form-data">
		 <input type="hidden" name="validator" id="validator" value="ok" />
		 <input type="hidden" name="id" id="id" value="<?php echo $Envios['id_envio'];?>" />
		 <input type="hidden" name="sucursal" id="sucursal" value="<?php echo $Envios['id_sucursal'];?>" />
		 <input type="hidden" name="empleado" id="empleado" value="<?php echo $Envios['id_empleado'];?>" />
		 <div>
            <h3>Cliente</h3>
            <section>
                <div class="row" style="padding-bottom:10px;">
                    <div class="col-md-6">
                        Buscar Cliente Por:
                    </div>
                    <div class="col-md-3">
                        <label><input type="radio" name="optradio" value="d">Documento</label>
                    </div>
                    <div class="col-md-3">
                        <label><input type="radio" name="optradio" value="n" checked>Nombres</label>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <select id="buscarCliente" name="buscarCliente" class="buscarCliente form-control"></select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <input type="hidden" id="id-cliente" name="id-cliente">
                            <label class="control-label" for="nombres"> Nombres</label>
                            <input id="nombres" name="nombres" type="text" class="form-control required" placeholder="Pedro Jose" autocomplete="name"/>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="tipo_id"> Tipo Doc</label>
                            <select id="tipo_id" name="tipo_id" class="form-control">
                                <?php
                                $strQuery = "SELECT * FROM tipo_doc";
                                $strBuscaTD = $mysqli->query($strQuery);
                                while ($strTD = $strBuscaTD->fetch_array()) {

                                    ?>
                                    <option value="<?php echo $strTD['id']; ?>"><?php echo $strTD['nombre']; ?></option>
                                    <?php
                                }
                                $strBuscaTD->close();
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="n_doc"> N Doc</label>
                            <input id="n_doc" name="n_doc" type="text" class="form-control" placeholder="0000000000"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="direccion"> Dirección</label>
                            <textarea id="direccion" name="direccion" class="form-control required" rows="2" autocomplete="street-address"> </textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="ciudad"> Ciudad</label>
                            <input id="ciudad" name="ciudad" type="text" class="form-control required" autocomplete="address-level2" placeholder="Madrid"/>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="telefono"> Teléfono</label>
                            <input id="telefono" name="telefono" type="number" class="form-control required" autocomplete="tel-national" placeholder="000000000"/>
                        </div>
                    </div>
                </div>
            </section>
            <h3>Beneficiario</h3>
            <section>
                <div class="row" style="padding-bottom:10px;">
                    <div class="col-md-6">
                        Buscar Beneficiario Por:
                    </div>
                    <div class="col-md-3">
                        &nbsp;
                    </div>
                    <div class="col-md-3">
                        <label><input type="radio" name="optradioB" value="n" checked>Nombres</label>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <select class="buscarBeneficiario form-control" name="buscarBeneficiario" id="buscarBeneficiario"></select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
							<input type="hidden" id="id-bene" name="id-bene" value="0">
                            <label class="control-label" for="nombresB"> Nombres</label>
                            <input id="nombresB" name="nombresB" type="text" class="form-control required" autocomplete="name" placeholder="Pedro Jose"/>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="pais"> Pais</label>
                            <select id="pais" name="pais" class="pais form-control required" autocomplete="country-name" readonly>
							 <option value="<?php echo $Envios['id_pais'];?>"><?php echo $Envios['pais_beneficiario'];?></option>
							</select>
                        </div>
                    </div>
					 <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="ciudadB"> Ciudad</label>
                            <input id="ciudadB" name="ciudadB" type="text" class="form-control required" autocomplete="address-level2" placeholder="Madrid"/>
                        </div>
                    </div>
                </div>
            </section>
			<h3>Envio</h3>
            <section>
                <div class="row">
                    <div class="col-md-3 col-xs-12">
					<label class="control-label" for="fecha"> Fecha</label>
					<div class="input-group date" data-provide="datepicker">
						<input type="text" id="fecha" name="fecha" class="form-control required" value="<?php echo $Envios['fecha_envio'];?>" readonly />
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-th"></span>
						</div>
					</div>
                    </div>
                    <div class="col-md-3 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="nenvio"> Nro Envio</label>
                            <input id="nenvio" name="nenvio" type="text" class="form-control" placeholder="000000"/>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="tpago"> Tipo Pago</label>
                           <select id="tpago" name="tpago" class="form-control required" readonly>
                              <option value="<?php echo $Envios['id_pago']; ?>"><?php echo $Envios['tpago_envio']; ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="monto"> Monto</label>
                            <input id="text" name="monto" type="number" class="form-control required" placeholder="00.00"  pattern="^\d+(?:\.\d{1,2})?$" value="<?php echo $Envios['monto'];?>" readonly />
                        </div>
                    </div>
					<div class="col-md-12 text-center">
               <button type="submit" class="btn btn-info btn-raised"><i class="fa fa-save"></i> Guardar</button>
            </div>
                </div>
            </section>
        </div>
        </form>
    </div>
</div>