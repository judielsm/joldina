<?php
include ("../inc/config.php");
$nacionalidad = $_GET['n'];
$pago = $_GET['p'];
$envios = $_GET['s'];
$compra = $_GET['c'];

$strQuery = "SELECT
				C.*,
                paises.nombre as pais,
                (SELECT COUNT(P.Id) FROM pagos P WHERE P.id_cliente= C.id) AS pagosCount,
                (SELECT SUM(P.monto) FROM pagos P WHERE P.id_cliente= C.id) AS pagoMonto,
                (SELECT COUNT(E.Id) FROM envios E WHERE E.id_cliente= C.id) AS enviosCount,
                (SELECT SUM(E.monto) FROM envios E WHERE E.id_cliente= C.id) AS enviosMonto,
                 (SELECT COUNT(CM.Id) FROM productos_ventas CM WHERE CM.id_cliente= C.id) AS comprasCount,
                 (SELECT SUM(CM.total) FROM productos_ventas CM WHERE CM.id_cliente= C.id) AS comprasMonto
               
			FROM
                 clientes C
             LEFT OUTER JOIN 
                paises ON paises.id = C.nacionalidad

            WHERE
                C.lista_negra = 1
                 ";


$clientes= $mysqli->query($strQuery);
$json = array();
while($row = $clientes->fetch_array()){
     $json['data'][] = $row;
}	

$clientes->close();

echo json_encode($json);

?>
