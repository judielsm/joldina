<?php
include ("../inc/config.php");			

$fechaIni = $_POST['fecha_ini']; 
$fechaIni = explode('-', $fechaIni);
$fechaIni = $fechaIni[2].'-'.$fechaIni[1].'-'.$fechaIni[0];
$fechaFin = $_POST['fecha_fin']; 
$fechaFin = explode('-', $fechaFin);
$fechaFin = $fechaFin[2].'-'.$fechaFin[1].'-'.$fechaFin[0];
$idSucursal  = $_POST['sucursal']; 
$formatoTipo = $_POST['formato'];

if($idSucursal=="100"){
	
	$Query    = "SELECT envios.id AS NUM, DATE_FORMAT(envios.fecha, '%d-%m-%Y') FECHA, sucursal.nombre AS SUCURSAL, envios.n_envio AS NENVIO, tipo_doc.nombre AS TDOC,";  
	$Query   .= "clientes.documento AS DOCUMENTO,clientes.nombres AS REMITENTE, UPPER(clientes.direccion) DIRECCION, UPPER(clientes.ciudad) CIUDAD, clientes.telefono AS TELEFONO,";  
	$Query   .= "beneficiarios.nombres AS BENEFICIARIO, UPPER(beneficiarios.ciudad) CIUDAD4,paises.iso AS PAIS, UPPER(tipo_pago.nombre) TPAGO, envios.monto AS MONTO,";  
	$Query   .= "remesadora.nombre AS REMESADORA, pagador.nombre AS PAGADOR FROM envios, sucursal, clientes, beneficiarios, remesadora, pagador, paises, tipo_doc,";  
	$Query   .= "tipo_pago WHERE envios.devolucion = 0 AND  envios.id_sucursal = sucursal.id AND  envios.id_cliente = clientes.id AND  clientes.tipo_doc = tipo_doc.id AND "; 
	$Query   .= "envios.id_beneficiario = beneficiarios.id AND beneficiarios.id_pais = paises.id AND envios.id_pago = tipo_pago.id AND envios.id_remesadora = remesadora.id AND "; 
	$Query   .= "envios.id_pagador = pagador.id AND envios.fecha BETWEEN('".$fechaIni."') AND ('".$fechaFin."') ORDER BY  NUM ASC";
	
}else{
		
	$Query    = "SELECT envios.id AS NUM, DATE_FORMAT(envios.fecha, '%d-%m-%Y') FECHA, sucursal.nombre AS SUCURSAL, envios.n_envio AS NENVIO, tipo_doc.nombre AS TDOC,";  
	$Query   .= "clientes.documento AS DOCUMENTO,clientes.nombres AS REMITENTE, UPPER(clientes.direccion) DIRECCION, UPPER(clientes.ciudad) CIUDAD, clientes.telefono AS TELEFONO,";  
	$Query   .= "beneficiarios.nombres AS BENEFICIARIO, UPPER(beneficiarios.ciudad) CIUDAD4,paises.iso AS PAIS, UPPER(tipo_pago.nombre) TPAGO, envios.monto AS MONTO,";  
	$Query   .= "remesadora.nombre AS REMESADORA, pagador.nombre AS PAGADOR FROM envios, sucursal, clientes, beneficiarios, remesadora, pagador, paises, tipo_doc,";  
	$Query   .= "tipo_pago WHERE envios.devolucion = 0 AND  envios.id_sucursal = sucursal.id AND  envios.id_cliente = clientes.id AND  clientes.tipo_doc = tipo_doc.id AND "; 
	$Query   .= "envios.id_beneficiario = beneficiarios.id AND beneficiarios.id_pais = paises.id AND envios.id_pago = tipo_pago.id AND envios.id_remesadora = remesadora.id AND "; 
	$Query   .= "envios.id_pagador = pagador.id AND envios.fecha BETWEEN('".$fechaIni."') AND ('".$fechaFin."') AND envios.id_sucursal = '".$idSucursal."' ORDER BY  NUM ASC";	
		
}
if($formatoTipo=="1"){
?>
<?php	
header('Content-type: application/vnd.ms-excel;charset=utf-8');
header('Content-Disposition: attachment; filename=envios-'.date('d-m-Y').'.xls');

	$Reporte = $mysqli->query($Query); ?>

	<table border="1" cellpadding="2" cellspacing="0" width="100%"> 
	  <tr>
		<td bgcolor="#C4D79B" align="center"><b>NUM</b></td>
		<td bgcolor="#C4D79B" align="center"><b>FECHA</b></td>
		<td bgcolor="#C4D79B" align="center"><b>SUCURSAL</b></td>
		<td bgcolor="#C4D79B" align="center"><b>N ENVIO</b></td>
		<td bgcolor="#C4D79B" align="center"><b>T. DOC</b></td>
		<td bgcolor="#C4D79B" align="center"><b>DOCUMENTO</b></td>
		<td bgcolor="#C4D79B" align="center"><b>REMITENTE</b></td>
		<td bgcolor="#C4D79B" align="center"><b>DIRECCI&Oacute;N</b></td>
		<td bgcolor="#C4D79B" align="center"><b>CIUDAD</b></td>
		<td bgcolor="#C4D79B" align="center"><b>TEL&Eacute;FONO</b></td>
		<td bgcolor="#C4D79B" align="center"><b>BENEFICIARIO</b></td>
		<td bgcolor="#C4D79B" align="center"><b>CIUDAD4</b></td>
		<td bgcolor="#C4D79B" align="center"><b>PAIS</b></td>
		<td bgcolor="#C4D79B" align="center"><b>T. PAGO</b></td>
		<td bgcolor="#C4D79B" align="center"><b>MONTO</b></td>
		<td bgcolor="#C4D79B" align="center"><b>REMESADORA</b></td>
		<td bgcolor="#C4D79B" align="center"><b>PAGADOR</b></td>
	  </tr>
	<?php while($row = $Reporte->fetch_array()){  
			if ($colordefila==0){
				$color= "#ffffff";
				$colordefila=1;
			 }else{
				$color="#DCE6F1";
				$colordefila=0;
			 }
	?>
	  <tr>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['NUM'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['FECHA'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['SUCURSAL'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['NENVIO'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['TDOC'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['DOCUMENTO'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo utf8_decode($row['REMITENTE']);?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo utf8_decode($row['DIRECCION']);?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo utf8_decode($row['CIUDAD']);?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['TELEFONO'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo utf8_decode($row['BENEFICIARIO']);?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo utf8_decode($row['CIUDAD4']);?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo utf8_decode($row['PAIS']);?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['TPAGO'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo number_format($row['MONTO'], 2, ',', '.');?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo utf8_decode($row['REMESADORA']);?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo utf8_decode($row['PAGADOR']);?></td>
	  </tr>
<?php	} ?>
	</table>
<?php	
}else{

header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename=envios-'.date('d-m-Y').'.csv');
?>
NUM;FECHA;SUCURSAL;N ENVIO;T DOC;DOCUMENTO;REMITENTE;DIRECCION;CIUDAD;TELEFONO;BENEFICIARIO;CIUDAD4;PAIS;T PAGO;MONTO;REMESADORA;PAGADOR
<?php
	$Reporte = $mysqli->query($Query);
	while($row = $Reporte->fetch_array()){   
echo$row['NUM'].";".$row['FECHA'].";".$row['SUCURSAL'].";".$row['NENVIO'].";".$row['TDOC'].";".$row['DOCUMENTO'].";".utf8_decode($row['REMITENTE']).";".utf8_decode($row['DIRECCION']).";".utf8_decode($row['CIUDAD']).";".$row['TELEFONO'].";".utf8_decode($row['BENEFICIARIO']).";".utf8_decode($row['CIUDAD4']).";".utf8_decode($row['PAIS']).";".$row['TPAGO'].";".number_format($row['MONTO'], 2, ',', '.').";".utf8_decode($row['REMESADORA']).";".utf8_decode($row['PAGADOR'])."\n"; }		
}
?>
