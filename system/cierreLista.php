<?php
   if ($_SESSION["joldin_log"] == true){
   }else{
       header("Location: ../index.php");
   }
   ?>
<script type="text/javascript">
$(document).ready(function() {
    $('#datatable-responsive').dataTable({
        ajax: 'cierreBusca.php?id=<?php echo $_SESSION["joldin_suc"]; ?>',
		sorting: [[0, 'desc']],
        columns: [
		{
                orderable: false,
                visible: false,
                data: "id"
            },
		{data: "fecha"},
		{data: "cantidad_500"}, 
		{data: "cantidad_200"},
		{data: "cantidad_100"},
		{data: "cantidad_50"}, 
		{data: "cantidad_20"}, 
		{data: "cantidad_10"}, 
		{data: "cantidad_5"}, 
		{data: "cantidad_2"}, 
		{data: "cantidad_1"},
		{data: "cantidad_050"}, 
		{data: "cantidad_020"}, 
		{data: "cantidad_010"},
		{searchable: false,sortable: false,
		render: function ( data, type, row, meta ) {
                    if(row[1]=='<?php echo date('d-m-Y')?>'){
								return '<a href="index.php?page=cierreDetalle&id=' + row[0] + '" class="btn btn-icon waves-effect btn-info btn-xs" title="Ver"><i class="typcn typcn-eye"></i> </a>&nbsp;' +
									   '<a href="index.php?page=cierreEdita&id=' + row[0] + '"' + 'class="btn btn-icon waves-effect btn-info btn-xs" title="Editar"> <i class="typcn typcn-pencil"></i> </a>'
							}else{
								return '<a href="index.php?page=cierreDetalle&id=' + row[0] + '" class="btn btn-icon waves-effect btn-info btn-xs" title="Ver"><i class="typcn typcn-eye"></i> </a>' 
							}
               
            }
            }

        ]

    });
});
window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function() {
        $(this).remove();
    });
}, 5000);
</script>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Cierres</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<?php
   $strAccion = $_GET['menssage'];
   switch ($strAccion) {
   case "add":
   ?>
<div class="row">
   <div class="alert alert-success col-md-12" align="center">
      <i class="mdi mdi-check-all"></i> El Cierre Fue Agregado.
   </div>
</div>
<?php
   break;
   case "edit":
?>   
<div class="row">
   <div class="alert alert-warning col-md-12" align="center">
      <i class="mdi mdi-check-all"></i> El Cierre Fue Editado.
   </div>
</div>
<?php
break;
   }
   ?>
<?php if($_SESSION["joldin_rol"]=="1"){ ?>
<form id="frm-exportar" action="cierresExportar.php" method="post" enctype="multipart/form-data">
<div class="row">
	<div class="col-md-3">
	<label class="control-label" for="fecha_ini"> Fecha Inicio</label>
				<div class="input-group date" data-provide="datepicker">
				<input type="text" id="fecha_ini" name="fecha_ini" class="form-control required" value="<?php echo date("d-m-Y"); ?>">
				<div class="input-group-addon">
			<span class="glyphicon glyphicon-th"></span>
		</div>
	</div>						
	</div>
	<div class="col-md-3">
	<label class="control-label" for="fecha_fin"> Fecha Fin</label>
				<div class="input-group date" data-provide="datepicker">
				<input type="text" id="fecha_fin" name="fecha_fin" class="form-control required" value="<?php echo date("d-m-Y"); ?>">
				<div class="input-group-addon">
			<span class="glyphicon glyphicon-th"></span>
		</div>
	</div>			
	</div>
	<div class="col-md-2">
		<div class="form-group">
                     <label class="control-label" for="sucursal"> Sucursal</label>
                     <select class="form-control" name="sucursal" id="sucursal">
                        <?php
								$strQuery = "SELECT * FROM sucursal ORDER BY id ASC";
								$Sucursales= $mysqli->query($strQuery);
								while($Suc = $Sucursales->fetch_array()) {?>
									<option value="<?php echo $Suc['id']; ?>"><?php echo $Suc['nombre']; ?></option>
								<?php	 
								}
								$Sucursales->close();
							?>	
                     </select>
                  </div>		
	</div>
	<div class="col-md-4 text-center" style="padding-top: 27px;">
		<button type="submit" id="formato" name="formato" value="1" class="btn w-lg btn-teal waves-effect w-md waves-light m-b-5"><i class="mdi mdi-file-excel-box"></i> Exportar Excel</button>	
	</div>
</div>
</form>
 <?php } ?>
<div class="row">
   <div class="card-box table-responsive">
      <div class="row">
         <div class="col-sm-12">
           <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap text-center text-tabla" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th class="text-center">ID</th>	
					<th class="text-center">Fecha</th>					
					<th class="text-center">500,00 €</th> 
					<th class="text-center">200,00 €</th>
					<th class="text-center">100,00 €</th>
					<th class="text-center">50,00 €</th> 
					<th class="text-center">20,00 €</th> 
					<th class="text-center">10,00 €</th>
					<th class="text-center">5,00 €</th>
					<th class="text-center">2,00 €</th>
					<th class="text-center">1,00 €</th>
					<th class="text-center">0,50 €</th>
					<th class="text-center">0,20 €</th>
					<th class="text-center">0,10 €</th>
					<th class="text-center">Opciones</th>
				</tr>
			</thead>
		  </table>
         </div>
      </div>
   </div>
</div>											