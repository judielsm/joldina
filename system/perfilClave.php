<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
$strQuery = "SELECT e.id as id_em,e.nombres as nombre_emp,s.id as id_su,s.nombre as nombre_su,e.email as email_em,e.ingo as clave_em,e.rol as rol_em FROM empleados as e, sucursal as s WHERE e.id=".$_SESSION["joldin_ide"]." AND e.id_sucursal=s.id";
 $strEmpleado = $mysqli->query($strQuery);
 $Emp = $strEmpleado->fetch_array(); 
?>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Perfil</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<div class="row">
<div class="col-md-4 col-xs-12">&nbsp;</div>
<div class="col-md-4 col-xs-12">
<div class="card-box">
   <div class="row">
      <div class="text-center">
         <div class="member-card">
		<form action="index.php?page=acciones&accion=perfilEdita" method="post" enctype="multipart/form-data">
		 <input type="hidden" id="validator" name="validator" value="ok">
		 <input type="hidden" id="id" name="id" value="<?php echo $_SESSION["joldin_ide"]; ?>">
            <div class="thumb-xl member-thumb m-b-10 center-block">
               <img src="../img/avatar.jpg" class="img-circle img-thumbnail" alt="profile-image">
            </div>
            <div class="text-center">
               <p class="text-muted font-13"> <span class="m-l-15">
			   <input class="form-control" type="text" id="pass" name="pass" placeholder="Nueva Clave" required></span>
			   </p>
            </div>
            <hr/>
            <button type="submit" class="btn btn-info btn-sm w-sm waves-effect m-t-10 waves-light"><i class="mdi mdi-lock-open"></i> Actualizar Clave</button>
			</form>
         </div>
      </div>
   </div>
   <div class="col-md-4 col-xs-12">&nbsp;</div>
</div>