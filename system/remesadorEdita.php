<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
$strQuery = "SELECT * FROM remesadora WHERE id=".$_GET['id']."";
 $strRemesador = $mysqli->query($strQuery);
 $Re = $strRemesador->fetch_array(); 
?>						
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Datos Remesador</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<div class="row">
   <div class="card-box">
      <form action="index.php?page=acciones&accion=remesadorEdita" method="post" enctype="multipart/form-data">
         <input type="hidden" name="validator" id="validator" value="ok" />
         <input type="hidden" name="id" id="id" value="<?php echo $_GET['id']; ?>" />
         <div class="row">
            <div class="col-md-4 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="nombre"> Nombre</label>
                  <input type="text" class="form-control" name="nombre" id="nombre" placeholder="RIA" value="<?php echo $Re['nombre']; ?>" required />
               </div>
            </div>
            <div class="col-md-2 col-xs-12" align="center">
               <img src="../remesador/<?php echo $Re['imagen']; ?>" title="<?php echo $Re['nombre']; ?>" height="42" width="42" class="img-thumbnail img-responsive">
            </div>
            <div class="col-md-6 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="imagen"> Imagen</label>
                  <input type="file" class="form-control" name="imagen" id="imagen"  />
               </div>
            </div>
         </div>
         <div class="row mb-4">
            <div class="col-md-2 col-xs-1">&nbsp;</div>
            <div class="col-md-8 col-xs-10 text-center">
               <button type="submit" class="btn btn-warning btn-raised"><i class="fa fa-pencil"></i> Editar</button>
            </div>
            <div class="col-md-2 col-xs-1">&nbsp;</div>
         </div>
      </form>
   </div>
</div>