<?php
   if($_SESSION["joldin_log"] == TRUE) {
   	} else {
   	header("Location: ../index.php");
   }
   
   $Query = "SELECT
    pagos.fecha AS fecha_pa,
    pagos.n_pago AS numero_pa,
    sucursal.nombre AS su_pago,
    pagos.monto AS mo_pago,
    clientes.nombres AS be_nombre,
    tipo_doc.nombre AS be_tipo,
    clientes.documento AS be_documento,
    clientes.ciudad AS be_ciudad,
    remesadora.nombre AS pa_remesadora
FROM
	pagos, 
    clientes, 
    sucursal, 
    tipo_doc,
    remesadora
WHERE
   
    pagos.id_sucursal = sucursal.id AND 
    pagos.id_cliente = clientes.id AND 
    clientes.tipo_doc = tipo_doc.id AND
    pagos.id_remesadora = remesadora.id AND
    pagos.id ='".$_GET['id']."'";
   $Busca = $mysqli->query($Query);
   $Pago  = $Busca->fetch_array();
   $Busca -> close();
   
   
   ?>
   <div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Datos Pago</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
   <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
									<div class="clearfix">
                                            <div class="pull-left">
                                               <img src="../img/logo_sm.png"> 
                                            </div>
                                            <div class="pull-right">
											<p>&nbsp;</p>
                                                <h3 style="color:red;"><strong># <?php echo $Pago['numero_pa']; ?></strong></h3>
                                            </div>
                                        </div>
                                        <hr style="margin-top:9px; margin-bottom:1px;">
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="pull-left m-t-30">
                                                    <address>
                                                      <strong>Remesadora</strong><br>
                                                      <?php echo $Pago['pa_remesadora']; ?>
                                                      </address>
                                                </div>
                                                <div class="pull-right m-t-30">
                                                    <p><strong>Fecha: </strong> <?php echo $Pago['fecha_pa']; ?></p>
                                                    <p><strong>Sucursal: </strong> <span class="label label-info"><?php echo $Pago['su_pago']; ?></span></p>
                                                </div>
                                            </div>
                                        </div>
										
										<div class="row">
                                            <div class="col-md-12">
											<span class="label label-info"> <strong>Beneficiario</strong></span>
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
																<th width="50%">Nombres</th>
																<th width="10%" class="text-center">T. Doc.</th>
																<th width="20%" class="text-center">Documento</th>
																<th width="20%" class="text-center">Ciudad</th>
                                                            </tr>
														</thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><?php echo $Pago['be_nombre']; ?></td>
																<td class="text-center"><?php echo $Pago['be_tipo']; ?></td>
																<td class="text-center"><?php echo $Pago['be_documento']; ?></td>
																<td class="text-center"><?php echo $Pago['be_ciudad']; ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                       
											<div class="row">
                                            <div class="col-md-9">
                                                   &nbsp; 
                                            </div>
                                            <div class="col-md-3">
											 <hr>
                                                <h4 class="text-right text-info"><b>Monto: <?php echo $Pago['mo_pago']; ?>  €</b></h4>
                                            </div>
											</div>
                                        <div class="hidden-print" style="padding-top:20px;">
                                            <div class="pull-right">
                                                <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i> Imprimir</a>
                                                <a href="index.php?page=pagosLista" class="btn btn-info waves-effect waves-light"><i class="mdi mdi-keyboard-backspace"></i> Atras</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div> 