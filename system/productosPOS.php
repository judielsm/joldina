<?php
if ($_SESSION["joldin_log"] == TRUE) {
} else {
    header("Location: ../index.php");
}
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/node-uuid/1.4.7/uuid.min.js"></script>

<script type="text/javascript">
$(function() {
	$('.producto').select2({
        placeholder: 'Seleccione uno...',
        ajax: {
           url: 'productosB.php',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      });
	
	$selecClientes = $("#buscarCliente").select2({
        allowClear: true,
        tags: true,
        language: "es",
        ajax: {
            url: 'clientes.php',
            dataType: 'json'
        }
    });
	$selecPaises = $('#pais').select2({
		tags:true,
        language: "es",
		allowClear: true,
        ajax: {
            url: 'enviosPais.php',
            dataType: 'json',
            processResults: function(data) {
                return {
                    results: data
                };
            },
			
        }
    });
	$("#buscarCliente").on("select2:select", function() {
        cargarCliente();
    });
 });
function iniciarBusquedaPais() {
		$selecPaises = $('#pais').select2({
		tags:true,
        language: "es",
		allowClear: true,
        ajax: {
            url: 'enviosPais.php',
            dataType: 'json',
            processResults: function(data) {
                return {
                    results: data
                };
            },
			
        }
    });
}

function cargarCliente() {
    var id = $("#buscarCliente").val();
    console.log(id);
    $.ajax({
        url: 'clienteCarga.php',
        type: 'POST',
        data: "id=" + id,
        dataType: "JSON",
        success: function(per) {
            console.log(per);
            if (per.cant > 0) {
                var dat = per.datos;
                $("#id-cliente").val(dat.id);
                $("#nombres").val(dat.nombres);
                $("#tipo_id").val(dat.tipo_doc);
                $("#codigo_zip").val(dat.codigo_postal);
				$("#email").val(dat.email);
				$("#ocupacion").val(dat.ocupacion);
				$("#residente").val(dat.residente);
				$("#n_doc").val(dat.documento);
                $("#direccion").val(dat.direccion);
                $("#ciudad").val(dat.ciudad);
                $("#telefono").val(dat.telefono);
               var data = [{
                    id: dat.nacionalidad,
                    text: dat.pais_iso+" | "+dat.pais_nombre
                }];

                $('#pais').select2({
                    data: data
                });
                $('#pais').val(dat.nacionalidad);
                iniciarBusquedaPais();
				iniciarBusquedaBene();
            } else {
                $("#id-cliente").val('');
                $("#nombres").val('');
                $("#tipo_id").val('');
                $("#codigo_zip").val('');
				$("#email").val('');
				$("#ocupacion").val('');
				$("#residente").val('');
				$("#n_doc").val('');
                $("#direccion").val('');
                $("#ciudad").val('');
                $("#telefono").val('');
				$("#pais").val('').trigger('change');
            }
            $selecClientes.val(null).trigger("change");
        }
    });
} 
async function addProducto() {
    var id = uuid.v1();
    var idm = '"'+id+'"';
    console.log('id generado' + id);
    var productos = await $.get('productosB.php');
    console.log(productos);
    htmlList = '';
   JSON.parse(productos).forEach(function (producto) {
        
        htmlList +='<div class="col-md-3 pr" id="pr_'+id+'" data-id="'+producto.id+'"> <img class="img-ressponsive" src="'+producto.src+'"> <span>'+producto.text+'</span> </div>';
    })
    var htmlProductos = '<div class="form-group">'+
                '<div class="row"> '+
                htmlList+
                '</div>'+
               ' <div class="row">'+    
                   ' <div class="col-md-2">'+
                       ' <div class="form-group">'+
                          '  <label class="control-label" for="coste"> Coste</label>'+
                           ' <input id="coste_' +id+ '" name="coste[]" type="number" class="form-control coste" placeholder="00.00" readonly/>'+
                      '  </div>'+
                  '  </div>'+
                  ' <div class="col-md-2">'+
                      '  <div class="form-group">'+
                          '  <label class="control-label" for="cantidad"> Cantidad</label>'+
                          '  <input name="cantidad[]" onchange="calcular(\''+id+'\')" id="cant_' +id+ '" type="number" class="form-control" placeholder="00.00" />'+
                       ' </div>'+
                   ' </div>'+
					'<div class="col-md-3">'+
                       ' <div class="form-group">'+
                            '<label class="control-label" for="total"> Total</label>'+
                           ' <input name="total[]" readonly id="total_' +id+ '" type="number" class="form-control" placeholder="00.00"  pattern="^\d+(?:\.\d{1,2})?$"/>'+
                      '  </div>'+
                  '  </div>'+
					'<a href="#" onclick="removeProducto(this)" class="btn btn-info btn-xs" style="height: 20px;margin-top: 32px;"><i class="fa fa-minus"></i></a>' +
               ' </div>'+
				'</div>';
  $('.input-productos').append(htmlProductos);
  $('.pr').click(function (e) {
      var id = $(this).attr('data-id');
      elemento = $(this).attr('id').split('_')[1];
      cargarMonto(id, elemento  );
  })
  $('.producto').select2({
        placeholder: 'Seleccione uno...',
        ajax: {
           url: 'productosB.php',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      });
	$(".producto").on("select2:select", function() {
	    elemento = $(this).attr('id').split('_')[1];
	    console.log(elemento);
	    id = $(this).val();
	    console.log('este es el ida a buscar'+ id);
        cargarMonto(id, elemento);
    });
	window.cargarMonto = function (id, input) {
        cargarMonto(id, input);
    }
	function cargarMonto(id, input) {
	 //var id = $(".producto").val();
	 console.log(id);
	  $.ajax({
        url: 'productoCarga.php',
        type: 'POST',
        data: "id=" + id,
        dataType: "JSON",
        success: function(div) {
			console.log(div);
            if (div.cant > 0) {
				var dat = div.datos;
				$("#coste_"+elemento).val(dat.precio);
			} else {
                $("#coste_"+elemento).val('');
				$(".producto").val('').trigger('change');
			}
		}
    });
	}


	
}
function calcular(id) {
    console.log(id);
    coste = $("#coste_"+id).val();
    cant = $("#cant_"+id).val();
    $("#total_"+id).val((coste*cant).toFixed(2));
    var total = 0;
    $("[name='total[]']").each(function(){
        total += parseFloat($(this).val());
    });
    $("#sub_total").val(total.toFixed(2));
}

function removeProducto(obj) {
    var parent = $(obj).parent();

    parent.parent().remove();
    var total = 0;
    $("[name='total[]']").each(function(){
        total += parseFloat($(this).val());
    });
    $("#sub_total").val(total.toFixed(2));
}
</script>
<div class="row">
    <div class="col-xs-12">
        <div class="page-title-box">
            <h4 class="page-title">Datos Venta</h4>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="card-box">
		<form id="frm-envio" action="index.php?page=acciones&accion=comprasPOS" method="post" enctype="multipart/form-data" novalidate>
		<input type="hidden" name="validator" id="validator" value="ok" />
		 <input type="hidden" name="sucursal" id="sucursal" value="<?php echo $_SESSION["joldin_suc"];?>" />
		 <div>
            <!--<h3>Cliente</h3> -->
            <!-- <section>
                <div class="row" style="padding-bottom:10px;">
                    <div class="col-md-12 " align="left">
                        Buscar Cliente:
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <select id="buscarCliente" name="buscarCliente" class="form-control"></select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <input type="hidden" id="id-cliente" name="id-cliente">
                            <label class="control-label" for="nombres"> Nombres</label>
                            <input id="nombres" name="nombres" type="text" class="form-control" placeholder="Pedro Jose"/>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="tipo_id"> Tipo Doc</label>
                            <select id="tipo_id" name="tipo_id" class="form-control">
                                <?php
                                $strQuery = "SELECT * FROM tipo_doc";
                                $strBuscaTD = $mysqli->query($strQuery);
                                while ($strTD = $strBuscaTD->fetch_array()) {

                                    ?>
                                    <option value="<?php echo $strTD['id']; ?>"><?php echo $strTD['nombre']; ?></option>
                                    <?php
                                }
                                $strBuscaTD->close();
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="n_doc"> N Doc</label>
                            <input id="n_doc" name="n_doc" type="text" class="form-control" placeholder="0000000000"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label" for="direccion"> Dirección</label>
                            <input id="direccion" name="direccion" type="text" class="form-control"  placeholder="Calle Alcala"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label" for="codigo_zip"> Codigo Postal</label>
                            <input id="codigo_zip" name="codigo_zip" type="number" class="form-control" placeholder="31002"/>
                        </div>
                    </div>
					<div class="col-md-3">
                           <div class="form-group">
                            <label class="control-label" for="tipo_id"> Residente</label>
                            <select id="residente" name="residente" class="form-control">
                               <option value="1">SI</option>
							   <option value="0">NO</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label" for="ciudad"> Ciudad</label>
                            <input id="ciudad" name="ciudad" type="text" class="form-control" placeholder="Madrid"/>
                        </div>
                    </div>
                </div>
				 <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label" for="telefono"> Teléfono</label>
                            <input id="telefono" name="telefono" type="tel" class="form-control "  placeholder="0000000000"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label" for="email"> Email</label>
                            <input id="email" name="email" type="email" class="form-control " placeholder="email@tudominio.com"/>
                        </div>
                    </div>
					<div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label" for="pais"> Nacionalidad</label>
                              <select id="pais" name="pais" class="pais form-control"></select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label" for="ocupacion"> Ocupación</label>
                            <input id="ocupacion" name="ocupacion" type="text" class="form-control" placeholder="Empleado"/>
                        </div>
                    </div>
                </div>
            </section> -->
			<h3>Productos</h3>
            <section>
                <div style="padding-top:30px;">
                    <a  onclick="addProducto()" class="btn btn-info btn-xs"><i class="fa fa-plus"></i></a>
                </div>
                <div>
				<div class="input-productos">

				</div>
				</div>
				
				<div class="row">
				 <div class="col-md-3">&nbsp;</div>
				 <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="sub_total"> Sub Total</label>
                            <input id="sub_total" name="sub_total" type="number" class="form-control" placeholder="00.00" readonly/>
                        </div>
                    </div>
					 <div class="col-md-3">&nbsp;</div>
				 </div>
				 <div class="row">
			 <div class="col-md-12 text-center">
               <button type="submit" class="btn btn-info btn-raised"><i class="fa fa-save"></i> Guardar</button>
            </div>
			 </div>
            </section>

			
        </div>
        </form>
    </div>
</div>