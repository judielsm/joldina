<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
?>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Datos Entrega</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<div class="row">
   <div class="card-box">
        <form action="index.php?page=acciones&accion=entregaNuevo" method="post" enctype="multipart/form-data">
         <input type="hidden" name="validator" id="validator" value="ok" />
		 <input type="hidden" name="empleado" id="empleado" value="<?php echo $_SESSION["joldin_ide"]; ?>" />
		 <input type="hidden" name="suc" id="suc" value="<?php echo  $_SESSION["joldin_suc"]; ?>" />
		 <div class="row">
            <div class="col-md-4 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="fecha"> Fecha</label>
                  <input type="text" class="form-control" name="fecha" id="fecha" value="<?php echo  date('d-m-Y');?>" readonly />
               </div>
            </div>
			<div class="col-md-4 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="hora"> Hora</label>
                  <input type="text" class="form-control" name="hora" id="hora" value="<?php echo  date('H:i:s');?>"  readonly />
               </div>
            </div>
            <div class="col-md-4 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="monto"> Monto</label>
                  <input type="text" class="form-control" name="monto" id="monto" placeholder="00.00"  pattern="^\d+(?:\.\d{1,2})?$" required/>
               </div>
            </div>
         </div>
		   <div class="row">
            <div class="col-md-12 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="comentario"> Comentario</label>
                  <textarea rows="2" class="form-control" name="comentario" id="comentario"></textarea>
               </div>
            </div>
         </div>
         <div class="row mb-4">
            <div class="col-md-2 col-xs-1">&nbsp;</div>
            <div class="col-md-8 col-xs-10 text-center">
               <button type="submit" class="btn btn-info btn-raised"><i class="fa fa-save"></i> Guardar</button>
            </div>
            <div class="col-md-2 col-xs-1">&nbsp;</div>
         </div>
      </form>
   </div>
</div>