<?php
include ("../inc/config.php");
session_start();
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
	$bTicket = $mysqli->query("SELECT 
clientes.nombres nombres,
productos_ventas.fecha fecha,
productos_ventas.total total
FROM
clientes,
productos_ventas
WHERE
productos_ventas.id_cliente=clientes.id AND
productos_ventas.id ='".$_GET['id']."'");
    $cTicket  = $bTicket->fetch_array();	
?>
<!DOCTYPE html>
<html>

<head>
    <style>
	* {
    font-size: 12px;
    font-family: 'Times New Roman';
}

td,
th,
tr,
table {
    border-top: 1px solid black;
    border-collapse: collapse;
}

td.producto,
th.producto {
    width: 75px;
    max-width: 75px;
}

td.cantidad,
th.cantidad {
    width: 40px;
    max-width: 40px;
    word-break: break-all;
}

td.precio,
th.precio {
    width: 40px;
    max-width: 40px;
    word-break: break-all;
}

.centrado {
    text-align: center;
    align-content: center;
}

.ticket {
    width: 170px;
    max-width: 170px;
}


	</style>
</head>

<body onload="window.print()">
    <div class="ticket">
        <div align="center"><img src="../img/brand.png" alt="Logotipo"></div>
        <p class="centrado">TICKET DE VENTA
            <br><?=$cTicket['nombres'];?>
            <br><br><?=$cTicket['fecha'];?></p>
        <table>
            <thead>
                <tr>
                    <th class="cantidad">CANT</th>
                    <th class="producto">PRODUCTO</th>
                    <th class="precio">€</th>
                </tr>
            </thead>
            <tbody>
			<?php
			$Busca = $mysqli->query("SELECT 
productos.nombre producto,
productos_ventas_detalle.cantidad cantidad,
productos_ventas_detalle.monto monto
FROM 
productos,
productos_ventas_detalle
WHERE
productos_ventas_detalle.id_producto=productos.id AND
productos_ventas_detalle.id_venta='".$_GET['id']."'");
			while($Pago  = $Busca->fetch_array()){
			?>
                <tr>
                    <td class="cantidad"><?=$Pago['cantidad'];?></td>
                    <td class="producto"><?=$Pago['producto'];?></td>
                    <td class="precio">€ <?=$Pago['monto'];?></td>
                </tr>
			<?php
			}
			?>	
                <tr>
                    <td class="cantidad"></td>
                    <td class="producto">TOTAL</td>
                    <td class="precio">€ <?=$cTicket['total'];?></td>
                </tr>
            </tbody>
        </table>
        <p class="centrado">¡GRACIAS POR SU COMPRA!</p>
    </div>
</body>

</html>