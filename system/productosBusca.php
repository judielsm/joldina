<?php
include ("../inc/config.php");

	
$sql = "SELECT
    productos.id id,
    productos.nombre nombre,
    productos_categoria.nombre categoria,
    productos.estock stock,
    productos.precio importe
FROM
    productos,
    productos_categoria
WHERE
    productos.id_categoria = productos_categoria.id AND productos.estock > 0";


				
				
$result = $mysqli->query($sql);
$json = array();
while($row = $result->fetch_array()){
     $json['data'][] = $row;
}	

$result->close();		
echo json_encode($json);		

?>