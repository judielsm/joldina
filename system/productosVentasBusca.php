<?php
include ("../inc/config.php");

$sql = "SELECT
			productos_ventas.id id,
			DATE_FORMAT(
				productos_ventas.fecha,
				'%d-%m-%Y'
			) fecha,
			sucursal.nombre sucursal,
			clientes.nombres cliente,
			productos_ventas.total importe
		FROM
			productos_ventas,
			sucursal,
			clientes
		WHERE
			productos_ventas.id_sucursal = sucursal.id AND 
			productos_ventas.id_cliente = clientes.id AND 
			DATE_FORMAT(productos_ventas.fecha, '%d-%m-%Y') ='".date('d-m-Y')."'";

if($_GET['id'] !="100"){
	$sql .= " AND productos_ventas.id_sucursal='".$_GET['id']."'";
}

$result = $mysqli->query($sql);
$json = array();
while($row = $result->fetch_array()){
     $json['data'][] = $row;
}	

$result->close();		
echo json_encode($json);		

?>