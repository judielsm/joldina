<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Cierre del Dia</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<form id="frm-exportar" action="index.php?page=detalleAcumulado" method="post" enctype="multipart/form-data">
<input type="hidden" id="sucursal" name="sucursal" class="form-control" value="<?php echo $_SESSION["joldin_suc"]; ?>" required>
<input type="hidden" id="empleado" name="empleado" class="form-control" value="<?php echo $_SESSION["joldin_ide"]; ?>" required>
<div class="row">
	<div class="col-md-6">
	<label class="control-label" for="fecha_ini"> Fecha</label>
				<div class="input-group date" data-provide="datepicker">
				<input type="text" id="fecha" name="fecha" class="form-control" value="<?php echo date("d-m-Y"); ?>" required>
				<div class="input-group-addon">
			<span class="glyphicon glyphicon-th"></span>
		</div>
	</div>						
	</div>
	<div class="col-md-6 text-center" style="padding-top: 27px;">
		<button type="submit" id="formato" name="formato" value="2" class="btn w-lg btn-info waves-effect w-md waves-light m-b-5"><i class="mdi mdi-eye"></i> Visualizar</button>	
	</div>
</div>
</form>