<?php
if ($_SESSION["joldin_log"] == TRUE) {
} else {
    header("Location: ../index.php");
}
?>
<script type="text/javascript">
$(function() {
$selecCategoria = $('#categoria').select2({
		tags:true,
        language: "es",
		allowClear: true,
        ajax: {
            url: 'productosC.php',
            dataType: 'json',
            processResults: function(data) {
                return {
                    results: data
                };
            },
			
        }
    });	
});
</script>	
<div class="row">
    <div class="col-xs-12">
        <div class="page-title-box">
            <h4 class="page-title">Datos Producto</h4>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="card-box">
		<form id="frm-envio" action="index.php?page=acciones&accion=productosEditar" method="post" enctype="multipart/form-data">
		 <input type="hidden" name="validator" id="validator" value="ok" />
		 <div>
            <section>
                <div class="row">
					<div class="col-md-3 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="categoria"> Categoria</label>
                            <select id="categoria" name="categoria" class="form-control"></select>
                        </div>
                    </div>
					 <div class="col-md-3 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="nombre"> Nombre</label>
                            <input id="nombre" name="nombre" type="text" class="form-control" placeholder="SIM CARD" required />
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="importe"> Importe</label>
                            <input id="importe" name="importe" type="text" class="form-control" placeholder="00.00"  pattern="^\d+(?:\.\d{1,2})?$" required />
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="estock"> Stock</label>
                            <input id="estock" name="estock" type="number" class="form-control" placeholder="100" required />
                        </div>
                    </div>
					<div class="col-md-12 text-center">
               <button type="submit" class="btn btn-info btn-raised"><i class="fa fa-save"></i> Guardar</button>
            </div>
                </div>
            </section>
        </div>
        </form>
    </div>
</div>