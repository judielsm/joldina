<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
 if (is_numeric($_GET['id'])){
$strQuery = "SELECT e.id as id_em,e.nombres as nombre_emp,s.id as id_su,s.nombre as nombre_su,e.email as email_em,e.ingo as clave_em,e.rol as rol_em FROM empleados as e, sucursal as s WHERE e.id=".$_GET['id']." AND e.id_sucursal=s.id";
 $strEmpleado = $mysqli->query($strQuery);
 $Emp = $strEmpleado->fetch_array(); 
?>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Datos Empleado</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<div class="row">
   <div class="card-box">
      <form action="index.php?page=acciones&accion=empleadosEdita" method="post" enctype="multipart/form-data">
	  <input type="hidden" name="validator" id="validator" value="ok" />
	  <input type="hidden" name="id" id="id" value="<?php echo $_GET['id'];?>" />
         <div class="row">
            <div class="col-md-4 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="nombres"> Nombres</label>
                  <input type="text" class="form-control" name="nombres" id="nombres" placeholder="Pedro Jose" value="<?php echo $Emp['nombre_emp'];?>" required/>
               </div>
            </div>
			  <div class="col-md-4 col-xs-6">
                  <div class="form-group">
                     <label class="control-label" for="sucursal"> Sucursal</label>
                     <select class="form-control" name="sucursal" id="sucursal">
                       <option value="<?php echo $Emp['id_su'];?>"><?php echo $Emp['nombre_su'];?></option>	
                     <?php
                        $strQuery = "SELECT * FROM sucursal WHERE id != ".$Emp['id_su']."";
                        $strBuscaTD = $mysqli->query($strQuery);
                        while($strTD = $strBuscaTD->fetch_array()) {
                        
                        ?>
                     <option value="<?php echo $strTD['id'];?>"><?php echo $strTD['nombre'];?></option>
                     <?php } ?>
                     </select>
                  </div>
               </div>
            <div class="col-md-4 col-xs-6">
               <div class="form-group">
                  <label class="control-label" for="tipo_id"> Rol</label>
                  <select class="form-control" name="rol" id="rol">
				  <?php if($Emp['rol_em']=="1"){?>
                     <option value="1" selected>Administrador</option>
					  <option value="2">Empleado</option>
				  <?php }else{ ?>
                     <option value="2" selected>Empleado</option>
					 <option value="1">Administrador</option>
				  <?php } ?>
                  </select>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-6 col-xs-6">
               <div class="form-group">
                  <label class="control-label" for="email"> E-mail</label>
                  <input type="email" class="form-control" name="mail" id="mail" placeholder="nombre@gmail.com" value="<?php echo $Emp['email_em'];?>" required />
               </div>
            </div>
            <div class="col-md-6 col-xs-6">
               <div class="form-group">
                  <label class="control-label" for="clave"> Clave</label>
                  <input type="text" class="form-control" name="clave" id="clave" placeholder="********" value="<?php echo $Emp['clave_em'];?>" required />
               </div>
            </div>
         </div>
         <div class="row mb-4">
            <div class="col-md-2 col-xs-1">&nbsp;</div>
            <div class="col-md-8 col-xs-10 text-center">
                <button type="submit" class="btn btn-warning btn-raised"><i class="fa fa-pencil"></i> Editar</button>
            </div>
            <div class="col-md-2 col-xs-1">&nbsp;</div>
         </div>
      </form>
   </div>
</div>
<?php
	 }else{
		   echo '<meta http-equiv=Refresh content="0; url=index.php?page=empleadosLista&menssage=error">';
 }
?>