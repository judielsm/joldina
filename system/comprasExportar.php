<?php
include ("../inc/config.php");			

$fechaIni = $_POST['fecha_ini']; 
$fechaIni = explode('-', $fechaIni);
$fechaIni = $fechaIni[2].'-'.$fechaIni[1].'-'.$fechaIni[0];
$fechaFin = $_POST['fecha_fin']; 
$fechaFin = explode('-', $fechaFin);
$fechaFin = $fechaFin[2].'-'.$fechaFin[1].'-'.$fechaFin[0];
$idSucursal  = $_POST['sucursal']; 

if($idSucursal=="100"){
	
	$Query    = "SELECT
				ventas.id NUM,
				DATE_FORMAT(ventas.fecha, '%d/%m/%Y') FECHA,
                tipo_doc.nombre TIPO_DOC,
                clientes.documento DOCUMENTO,
                IF(clientes.residente='1', 'SI', 'NO') RESIDENTE,
                clientes.nombres NOMBRES,
                paises.iso NACIONALIDAD,
                ventas.monto IMPORTE,
                divisas.divisa MONEDA,
                divisas.cambio TIPO_CAMBIO,
                FORMAT(ventas.monto * divisas.cambio,2) TOTAL,
                clientes.direccion DIRECCION,
                clientes.telefono TELEFONO,
                divisas.descripcion DESCRIPCION,
                sucursal.nombre SUCURSAL,
                clientes.ocupacion OCUPACION,
                ventas.hora HORA
			FROM
				ventas,
				clientes,
				divisas,
                sucursal,
                tipo_doc,
                paises
			WHERE
				ventas.id_cliente = clientes.id AND
				ventas.id_divisa = divisas.id AND
                ventas.id_sucursal =sucursal.id AND
                clientes.tipo_doc=tipo_doc.id AND
                clientes.nacionalidad=paises.id AND 
				ventas.fecha BETWEEN('".$fechaIni."') AND ('".$fechaFin."') ORDER BY  NUM ASC";
	
}else{
		
	$Query    = "SELECT
				ventas.id NUM,
				DATE_FORMAT(ventas.fecha, '%d/%m/%Y') FECHA,
                tipo_doc.nombre TIPO_DOC,
                clientes.documento DOCUMENTO,
                IF(clientes.residente='1', 'SI', 'NO') RESIDENTE,
                clientes.nombres NOMBRES,
                paises.iso NACIONALIDAD,
                ventas.monto IMPORTE,
                divisas.divisa MONEDA,
                divisas.cambio TIPO_CAMBIO,
                FORMAT(ventas.monto * divisas.cambio,2) TOTAL,
                clientes.direccion DIRECCION,
                clientes.telefono TELEFONO,
                divisas.descripcion DESCRIPCION,
                sucursal.nombre SUCURSAL,
                clientes.ocupacion OCUPACION,
                ventas.hora HORA
			FROM
				ventas,
				clientes,
				divisas,
                sucursal,
                tipo_doc,
                paises
			WHERE
				ventas.id_cliente = clientes.id AND
				ventas.id_divisa = divisas.id AND
                ventas.id_sucursal =sucursal.id AND
                clientes.tipo_doc=tipo_doc.id AND
                clientes.nacionalidad=paises.id AND
				ventas.fecha BETWEEN('".$fechaIni."') AND ('".$fechaFin."') AND ventas.id_sucursal = '".$idSucursal."' ORDER BY  NUM ASC";	
		
}
?>
<?php	
header('Content-type: application/vnd.ms-excel;charset=utf-8');
header('Content-Disposition: attachment; filename=compras-'.date('d-m-Y').'.xls');

	$Reporte = $mysqli->query($Query); ?>

	<table border="1" cellpadding="2" cellspacing="0" width="100%"> 
	  <tr>
		<td bgcolor="#C4D79B" align="center"><b>NUM FACTURA</b></td>
		<td bgcolor="#C4D79B" align="center"><b>FECHA</b></td>
		<td bgcolor="#C4D79B" align="center"><b>TIPO DOC</b></td>
		<td bgcolor="#C4D79B" align="center"><b>DOCUMENTO</b></td>
		<td bgcolor="#C4D79B" align="center"><b>RESIDENTE</b></td>
		<td bgcolor="#C4D79B" align="center"><b>NOMBRES / APELLIDOS</b></td>
		<td bgcolor="#C4D79B" align="center"><b>NACIONALIDAD</b></td>
		<td bgcolor="#C4D79B" align="center"><b>IMPORTE</b></td>
		<td bgcolor="#C4D79B" align="center"><b>MONEDA</b></td>
		<td bgcolor="#C4D79B" align="center"><b>TIPO CAMBIO</b></td>
		<td bgcolor="#C4D79B" align="center"><b>TOTAL</b></td>
		<td bgcolor="#C4D79B" align="center"><b>DIRECCION</b></td>
		<td bgcolor="#C4D79B" align="center"><b>TELEFONO</b></td>
		<td bgcolor="#C4D79B" align="center"><b>DESCRIPCION</b></td>
		<td bgcolor="#C4D79B" align="center"><b>SUCURSAL</b></td>
		<td bgcolor="#C4D79B" align="center"><b>OCUPACION</b></td>
		<td bgcolor="#C4D79B" align="center"><b>HORA</b></td>
	  </tr>
	<?php while($row = $Reporte->fetch_array()){  
			if ($colordefila==0){
				$color= "#ffffff";
				$colordefila=1;
			 }else{
				$color="#DCE6F1";
				$colordefila=0;
			 }
	?>
	  <tr>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['NUM'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['FECHA'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['TIPO_DOC'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['DOCUMENTO'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['RESIDENTE'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['NOMBRES'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['NACIONALIDAD'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['IMPORTE'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['MONEDA'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['TIPO_CAMBIO'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['TOTAL'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['DIRECCION'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['TELEFONO'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['DESCRIPCION'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['SUCURSAL'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['OCUPACION'];?></td>
		<td bgcolor="<?php echo $color; ?>" align="center"><?php echo $row['HORA'];?></td>
	  </tr>
<?php	} ?>
	</table>