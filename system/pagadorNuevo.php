<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
?>
<script type="text/javascript">
$selecPaises = null;
$selecRemesadora = null;
$(function() {
    
    iniciarBusquedaPais();
	iniciarBuscaRemesadora();

});

function iniciarBusquedaPais() {
	$("#pais").val('').trigger('change');
    $selecPaises = $('#pais').select2({
		tags:true,
        language: "es",
		allowClear: true,
		 placeholder: "Escriba el nombre del pais...",
        ajax: {
            url: 'enviosPais.php',
            dataType: 'json',
            processResults: function(data) {
                return {
                    results: data
                };
            },
        }
    });
}

function iniciarBuscaRemesadora(){
		$("#pagador").val('').trigger('change');
		$selecRemesadora = $('#remesadora').select2({
		tags:true,
        language: "es",
		allowClear: true,
		 placeholder: "Escriba el nombre de la remesadora...",
        ajax: {
          url: "pagadorRemesador.php",
          dataType: 'json',
		  type:"GET",
          processResults: function (data) {
            return {
              results: data
            };
          },
        }
      });
}
</script>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Datos Pagador</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<div class="row">
   <div class="card-box">
      <form action="index.php?page=acciones&accion=pagadorNuevo" method="post" enctype="multipart/form-data">
         <input type="hidden" name="validator" id="validator" value="ok" />
         <div class="row">
            <div class="col-md-4 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="nombre"> Nombre</label>
                  <input type="text" class="form-control" name="nombre" id="nombre" placeholder="RIA" required />
               </div>
            </div>
			<div class="col-md-4 col-xs-12">
             <div class="form-group">
                            <label class="control-label" for="remesadora"> Remesadora</label>
                            <select id="remesadora" name="remesadora" class="remesadora form-control" required></select>
							<option value=""></option>
                        </div>
            </div>
			<div class="col-md-4 col-xs-12">
             <div class="form-group">
                            <label class="control-label" for="pais"> Pais</label>
                            <select id="pais" name="pais" class="pais form-control" autocomplete="country-name" required></select>
                        </div>
            </div>
         </div>
         <div class="row mb-4">
            <div class="col-md-2 col-xs-1">&nbsp;</div>
            <div class="col-md-8 col-xs-10 text-center">
               <button type="submit" class="btn btn-info btn-raised"><i class="fa fa-save"></i> Guardar</button>
            </div>
            <div class="col-md-2 col-xs-1">&nbsp;</div>
         </div>
      </form>
   </div>
</div>