<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
?>
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Datos Sucursal</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<div class="row">
   <div class="card-box">
      <form action="index.php?page=acciones&accion=sucursalesNuevo" method="post" method="post" enctype="multipart/form-data">
	  <input type="hidden" name="validator" id="validator" value="ok" />
         <div class="row">
            <div class="col-md-3 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="nombre"> Nombre</label>
                  <input type="text" class="form-control" name="nombre" id="nombre" placeholder="MADRID" required />
               </div>
            </div>
            <div class="col-md-3 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="long"> Longitud</label>
                  <input type="text" class="form-control" name="long" id="long" placeholder="0.0000"  />
               </div>
            </div>
            <div class="col-md-3 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="lat"> Latitud</label>
                  <input type="text" class="form-control" name="lat" id="lat" placeholder="0.000"  />
               </div>
            </div>
			<div class="col-md-3 col-xs-12">
               <div class="form-group">
                  <label class="control-label" for="limite"> Limite</label>
                  <input type="number" class="form-control" name="limite" id="limite" placeholder="00.00"   pattern="^\d+(?:\.\d{1,2})?$"/>
               </div>
            </div>
         </div>
         <div class="row mb-4">
            <div class="col-md-2 col-xs-1">&nbsp;</div>
            <div class="col-md-8 col-xs-10 text-center">
               <button type="submit" class="btn btn-info btn-raised"><i class="fa fa-save"></i> Guardar</button>
            </div>
            <div class="col-md-2 col-xs-1">&nbsp;</div>
         </div>
      </form>
   </div>
</div>