
<?php
   if($_SESSION["joldin_log"] == TRUE) {
   	} else {
   	header("Location: ../index.php");
   }

//Reporte   
$CierreB = "SELECT
				cierres.id as id,
				cierres.fecha as fecha,
				empleados.nombres AS empleado,
				sucursal.nombre AS sucursal,
				cono_monetario.total AS total,
				IFNULL(cono_monetario.cantidad_500,0) 0cantidad_500,
				IFNULL(500.00 * cono_monetario.cantidad_500,0) cantidad_500,
				IFNULL(cono_monetario.cantidad_200,0) 0cantidad_200,
				IFNULL(200.00 * cono_monetario.cantidad_200,0) cantidad_200,
				IFNULL(cono_monetario.cantidad_100,0) 0cantidad_100,
				IFNULL(100.00 * cono_monetario.cantidad_100,0) cantidad_100,
				IFNULL(cono_monetario.cantidad_50,0) 0cantidad_50,
				IFNULL(50.00 * cono_monetario.cantidad_50,0) cantidad_50,
				IFNULL(cono_monetario.cantidad_20,0) 0cantidad_20,
				IFNULL(20.00 * cono_monetario.cantidad_20,0) cantidad_20,
				IFNULL(cono_monetario.cantidad_10,0) 0cantidad_10,
				IFNULL(10.00 * cono_monetario.cantidad_10,0) cantidad_10,
				IFNULL(cono_monetario.cantidad_5,0) 0cantidad_5,
				IFNULL(5.00 * cono_monetario.cantidad_5,0) cantidad_5,
				IFNULL(cono_monetario.cantidad_2,0) 0cantidad_2,
				IFNULL(2.00 * cono_monetario.cantidad_2,0) cantidad_2,
				IFNULL(cono_monetario.cantidad_1,0) 0cantidad_1,
				IFNULL(1.00 * cono_monetario.cantidad_1,0) cantidad_1,
				IFNULL(cono_monetario.cantidad_050,0) 0cantidad_050,
				IFNULL(0.50 * cono_monetario.cantidad_050,0) cantidad_050,
				IFNULL(cono_monetario.cantidad_020,0) 0cantidad_020,
				IFNULL(0.20 * cono_monetario.cantidad_020,0) cantidad_020,
				IFNULL(cono_monetario.cantidad_010,0) 0cantidad_010,
				IFNULL(0.10 * cono_monetario.cantidad_010,0) cantidad_010,
				IFNULL(cono_monetario.cantidad_005,0) 0cantidad_005,
				IFNULL(0.05 * cono_monetario.cantidad_005,0) cantidad_005,
				IFNULL(cono_monetario.cantidad_002,0) 0cantidad_002,
				IFNULL(0.02 * cono_monetario.cantidad_002,0) cantidad_002,
				IFNULL(cono_monetario.cantidad_001,0) 0cantidad_001,
				IFNULL(0.01 * cono_monetario.cantidad_001,0) cantidad_001
			FROM
				cierres,
				cono_monetario,
				sucursal,
				empleados
			WHERE
				cierres.id = cono_monetario.id_cierre AND 
				cierres.id_sucursal = sucursal.id AND
				cierres.id_empleado = empleados.id AND 
				cierres.id = '".$_GET['id']."'";
				
$Cierres = $mysqli->query($CierreB);
$Cierre = $Cierres->fetch_array();
$Cierres->close();


 
   
   ?>
   <div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title">Datos Cierre</h4>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
   <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="pull-left">
                                               <img src="../img/logo_sm.png">
                                            </div>
                                            <div class="pull-right">
                                                <h4><strong><?php echo $Cierre['fecha']; ?>  <?php echo $Cierre['hora']; ?></strong></h4>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6"> 
											  <p><strong>Empleado:</strong><br> <?php echo $Cierre['empleado']; ?></p>
											</div>
											 <div class="col-md-6 text-right"> 
											  <p><strong>Sucusal:</strong> <span class="label label-info"><?php echo $Cierre['sucursal']; ?></span></p>
											</div>
                                        </div>
										 <div class="clearfix m-t-10"></div>
									
											<table width="100%" border="1" cellspacing="0" cellpadding="0" align="center">
										  <tr align="center">
											<td><strong>VALOR</strong></td>
											<td><strong>CANTIDAD</strong></td>
											<td><strong>TOTAL</strong></td>
										  </tr>
										  <tr align="center">
											<td><strong>500,00 €</strong></td>
											<td><?php echo $Cierre['0cantidad_500']; ?></td>
											<td><?php echo $Cierre['cantidad_500']; ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>200,00 €</strong></td>
											<td><?php echo $Cierre['0cantidad_200']; ?></td>
											<td><?php echo $Cierre['cantidad_200']; ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>100,00 €</strong></td>
											<td><?php echo $Cierre['0cantidad_100']; ?></td>
											<td><?php echo $Cierre['cantidad_100']; ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>50,00 €</strong></td>
											<td><?php echo $Cierre['0cantidad_50']; ?></td>
											<td><?php echo $Cierre['cantidad_50']; ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>20,00 €</strong></td>
											<td><?php echo $Cierre['0cantidad_20']; ?></td>
											<td><?php echo $Cierre['cantidad_20']; ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>10,00 €</strong></td>
											<td><?php echo $Cierre['0cantidad_10']; ?></td>
											<td><?php echo $Cierre['cantidad_10']; ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>5,00 €</strong></td>
											<td><?php echo $Cierre['0cantidad_5']; ?></td>
											<td><?php echo $Cierre['cantidad_5']; ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>2,00 €</strong></td>
											<td><?php echo $Cierre['0cantidad_2']; ?></td>
											<td><?php echo $Cierre['cantidad_2']; ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>1,00 €</strong></td>
											<td><?php echo $Cierre['0cantidad_1']; ?></td>
											<td><?php echo $Cierre['cantidad_1']; ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,50 €</strong></td>
											<td><?php echo $Cierre['0cantidad_050']; ?></td>
											<td><?php echo $Cierre['cantidad_050']; ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,20 €</strong></td>
											<td><?php echo $Cierre['0cantidad_020']; ?></td>
											<td><?php echo $Cierre['cantidad_020']; ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,10 €</strong></td>
											<td><?php echo $Cierre['0cantidad_010']; ?></td>
											<td><?php echo $Cierre['cantidad_010']; ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,05 €</strong></td>
											<td><?php echo $Cierre['0cantidad_005']; ?></td>
											<td><?php echo $Cierre['cantidad_005']; ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,02 €</strong></td>
											<td><?php echo $Cierre['0cantidad_002']; ?></td>
											<td><?php echo $Cierre['cantidad_002']; ?></td>
										  </tr>
										  <tr align="center">
											<td><strong>0,01 €</strong></td>
											<td><?php echo $Cierre['0cantidad_001']; ?></td>
											<td><?php echo $Cierre['cantidad_001']; ?></td>
										  </tr>
										</table>
										<hr>
										
                                        <div class="row">
                                            <div class="col-md-6 text-justify">
											&nbsp;
											</div>
                                            <div class="col-md-6 text-right">
											<b>Total:</b> <?php echo $Cierre['total']; ?> €
											</div>
                                            </div>
                                        <hr class="hidden-print">
                                     <div class="hidden-print">
                                            <div class="pull-right">
                                                <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i> Imprimir</a>
                                                <a href="index.php?page=cierreLista" class="btn btn-info waves-effect waves-light"><i class="mdi mdi-keyboard-backspace"></i> Atras</a>
                                            </div>
                                        </div>
										
                                    </div>
                                </div>

                            </div>

                        </div>