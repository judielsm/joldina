<?php
include ("../inc/config.php");			

$fechaIni = $_POST['fecha_ini']; 
$fechaIni = explode('-', $fechaIni);
$fechaIni = $fechaIni[2].'-'.$fechaIni[1].'-'.$fechaIni[0];
$fechaFin = $_POST['fecha_fin']; 
$fechaFin = explode('-', $fechaFin);
$fechaFin = $fechaFin[2].'-'.$fechaFin[1].'-'.$fechaFin[0];
$formatoTipo = $_POST['formato'];



if($formatoTipo=="1"){
?>
<?php	
header('Content-type: application/vnd.ms-excel;charset=utf-8');
header('Content-Disposition: attachment; filename=proveedores-'.date('d-m-Y').'.xls');
$query = "SELECT * FROM sucursal where id!=100";
$rs = $mysqli->query($query);
$datos = array();
$cab = array();
$cab2 = array();
$cab3 = array();
while ($filas = $rs->fetch_array()) {
    $cab[] = $filas["nombre"];
    $query2 = "SELECT
					remesadora.nombre,
					IFNULL(a.cant, 0) AS cantEnvio,
					remesadora.id,
					IFNULL(a.total, 0) AS totalEnvio,
					IFNULL(b.cant, 0) AS cantDevo,
					IFNULL(b.total, 0) AS totalDevo,
					IFNULL(COUNT(pagos.id_remesadora),0) AS cantPagos,
					IFNULL(SUM(pagos.monto),
					0) AS totalPagos
				FROM
					remesadora
				LEFT JOIN(
					SELECT COUNT(*) AS cant,
						SUM(monto) AS total,
						id_remesadora AS idreme,
						devolucion
					FROM
						envios
					WHERE
						fecha BETWEEN('".$fechaIni."') AND('".$fechaFin."') AND id_sucursal =".$filas['id']." AND devolucion = 0
					GROUP BY
						envios.id_remesadora) AS a
				ON
					a.idreme = remesadora.id
				LEFT JOIN(
					SELECT COUNT(*) AS cant,
						SUM(monto) AS total,
						id_remesadora AS idreme,
						devolucion
					FROM
						envios
					WHERE
						fecha BETWEEN('".$fechaIni."') AND('".$fechaFin."') AND id_sucursal =".$filas['id']." AND devolucion = 1
					GROUP BY
						envios.id_remesadora) AS b
				ON
					b.idreme = remesadora.id
				LEFT JOIN pagos ON pagos.id_remesadora = remesadora.id AND pagos.fecha BETWEEN('".$fechaIni."') AND('".$fechaFin."') AND pagos.id_sucursal =".$filas['id']."
				GROUP BY
					remesadora.id";
    $rs2 = $mysqli->query($query2);

    while ($filas2 = $rs2->fetch_assoc()) {
        $cab2[$filas2["nombre"]] = $filas2["nombre"];
        $datos[$filas["nombre"]][$filas2["nombre"]] = $filas2;
    }
    $rs2->close();

    $queryl2 = "SELECT
					IFNULL(COUNT(pagos_locutorio.id),0) AS cantidad,
					IFNULL(SUM(pagos_locutorio.monto),0) AS monto,
					locutorio.nombre AS locutorio
				FROM
					locutorio
				LEFT JOIN pagos_locutorio ON pagos_locutorio.id_locutorio = locutorio.id AND 
					pagos_locutorio.fecha BETWEEN('".$fechaIni."') AND('".$fechaFin."') AND 
					pagos_locutorio.id_sucursal =".$filas['id']."
				GROUP BY
					locutorio.id";

    $rsl2 = $mysqli->query($queryl2);

    while ($locu = $rsl2->fetch_assoc()) {
        $cab3[$locu["locutorio"]] = $locu["locutorio"];
        $datos[$filas["nombre"]][$locu["locutorio"]] = $locu;
    }
    $rsl2->close();


    $totalesRR = array("1" => 0, "2" => 0, "3" => 0);
    $queryTotales1 = "SELECT
						IFNULL(COUNT(*),0) AS cant,
						IFNULL(SUM(monto),0) AS monto,
						devolucion,
						tipo_pago.id AS tipopago,
						nombre
					FROM
						tipo_pago
					LEFT JOIN envios ON tipo_pago.id = envios.id_pago AND 
						fecha BETWEEN('".$fechaIni."') AND('".$fechaFin."') AND id_sucursal =".$filas['id']."
					GROUP BY
						devolucion,
						envios.id_pago";

    $rsTotal = $mysqli->query($queryTotales1);
    while ($total1 = $rsTotal->fetch_array()) {
        if ($total1["devolucion"] == 0) $totalesRR[$total1["tipopago"]] += $total1["monto"];
        if ($total1["devolucion"] == 1) $totalesRR[$total1["tipopago"]] -= $total1["monto"];

    }
    $rsTotal->close();


    $queryTotales2 = "SELECT
						COUNT(*) AS cant,
						SUM(monto) AS monto,
						tipo_pago.id AS tipopago,
						nombre
					FROM
						tipo_pago
					LEFT JOIN pagos_locutorio ON pagos_locutorio.id_pago = tipo_pago.id AND 
						fecha BETWEEN('".$fechaIni."') AND('".$fechaFin."') AND id_sucursal =".$filas['id']."
					GROUP BY

						tipo_pago.id";
    $rsTotal2 = $mysqli->query($queryTotales2);
    while ($total2 = $rsTotal2->fetch_array()) {
        $totalesRR[$total2["tipopago"]] += $total2["monto"];

    }
    $rsTotal2->close();


    $queryTotales3 = "SELECT
						COUNT(*) AS cant,
						SUM(monto) AS monto
					FROM
						pagos
					WHERE
						fecha BETWEEN('".$fechaIni."') AND('".$fechaFin."') AND 
						id_sucursal =".$filas['id']."";
						
    $rsTotal3 = $mysqli->query($queryTotales3);
    while ($total3 = $rsTotal3->fetch_array()) {
        $totalesRR["3"] -= $total3["monto"];

    }
    $rsTotal3->close();

    $datos[$filas["nombre"]]["total1"] = $totalesRR["1"];
    $datos[$filas["nombre"]]["total2"] = $totalesRR["2"];
    $datos[$filas["nombre"]]["total3"] = $totalesRR["3"];

}
$rs->close();
array_unique($cab2);
array_unique($cab3);
?>
<table  width="100%" border="1" align='center' cellpadding="0" cellspacing="0" style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'>
	  <tr>
	    <td align="center">&nbsp;</td>
	    <td colspan="12" align="right"><b>Desde</b></td>
	    <td align="left"><b>Hasta</b></td>
	    <td align="center">&nbsp;</td>
  </tr>
	  <tr>
	    <td align="center">&nbsp;</td>
	    <td colspan="12" align="right"><?php echo $fechaIni; ?></td>
	    <td align="left"><?php echo $fechaFin; ?></td>
	    <td align="center">&nbsp;</td>
  </tr>
    <tr>
        <th><hr style='display:block;height:1px;border:0;border-top:1px solid #FFF;margin:1em 0;padding:0px;width:100px;'>&nbsp;</th>
        <?php
        foreach ($cab as $cb) {
            echo "<th bgcolor='#DCE6F1' align='center'><hr style='display: block;height:1px;border:0;border-top:1px solid #DCE6F1;margin:1em 0;padding:0px;width:100px;'>".$cb."</th>";
        }
        ?>
        <th bgcolor="#DCE6F1" align="center"><hr style='display: block;height:1px;border:0;border-top:1px solid #DCE6F1;margin:1em 0;padding:0px;width:100px;'>TOTAL GENERAL</th>
    </tr>
    <tbody>
    <?php

    $totales = array();
    foreach ($cab as $cb) {
        $totales[$cb] = 0;
    }
    foreach ($cab2 as $cb2) {
        $totalFila1 = 0;
        $totalFila2 = 0;
        $totalFila3 = 0;
        echo "<tr><th align='center' bgcolor='#C4D79B'>".$cb2."</th>";
        foreach ($cab as $cb) {

            $totalFila1 += $datos[$cb][$cb2]["totalEnvio"];
            $totalFila2 += $datos[$cb][$cb2]["totalDevo"];
            $totalFila3 += $datos[$cb][$cb2]["totalPagos"];
            $totales[$cb] += ($datos[$cb][$cb2]["totalEnvio"] - $datos[$cb][$cb2]["totalPagos"] - $datos[$cb][$cb2]["totalDevo"]);
			//totalEnvio,totalDevo,totalPagos
               echo "<td align='center'>".number_format($datos[$cb][$cb2]["totalEnvio"]-$datos[$cb][$cb2]["totalDevo"]-$datos[$cb][$cb2]["totalPagos"], 2, ',', '.') . "</td>";
        }//Fila1,2,3
          echo "<td align='center'>".number_format($totalFila1-$totalFila2-$totalFila3, 2, ',', '.');
		  echo "</td></tr>";
    }
    echo "<tr><td></td>";
    foreach ($cab as $cb) {
        echo "<td>&nbsp;</td>";
    }
    echo "<td></td></tr>";

    foreach ($cab3 as $cb3) {
        $totalFila1 = 0;
        $totalFila2 = 0;
        echo "<tr><th align='center' bgcolor='#C4D79B'>".$cb3."</th>";
        foreach ($cab as $cb) {
            $totalFila1 += $datos[$cb][$cb3]["monto"];
            echo "<td align='center'>" . number_format($datos[$cb][$cb3]["monto"], 2, ',', '.') . "</td>";
            $totales[$cb] += $datos[$cb][$cb3]["monto"];
        }
        echo "<td align='center'>".number_format($totalFila1, 2, ',', '.')."</td></tr>";
    }
    ?>
    </tbody>
    <tfoot>
    <tr>
        <th align='center' bgcolor='#C4D79B'>TOTAL</th>
        <?php

        foreach ($cab as $cb) {

            echo "<th align='center'>".number_format($totales[$cb], 2, ',', '.')."</th>";
        }
        echo "<th align='center'>".number_format(array_sum($totales), 2, ',', '.')."</th>";
        ?>
    </tr>
    <?php
    echo "<tr><td></td>";
    foreach ($cab as $cb) {
        echo "<td>&nbsp;</td>";
    }
    echo "<td></td>
</tr>
<tr><th align='center' bgcolor='#C4D79B'>Bankia</th>";
    $totalBankia = 0;
    foreach ($cab as $cb) {
        $totalBankia += $datos[$cb]["total1"];
        echo "<td align='center'>".number_format($datos[$cb]["total1"], 2, ',', '.')."</td>";
    }
    echo "<td align='center'>".number_format($totalBankia, 2, ',', '.')."</td></tr>";

    echo "
<tr><th align='center' bgcolor='#C4D79B'>Caixa</th>";
    $totalCai = 0;
    foreach ($cab as $cb) {
        $totalCai += $datos[$cb]["total2"];
        echo "<td align='center'>".number_format($datos[$cb]["total2"], 2, ',', '.')."</td>";
    }
    echo "<td align='center'>".number_format($totalCai, 2, ',', '.')."</td></tr>";
    echo "<tr><th align='center' bgcolor='#C4D79B'>Efectivo</th>";
    $totalEfec = 0;
    foreach ($cab as $cb) {
        $totalEfec += $datos[$cb]["total3"];
        echo "<td align='center'>".number_format($datos[$cb]["total3"], 2, ',', '.')."</td>";
    }
    echo "<td align='center'>".number_format($totalEfec, 2, ',', '.')."</td></tr>";
    ?>
    </tr>
    </tfoot>
</table>

<?php 
}else{

include("../inc/header.php"); 
$query = "SELECT * FROM sucursal where id!=100";
$rs = $mysqli->query($query);
$datos = array();
$cab = array();
$cab2 = array();
$cab3 = array();
while ($filas = $rs->fetch_array()) {
    $cab[] = $filas["nombre"];
    $query2 = "SELECT
					remesadora.nombre,
					IFNULL(a.cant, 0) AS cantEnvio,
					remesadora.id,
					IFNULL(a.total, 0) AS totalEnvio,
					IFNULL(b.cant, 0) AS cantDevo,
					IFNULL(b.total, 0) AS totalDevo,
					IFNULL(COUNT(pagos.id_remesadora),0) AS cantPagos,
					IFNULL(SUM(pagos.monto),
					0) AS totalPagos
				FROM
					remesadora
				LEFT JOIN(
					SELECT COUNT(*) AS cant,
						SUM(monto) AS total,
						id_remesadora AS idreme,
						devolucion
					FROM
						envios
					WHERE
						fecha BETWEEN('".$fechaIni."') AND('".$fechaFin."') AND id_sucursal =".$filas['id']." AND devolucion = 0
					GROUP BY
						envios.id_remesadora) AS a
				ON
					a.idreme = remesadora.id
				LEFT JOIN(
					SELECT COUNT(*) AS cant,
						SUM(monto) AS total,
						id_remesadora AS idreme,
						devolucion
					FROM
						envios
					WHERE
						fecha BETWEEN('".$fechaIni."') AND('".$fechaFin."') AND id_sucursal =".$filas['id']." AND devolucion = 1
					GROUP BY
						envios.id_remesadora) AS b
				ON
					b.idreme = remesadora.id
				LEFT JOIN pagos ON pagos.id_remesadora = remesadora.id AND pagos.fecha BETWEEN('".$fechaIni."') AND('".$fechaFin."') AND pagos.id_sucursal =".$filas['id']."
				GROUP BY
					remesadora.id";
    $rs2 = $mysqli->query($query2);

    while ($filas2 = $rs2->fetch_assoc()) {
        $cab2[$filas2["nombre"]] = $filas2["nombre"];
        $datos[$filas["nombre"]][$filas2["nombre"]] = $filas2;
    }
    $rs2->close();

    $queryl2 = "SELECT
					IFNULL(COUNT(pagos_locutorio.id),0) AS cantidad,
					IFNULL(SUM(pagos_locutorio.monto),0) AS monto,
					locutorio.nombre AS locutorio
				FROM
					locutorio
				LEFT JOIN pagos_locutorio ON pagos_locutorio.id_locutorio = locutorio.id AND 
					pagos_locutorio.fecha BETWEEN('".$fechaIni."') AND('".$fechaFin."') AND 
					pagos_locutorio.id_sucursal =".$filas['id']."
				GROUP BY
					locutorio.id";

    $rsl2 = $mysqli->query($queryl2);

    while ($locu = $rsl2->fetch_assoc()) {
        $cab3[$locu["locutorio"]] = $locu["locutorio"];
        $datos[$filas["nombre"]][$locu["locutorio"]] = $locu;
    }
    $rsl2->close();

    $queryPVentas = "SELECT *, SUM(total) as monto FROM productos_ventas WHERE
						fecha BETWEEN('".$fechaIni."') AND('".$fechaFin."') AND id_sucursal =".$filas['id']." 
					GROUP BY
						id_sucursal";
    //echo $queryPVentas;

    $rslPV = $mysqli->query($queryPVentas);

    while ($pv = $rslPV->fetch_assoc()) {
        $cab3['DIVISAS'] = 'DIVISAS';
        $datos[$filas["nombre"]]['DIVISAS'] = $pv;
    }
    $rslPV->close();

    $queryPPOST = "SELECT *, SUM(monto) as monto FROM ventas WHERE
						fecha BETWEEN('".$fechaIni."') AND('".$fechaFin."') AND id_sucursal =".$filas['id']." 
					GROUP BY
						id_sucursal";
    //echo $queryPVentas;

    $rslPPOST = $mysqli->query($queryPPOST);

    while ($pp = $rslPPOST->fetch_assoc()) {
        $cab3['POS'] = 'POS';
        $datos[$filas["nombre"]]['POS'] = $pp;
    }
    $rslPPOST->close();

    $totalesRR = array("1" => 0, "2" => 0, "3" => 0);
    $queryTotales1 = "SELECT
						IFNULL(COUNT(*),0) AS cant,
						IFNULL(SUM(monto),0) AS monto,
						devolucion,
						tipo_pago.id AS tipopago,
						nombre
					FROM
						tipo_pago
					LEFT JOIN envios ON tipo_pago.id = envios.id_pago AND 
						fecha BETWEEN('".$fechaIni."') AND('".$fechaFin."') AND id_sucursal =".$filas['id']."
					GROUP BY
						devolucion,
						envios.id_pago";

    $rsTotal = $mysqli->query($queryTotales1);
    while ($total1 = $rsTotal->fetch_array()) {
        if ($total1["devolucion"] == 0) $totalesRR[$total1["tipopago"]] += $total1["monto"];
        if ($total1["devolucion"] == 1) $totalesRR[$total1["tipopago"]] -= $total1["monto"];

    }
    $rsTotal->close();


    $queryTotales2 = "SELECT
						COUNT(*) AS cant,
						SUM(monto) AS monto,
						tipo_pago.id AS tipopago,
						nombre
					FROM
						tipo_pago
					LEFT JOIN pagos_locutorio ON pagos_locutorio.id_pago = tipo_pago.id AND 
						fecha BETWEEN('".$fechaIni."') AND('".$fechaFin."') AND id_sucursal =".$filas['id']."
					GROUP BY
						tipo_pago.id";
    $rsTotal2 = $mysqli->query($queryTotales2);
    while ($total2 = $rsTotal2->fetch_array()) {
        $totalesRR[$total2["tipopago"]] += $total2["monto"];

    }
    $rsTotal2->close();


    $queryTotales3 = "SELECT
						COUNT(*) AS cant,
						SUM(monto) AS monto
					FROM
						pagos
					WHERE
						fecha BETWEEN('".$fechaIni."') AND('".$fechaFin."') AND 
						id_sucursal =".$filas['id']."";
						
    $rsTotal3 = $mysqli->query($queryTotales3);
    while ($total3 = $rsTotal3->fetch_array()) {
        $totalesRR["3"] -= $total3["monto"];

    }
    $rsTotal3->close();


    $datos[$filas["nombre"]]["total1"] = $totalesRR["1"];
    $datos[$filas["nombre"]]["total2"] = $totalesRR["2"];
    $datos[$filas["nombre"]]["total3"] = $totalesRR["3"];

}
$rs->close();
array_unique($cab2);
array_unique($cab3);
?>
<style>
table{
   border-collapse: collapse;
   width: 100%;
   }
   th, td {
   padding: 9px;
   }

</style>
<table  width="100%" border="1" align='center' cellpadding="0" cellspacing="0" style='font-family:Arial, Helvetica, sans-serif;font-size:11px;'>
	  <tr>
	    <td colspan="25" align="center"><img src="../img/logo_sm.png"></td>
  </tr>
	  <tr>
	    <td align="center">&nbsp;</td>
	    <td colspan="12" align="right"><b>Desde</b></td>
	    <td colspan="11" align="left"><b>Hasta</b></td>
	    <td align="center">&nbsp;</td>
  </tr>
	  <tr>
	    <td align="center">&nbsp;</td>
	    <td colspan="12" align="right"><?php echo $fechaIni; ?></td>
	    <td colspan="11" align="left"><?php echo $fechaFin; ?></td>
	    <td align="center">&nbsp;</td>
  </tr>
    <tr>
        <th><hr style='display:block;height:1px;border:0;border-top:1px solid #FFF;margin:1em 0;padding:0px;width:100px;'>&nbsp;</th>
        <?php
        foreach ($cab as $cb) {
            echo "<th bgcolor='#DCE6F1' align='center'><hr style='display: block;height:1px;border:0;border-top:1px solid #DCE6F1;margin:1em 0;padding:0px;width:100px;'>".$cb."</th>";
        }
        ?>
        <th bgcolor="#DCE6F1" align="center"><hr style='display: block;height:1px;border:0;border-top:1px solid #DCE6F1;margin:1em 0;padding:0px;width:100px;'>TOTAL GENERAL</th>
    </tr>
    <tbody>
    <?php

    $totales = array();
    foreach ($cab as $cb) {
        $totales[$cb] = 0;
    }
    foreach ($cab2 as $cb2) {
        $totalFila1 = 0;
        $totalFila2 = 0;
        $totalFila3 = 0;
        echo "<tr><th align='center' bgcolor='#C4D79B'>".$cb2."</th>";
        foreach ($cab as $cb) {

            $totalFila1 += $datos[$cb][$cb2]["totalEnvio"];
            $totalFila2 += $datos[$cb][$cb2]["totalDevo"];
            $totalFila3 += $datos[$cb][$cb2]["totalPagos"];
            $totales[$cb] += ($datos[$cb][$cb2]["totalEnvio"] - $datos[$cb][$cb2]["totalPagos"] - $datos[$cb][$cb2]["totalDevo"]);
			//totalEnvio,totalDevo,totalPagos
               echo "<td align='center'>".number_format($datos[$cb][$cb2]["totalEnvio"]-$datos[$cb][$cb2]["totalDevo"]-$datos[$cb][$cb2]["totalPagos"], 2, ',', '.') . "</td>";
        }//Fila1,2,3
          echo "<td align='center'>".number_format($totalFila1-$totalFila2-$totalFila3, 2, ',', '.');
		  echo "</td></tr>";
    }
    echo "<tr><td></td>";
    foreach ($cab as $cb) {
        echo "<td>&nbsp;</td>";
    }
    echo "<td></td></tr>";

    foreach ($cab3 as $cb3) {
        $totalFila1 = 0;
        $totalFila2 = 0;
        echo "<tr><th align='center' bgcolor='#C4D79B'>".$cb3."</th>";
        foreach ($cab as $cb) {
            $totalFila1 += $datos[$cb][$cb3]["monto"];
            echo "<td align='center'>" . number_format($datos[$cb][$cb3]["monto"], 2, ',', '.') . "</td>";
            $totales[$cb] += $datos[$cb][$cb3]["monto"];
        }
        echo "<td align='center'>".number_format($totalFila1, 2, ',', '.')."</td></tr>";
    }
    ?>
    </tbody>
    <tfoot>
    <tr>
        <th align='center' bgcolor='#C4D79B'>TOTAL</th>
        <?php

        foreach ($cab as $cb) {

            echo "<th align='center'>".number_format($totales[$cb], 2, ',', '.')."</th>";
        }
        echo "<th align='center'>".number_format(array_sum($totales), 2, ',', '.')."</th>";
        ?>
    </tr>
    <?php
    echo "<tr><td></td>";
    foreach ($cab as $cb) {
        echo "<td>&nbsp;</td>";
    }
    echo "<td></td>
</tr>
<tr><th align='center' bgcolor='#C4D79B'>Bankia</th>";
    $totalBankia = 0;
    foreach ($cab as $cb) {
        $totalBankia += $datos[$cb]["total1"];
        echo "<td align='center'>".number_format($datos[$cb]["total1"], 2, ',', '.')."</td>";
    }
    echo "<td align='center'>".number_format($totalBankia, 2, ',', '.')."</td></tr>";

    echo "
<tr><th align='center' bgcolor='#C4D79B'>Caixa</th>";
    $totalCai = 0;
    foreach ($cab as $cb) {
        $totalCai += $datos[$cb]["total2"];
        echo "<td align='center'>".number_format($datos[$cb]["total2"], 2, ',', '.')."</td>";
    }
    echo "<td align='center'>".number_format($totalCai, 2, ',', '.')."</td></tr>";
    echo "<tr><th align='center' bgcolor='#C4D79B'>Efectivo</th>";
    $totalEfec = 0;
    foreach ($cab as $cb) {
        $totalEfec += $datos[$cb]["total3"];
        echo "<td align='center'>".number_format($datos[$cb]["total3"], 2, ',', '.')."</td>";
    }
    echo "<td align='center'>".number_format($totalEfec, 2, ',', '.')."</td></tr>";
    ?>
    </tr>
	<tr>
	    <td colspan="25" align="center">
        <a href="javascript:window.close();" style="text-decoration:none;color:black;"><b>Regresar</b></a> 
        </td>
  </tr>
    </tfoot>
</table>
<?php } ?>