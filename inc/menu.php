<?php
if ($_SESSION["joldin_log"] == true){
}else{
    header("Location: ../index.php");
}
?>
<div id="sidebar-menu">
   <ul>
    <li class="has_sub">
         <a href="index.php?page=sysHome" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Panel De Control </span></a>
      </li>
      <li class="has_sub">
         <a  class="waves-effect"><i class="mdi mdi-cash-multiple"></i> <span> Remesas </span> <span class="menu-arrow"></span></a>
         <ul class="list-unstyled">
		 <?php if($_SESSION["joldin_rol"]=="2"){ ?>
            <li><a href="index.php?page=enviosNuevo"><i class="mdi mdi-plus-box"></i> Envíos</a></li>
			<li><a href="index.php?page=enviosLista"><i class="mdi mdi-plus-box"></i> Ver Envíos</a></li>
             <li><a href="index.php?page=pagosNuevo"><i class="mdi mdi-plus-box"></i> Pagos</a></li>
			 <li><a href="index.php?page=pagosLista"><i class="mdi mdi-plus-box"></i> Ver Pagos</a></li>
			  <li><a href="index.php?page=devolucionesNuevo"><i class="mdi mdi-plus-box"></i> Devoluciones</a></li>
			 <li><a href="index.php?page=devolucionesLista"><i class="mdi mdi-plus-box"></i> Ver Devoluciones</a></li>
			<?php } ?>
			 <?php if($_SESSION["joldin_rol"]=="1"){ ?>
			<li><a href="index.php?page=enviosLista"><i class="mdi mdi-plus-box"></i> Ver Envíos</a></li>
			<li><a href="index.php?page=pagosLista"><i class="mdi mdi-plus-box"></i> Ver Pagos</a></li>
			 <li><a href="index.php?page=devolucionesLista"><i class="mdi mdi-plus-box"></i> Ver Devoluciones</a></li>
			<?php } ?>
         </ul>
      </li>
	  
	  <li class="has_sub">
         <a  class="waves-effect"><i class="mdi mdi-coin"></i> <span> Divisas </span> <span class="menu-arrow"></span></a>
         <ul class="list-unstyled">
		  <?php if($_SESSION["joldin_rol"]=="1"){ ?>
            <li><a href="index.php?page=nuevaDivisa"><i class="mdi mdi-plus-box"></i> Nueva</a></li>
			<li><a href="index.php?page=divisasLista"><i class="mdi mdi-plus-box"></i> Ver Divisas</a></li>
			<li><a href="index.php?page=divisasVentas"><i class="mdi mdi-plus-box"></i> Ver Compras</a></li>
			 <?php } ?>
			 <?php if($_SESSION["joldin_rol"]=="2"){ ?>
			 <li><a href="index.php?page=comprasNueva"><i class="mdi mdi-plus-box"></i> Nueva Compra</a></li>
			<li><a href="index.php?page=divisasVentas"><i class="mdi mdi-plus-box"></i> Ver Compras</a></li>
			 <?php } ?>
         </ul>
      </li>
	 
	  <li class="has_sub">
		<a  class="waves-effect"><i class="mdi mdi-sale"></i> <span> Punto de Venta </span> <span class="menu-arrow"></span></a>
		 <ul class="list-unstyled">
		  <?php if($_SESSION["joldin_rol"]=="1"){ ?>
		 <li><a href="index.php?page=productosNuevo"><i class="mdi mdi-plus-box"></i> Nuevo Producto</a></li>
		<li><a href="index.php?page=productosLista"><i class="mdi mdi-plus-box"></i> Productos</a></li>
		<li><a href="index.php?page=productosVentas"><i class="mdi mdi-plus-box"></i> Ventas</a></li>
		 <?php } ?>
			 <?php if($_SESSION["joldin_rol"]=="2"){ ?>
		<li><a href="index.php?page=productosVentas"><i class="mdi mdi-plus-box"></i> Ventas</a></li>
		<li><a href="index.php?page=productosPOS"><i class="mdi mdi-plus-box"></i> POS</a></li>
		 <?php } ?>
		 </ul>
	  </li>
	 
	 <li class="has_sub">
         <a class="waves-effect"><i class="mdi mdi-buffer"></i> <span> Locutorios </span> <span class="menu-arrow"></span></a>
         <ul class="list-unstyled">
		  <?php if($_SESSION["joldin_rol"]=="2"){ 
					  $sql = "SELECT id,nombre FROM locutorio";
			$result = $mysqli->query($sql);
			while($row = $result->fetch_array()){
				 echo '<li><a href="index.php?page=locutoriosNuevo&id='.$row['id'].'"><i class="mdi mdi-plus-box"></i> '.strtoupper($row['nombre']).'</a></li>';
			}	
		  
		  ?>
			<li><a href="index.php?page=locutoriosLista"><i class="mdi mdi-plus-box"></i> Ver Locutorios</a></li>
			<?php } ?>
			 <?php if($_SESSION["joldin_rol"]=="1"){ ?>
			<li><a href="index.php?page=locutoriosLista"><i class="mdi mdi-plus-box"></i> Ver Locutorios</a></li>
			<?php } ?>
         </ul>
      </li>
      <li class="has_sub">
         <a  class="waves-effect"><i class="mdi mdi-chart-bar"></i> <span> Administrativo </span> <span class="menu-arrow"></span></a>
         <ul class="list-unstyled">
		  <?php if($_SESSION["joldin_rol"]=="1"){ ?>
			<li><a href="index.php?page=reporteAcumulado"><i class="mdi mdi-plus-box"></i> Acumulado</a></li>
			<li><a href="index.php?page=reporteProveedor"><i class="mdi mdi-plus-box"></i> Proveedor</a></li>
			<li><a href="index.php?page=reporteProveedorD"><i class="mdi mdi-plus-box"></i> Operaciones</a></li>
			<li><a href="index.php?page=cierreLista"><i class="mdi mdi-plus-box"></i> Cierres</a></li>
			<li><a href="index.php?page=entregaLista"><i class="mdi mdi-plus-box"></i> Entregas</a></li>
		  <?php } 
		  if($_SESSION["joldin_rol"]=="2"){ ?>
             <li><a href="index.php?page=entregaDinero"><i class="mdi mdi-plus-box"></i> Entregar Dinero</a></li>
			 <li><a href="index.php?page=entregaLista"><i class="mdi mdi-plus-box"></i> Ver Entregas</a></li>
			 <li><a href="index.php?page=cierreCaja"><i class="mdi mdi-plus-box"></i> Cierre Caja</a></li>
			 <li><a href="index.php?page=cierreLista"><i class="mdi mdi-plus-box"></i> Ver Cierres</a></li>
			 <li><a href="index.php?page=rAcumulado"><i class="mdi mdi-plus-box"></i> Cierre del Dia</a></li>
			  <?php } ?>
         </ul>
      </li>
	   <?php if($_SESSION["joldin_rol"]=="1"){ ?>
      <li class="has_sub">
         <a  class="waves-effect"><i class="mdi mdi-settings"></i> <span> Configuración </span> <span class="menu-arrow"></span></a>
         <ul class="list-unstyled">
		 <li><a href="index.php?page=accesoLista"><i class="mdi mdi-plus-box"></i>  Accesos</a></li>
			<li><a href="index.php?page=clientesLista"><i class="mdi mdi-plus-box"></i>  Clientes</a></li>
            <li><a href="index.php?page=beneficiariosLista"><i class="mdi mdi-plus-box"></i>  Beneficiarios</a></li>
            <li><a href="index.php?page=sucursalesLista"><i class="mdi mdi-plus-box"></i> Sucursales</a></li>
            <li><a href="index.php?page=empleadosLista"><i class="mdi mdi-plus-box"></i> Empleados</a></li>
			<li><a href="index.php?page=remesadorLista"><i class="mdi mdi-plus-box"></i> Remesador</a></li>
            <li><a href="index.php?page=pagadorLista"><i class="mdi mdi-plus-box"></i> Pagador</a></li>
			<li><a href="index.php?page=locutorioLista"><i class="mdi mdi-plus-box"></i> Locutorio</a></li>
			<li><a href="index.php?page=sms"><i class="mdi mdi-plus-box"></i> SMS</a></li>
         </ul>
      </li>
	  	   <?php } ?>
      <li class="has_sub">
         <a href="index.php?page=sysRespaldo" class="waves-effect"><i class="mdi mdi-database"></i> <span> Respaldo </span></a>
      </li>
	  <li class="has_sub">
	  	<a  class="waves-effect"><i class="mdi mdi-settings"></i> <span> Cumplimiento </span> <span class="menu-arrow"></span></a>
		  <ul class="list-unstyled">
		  	<li><a href="index.php?page=cumplimientoCliente"><i class="mdi mdi-plus-box"></i>  Clientes</a></li>
			  <li><a href="index.php?page=listaNegra"><i class="mdi mdi-plus-box"></i> Lista Negra</a></li>
		  </ul>
	  </li>
   </ul>
</div>